# https://leetcode.com/problems/unique-paths/
class Solution(object):
    #     d[i][j] represents the # of ways to reach pos[i][j];
    #     d[0][j] = d[i][0] = 1
    #     d[i][j] = d[i-1][j] + d[i][j-1]
    #  update 2d array line by line (or col by col)
    def uniquePaths(self, m, n):
        """
        :type m: int
        :type n: int
        :rtype: int
        """
        dp = [[1 for _ in range(n)] for _ in range(m)]
        for i in range(1, m):
            for j in range(1, n):
                dp[i][j] = dp[i][j - 1] + dp[i - 1][j]
        return dp[m - 1][n - 1]

    # TLE
    def uniquePaths2(self, m, n):
        if m == 1 or n == 1: return 1
        return self.uniquePaths(m, n - 1) + self.uniquePaths(m - 1, n)

    # Formula
    # total N = m + n - 2 steps, k = m - 1 down, n - 1 left, so choose k out of N
    # N*(N-1)*(N-2)...*(N-k+1)/k!
    def uniquePaths3(self, m, n):
        N, k = m + n - 2, m - 1
        # res = 1
        num, denom = 1, 1
        for i in range(1, k + 1):
            # res *= ((N - k + i) / i)
            num *= (N - k + i)
            denom *= i
        # return int(res)
        return num // denom


print(Solution().uniquePaths3(3, 2))
