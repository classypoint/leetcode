class Solution(object):
    # https://leetcode.com/problems/multiply-strings/discuss/17605/Easiest-JAVA-Solution-with-Graph-Explanation
    def multiply(self, num1, num2):
        """
        :type num1: str
        :type num2: str
        :rtype: str
        """
        l1, l2 = len(num1), len(num2)
        pos = [0 for i in range(l1 + l2)]
        for i in reversed(range(l1)):
            for j in reversed(range(l2)):
                mul = (ord(num1[i]) - ord('0')) * (ord(num2[j]) - ord('0'))
                p1, p2 = i + j, i + j + 1
                mul += pos[p2]
                # pos[p1] = mul // 10
                pos[p1] += mul // 10
                pos[p2] = mul % 10

        res = ''
        for num in pos:
            if res or num: res += str(num)
        return res if res else '0'

s = Solution()
s.multiply("123", "12")
