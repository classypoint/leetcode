# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
def isSameTree(p, q):
"""
:type p: TreeNode
:type q: TreeNode
:rtype: bool
"""
	if p and q:
		return p.val == q.val and self.isSameTree(p.left,q.left) and self.isSameTree(p.right, q.right)
	return p is q
	#return p and q and p.val == q.val and all(map(self.isSameTree, (p.left, p.right), (q.left, q.right))) or p is q