# 80
class Solution:
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) < 2:
            return len(nums)
        i, j = 0, 1
        while j < len(nums):
            if nums[j] != nums[i]:
                nums[i+1] = nums[j]
                i += 1
            j += 1
        return i+1

    def removeDuplicates2(self, nums):
        count = 0
        for i in range(1, len(nums)):
            if nums[i] == nums[i - 1]:  count += 1
            else:   nums[i - count] = nums[i]
        return len(nums) - count


s = Solution()
lst = [0,0,1,1,1,2,2,3,3,4]
s.removeDuplicates2(lst)
# print(lst)