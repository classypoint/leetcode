import heapq

class Solution:
    def ladderLength(self, beginWord, endWord, wordList):
        """
        :type beginWord: str
        :type endWord: str
        :type wordList: List[str]
        :rtype: int
        """
        if not endWord in wordList:
            return 0
        # frequently used, shouldnot pass around
        self.wordList = wordList
        self.endWord = endWord
        self.beginWord = beginWord
        self.fringe = {beginWord: self.distToEnd(beginWord)}
        self.fromWord = {beginWord: None}
        # self.fringe = MyHeap([(beginWord, self.distToEnd(beginWord))])
        # self.fringe = MyHeap([(beginWord, 0)])
        # self.fg = {beginWord: self.distToEnd(beginWord)}
        self.aStar()
        return len(self.retrievePath())
        # return self.fromWord


    def retrievePath(self):
    #     retrieve path from self.fromWord list
    #     if not self.fromWord[self.endWord]:
        if not self.endWord in self.fromWord:
            return 0
        word = self.endWord
        path = [word]
        while word != self.beginWord:
            word = self.fromWord[word]
            path.append(word)
        return path

    # def Astar(self):
    #     curWord, curDis = self.fringe.pop()[0], self.fringe.pop()[1]
    #     self.fg.pop(curWord)
    #     while self.fringe or curWord:
    #         nbrs = self.getNbr(curWord)
    #         for nbr in nbrs:
    #             # relax edge
    #             newDis = curDis + 1 + self.distToEnd(nbr)
    #             if nbr not in self.fg:
    #                 self.fringe.push((nbr, newDis))
    #                 self.fg[nbr] = newDis
    #             else:
    #                 if newDis < self.fg[nbr]:
    #                     self.fg[nbr] = newDis
    # #                     hard to update heap
    def aStar(self):
        # path = [self.beginWord]
        temp = self.popFringe()
        curWord, curDis = temp[0], temp[1]
        popped = {curWord}
        while self.fringe or curWord:
            # get endWord, no need to relax remaining edges
            if curWord == self.endWord:
                return
            nbrs = self.getNbr(curWord)
            for nbr in nbrs:
                #relax edges, should not relax nodes that has been dequeued from popped
                if nbr not in popped:
                    newDis = curDis + 1 + self.distToEnd(nbr)
                    if nbr not in self.fringe:
                        self.fringe[nbr] = newDis
                        self.fromWord[nbr] = curWord
                    else:
                        if newDis < self.fringe[nbr]:
                            self.fringe[nbr] = newDis
                            self.fromWord[nbr] = curWord
            if not self.fringe:
                return
            temp = self.popFringe()
            curWord, curDis = temp[0], temp[1]
            popped.add(curWord)
            # path.append(curWord)
        # return []


    def popFringe(self):
    #     remove the word from fringe whose dist is the smallest, return the smallest dis that is popped
        if not self.fringe:
            return None
        temp = list(self.fringe.items())
        minPair = min(temp, key=lambda x: x[1])
        self.fringe.pop(minPair[0])
        return (minPair[0], minPair[1])


    def LD(self, s, t):
        # calculate word distance
        if s == "":
            return len(t)
        if t == "":
            return len(s)
        if s[-1] == t[-1]:
            cost = 0
        else:
            cost = 1
        res = min([self.LD(s[:-1], t) + 1,
                   self.LD(s, t[:-1]) + 1,
                   self.LD(s[:-1], t[:-1]) + cost])
        return res


    def getNbr(self, word):
        # the surrounding nbr distance is always 1(plus generic dist)
        nbrs = []
        for w in self.wordList:
            if self.LD(w, word) == 1:
                nbrs.append(w)
        return nbrs


    def distToEnd(self, word):
        return self.LD(word, self.endWord)

# tricky to use, abandon
class MyHeap(object):
   def __init__(self, initial=None, key=lambda x:x[1]):
       self.key = key
       if initial:
           self._data = [(key(item), item) for item in initial]
           heapq.heapify(self._data)
       else:
           self._data = []

   def push(self, item):
       heapq.heappush(self._data, (self.key(item), item))

   def pop(self):
       return heapq.heappop(self._data)[1]

   def replace(self, item): return


s = Solution()
# s.ladderLength("hit", "cog", ["hot", "dot", "dog", "lot", "log", "cog"])
# s.ladderLength("hot", "dog", ["hot","dog"])
s.ladderLength("cet", "ism", ["kid","tag","pup","ail","tun","woo","erg","luz","brr","gay","sip","kay","per","val","mes","ohs","now","boa","cet","pal","bar","die","war","hay","eco","pub","lob","rue","fry","lit","rex","jan","cot","bid","ali","pay","col","gum","ger","row","won","dan","rum","fad","tut","sag","yip","sui","ark","has","zip","fez","own","ump","dis","ads","max","jaw","out","btu","ana","gap","cry","led","abe","box","ore","pig","fie","toy","fat","cal","lie","noh","sew","ono","tam","flu","mgm","ply","awe","pry","tit","tie","yet","too","tax","jim","san","pan","map","ski","ova","wed","non","wac","nut","why","bye","lye","oct","old","fin","feb","chi","sap","owl","log","tod","dot","bow","fob","for","joe","ivy","fan","age","fax","hip","jib","mel","hus","sob","ifs","tab","ara","dab","jag","jar","arm","lot","tom","sax","tex","yum","pei","wen","wry","ire","irk","far","mew","wit","doe","gas","rte","ian","pot","ask","wag","hag","amy","nag","ron","soy","gin","don","tug","fay","vic","boo","nam","ave","buy","sop","but","orb","fen","paw","his","sub","bob","yea","oft","inn","rod","yam","pew","web","hod","hun","gyp","wei","wis","rob","gad","pie","mon","dog","bib","rub","ere","dig","era","cat","fox","bee","mod","day","apr","vie","nev","jam","pam","new","aye","ani","and","ibm","yap","can","pyx","tar","kin","fog","hum","pip","cup","dye","lyx","jog","nun","par","wan","fey","bus","oak","bad","ats","set","qom","vat","eat","pus","rev","axe","ion","six","ila","lao","mom","mas","pro","few","opt","poe","art","ash","oar","cap","lop","may","shy","rid","bat","sum","rim","fee","bmw","sky","maj","hue","thy","ava","rap","den","fla","auk","cox","ibo","hey","saw","vim","sec","ltd","you","its","tat","dew","eva","tog","ram","let","see","zit","maw","nix","ate","gig","rep","owe","ind","hog","eve","sam","zoo","any","dow","cod","bed","vet","ham","sis","hex","via","fir","nod","mao","aug","mum","hoe","bah","hal","keg","hew","zed","tow","gog","ass","dem","who","bet","gos","son","ear","spy","kit","boy","due","sen","oaf","mix","hep","fur","ada","bin","nil","mia","ewe","hit","fix","sad","rib","eye","hop","haw","wax","mid","tad","ken","wad","rye","pap","bog","gut","ito","woe","our","ado","sin","mad","ray","hon","roy","dip","hen","iva","lug","asp","hui","yak","bay","poi","yep","bun","try","lad","elm","nat","wyo","gym","dug","toe","dee","wig","sly","rip","geo","cog","pas","zen","odd","nan","lay","pod","fit","hem","joy","bum","rio","yon","dec","leg","put","sue","dim","pet","yaw","nub","bit","bur","sid","sun","oil","red","doc","moe","caw","eel","dix","cub","end","gem","off","yew","hug","pop","tub","sgt","lid","pun","ton","sol","din","yup","jab","pea","bug","gag","mil","jig","hub","low","did","tin","get","gte","sox","lei","mig","fig","lon","use","ban","flo","nov","jut","bag","mir","sty","lap","two","ins","con","ant","net","tux","ode","stu","mug","cad","nap","gun","fop","tot","sow","sal","sic","ted","wot","del","imp","cob","way","ann","tan","mci","job","wet","ism","err","him","all","pad","hah","hie","aim","ike","jed","ego","mac","baa","min","com","ill","was","cab","ago","ina","big","ilk","gal","tap","duh","ola","ran","lab","top","gob","hot","ora","tia","kip","han","met","hut","she","sac","fed","goo","tee","ell","not","act","gil","rut","ala","ape","rig","cid","god","duo","lin","aid","gel","awl","lag","elf","liz","ref","aha","fib","oho","tho","her","nor","ace","adz","fun","ned","coo","win","tao","coy","van","man","pit","guy","foe","hid","mai","sup","jay","hob","mow","jot","are","pol","arc","lax","aft","alb","len","air","pug","pox","vow","got","meg","zoe","amp","ale","bud","gee","pin","dun","pat","ten","mob"])
# mh = MyHeap([('a',2),('c',1),('e',4)])
# s.getNbr("lot")
