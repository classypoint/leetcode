# https://leetcode.com/problems/regular-expression-matching/
# '.'Matches any single character.
# '*' Matches zero or more of the [preceding] element.
# compared to Q44
class Solution:

    def isMatch2(self, text, pattern):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """
        def match(text, pattern):
            # If the pattern is empty, then only an empty string can match
            if not pattern: return not text
            # checks if current char in string is equal to current char in pattern
            first_match = bool(text) and pattern[0] in {text[0], '.'}
            return first_match and match(text[1:], pattern[1:])
        if not pattern:
            return not text
        first_match = bool(text) and pattern[0] in {text[0], '.'}
        if len(pattern) >= 2 and pattern[1] == '*':
            # if there are 0 occurences of the stars char, skip over it.
            return (self.isMatch2(text, pattern[2:]) or
            # there is an instance of the star's preceding characting.
            first_match and self.isMatch2(text[1:], pattern))
        else:
            # since there was no star, check isFirstMatchand then rest of string
            return first_match and self.isMatch2(text[1:], pattern[1:])

    def isMatch3(self, s, p):
        # DP, iteration
        m, n = len(s), len(p)
        f = [[False] * (n + 1) for _ in range(m + 1)]
        f[0][0] = True
        # p[0.., j - 3, j - 2, j - 1] matches empty iff p[j - 1] is '*' and p[0..j - 3] matches empty
        for j in range(2, n + 1):
            f[0][j] = p[j - 1] == '*' and f[0][j - 2];

        for i in range(1, m + 1):
            for j in range(1, n + 1):
                if p[j - 1] != '*':
                    f[i][j] = f[i - 1][j - 1] and (s[i - 1] == p[j - 1] or p[j - 1] == '.')
                else:
                    f[i][j] = f[i][j - 2] or ((s[i - 1] == p[j - 2] or p[j - 2] == '.') and f[i - 1][j])

        return f[m][n]


    def isMatch(selfself, s, p):
#         dynamic programming d[i][j] represents if s[:i] with p[:j](exclusive)
# 1. if p[j] == s[i]: dp[i][j] = dp[i-1][j-1]
# 2. if p[j] == '.', dp[i][j] = dp[i-1][j-1]
# 3. if p[j] == '*':
#           (1) if p[j-1] != s[i], dp[i][j] == dp[i][j-2] //a* counts as empty
#           (2) if p[j-1] == s[i] or p[i-1] == '.':
#                   dp[i][j] = dp[i-1][j]//a* counts as multiple a
#               or  dp[i][j] = dp[i][j-1] //a* counts as single a
#               or  dp[i][j] = dp[i][j-2] // a* counts as empty
        return False
s = Solution()
# s.isMatch('acb', 'a.b')
# s.isMatch('aaab', 'a*b')
# s.isMatch2('acb', 'a*cb')
# s.isMatch2('abb', '.*b')
s.isMatch3('xaa', 'xa*b')
# True as .* -> .. -> ab