# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    # return a new lst, do not change l1 and l2
    def mergeTwoLists(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        dummy = ListNode(-1)
        node = dummy

        while l1 and l2:
            if l1.val <= l2.val:
                node.next = ListNode(l1.val)
                l1 = l1.next
            else:
                node.next = ListNode(l2.val)
                l2 = l2.next
            node = node.next

        while l1:
            node.next = ListNode(l1.val)
            l1 = l1.next
            node = node.next

        while l2:
            node.next = ListNode(l2.val)
            l2 = l2.next
            node = node.next

        return dummy.next
    # in-place
    def mergeTwoLists2(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        dummy = ListNode(-1)
        node = dummy

        while l1 and l2:
            if l1.val <= l2.val:
                tmp = l1.next
                node.next = l1
                l1 = tmp
            else:
                tmp = l2.next
                node.next = l2
                l2 = tmp
            node = node.next

        if l1:
            node.next = l1

        elif l2:
            node.next = l2

        return dummy.next

s = Solution()
l1 = ListNode(1)
l1.next = ListNode(2)
l1.next.next = ListNode(4)
l2 = ListNode(1)
l2.next = ListNode(3)
l2.next.next = ListNode(5)

s.mergeTwoLists2(l1, l2)