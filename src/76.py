# https://leetcode.com/problems/minimum-window-substring/
# template
# https://leetcode.com/problems/minimum-window-substring/discuss/26808/Here-is-a-10-line-template-that-can-solve-most-'substring'-problems
class Solution(object):
    def minWindow(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: str
        """
        if not s or len(s) < len(t):
            return ""
        map = {}
        for i in t:
            if i in map:
                map[i] += 1
            else:
                map[i] = 1

        left = 0
        minLeft = 0
        minLen = len(s) + 1
        count = 0

        for right in range(len(s)):
            if s[right] in map:
                map[s[right]] -= 1
                if map[s[right]] >= 0:
                    #if map[s[right]] == -1, then we've already have enough s[right] in current window
                    count += 1
                while count == len(t):
                    if right - left + 1 < minLen:
                        minLeft = left
                        minLen = right - left + 1
                    if s[left] in map:
                        map[s[left]] += 1
                        if map[s[left]] > 0:
                            count -= 1
                    left += 1

        if minLen > len(s):
            return ""

        return s[minLeft:minLeft + minLen]

    def minWindow2(self, s, t):
        if not s or len(s) < len(t):
            return ""

        map = [0] * 128
        for i in t:
            map[ord(i)] += 1

        left = 0
        minLeft = 0
        minLen = len(s) + 1
        matched = 0

        for right in range(len(s)):
            if s[right] in t:
                map[ord(s[right])] -= 1
                if map[ord(s[right])] >= 0:
                    matched += 1

            while matched == len(t):
                curLen = right - left + 1
                if curLen < minLen:
                    minLen = curLen
                    minLeft = left
                if s[left] in t:
                    map[ord(s[left])] += 1
                    if map[ord(s[left])] > 0:
                        matched -= 1
                left += 1

        if minLen > len(s):
            return ""

        return s[minLeft: minLeft + minLen]


s = "ADOBECODEBANC"
t = "ABC"
print(Solution().minWindow2(s, t))