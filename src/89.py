class Solution(object):
    def grayCode(self, n):
        """
        :type n: int
        :rtype: List[int]
        """
        # res = []
        # self.dfs_all(n, [[0]*n], res)
        # return res

        ans = self.dfs(n, [[0]*n])

        for i in range(len(ans)):
            num = 0
            for n in ans[i]:
                num = (num << 1) + n
            ans[i] = num
        return ans

    def dfs_all(self, n, codes, res):
        if len(codes) == pow(2, n):
            res.append(codes)
            return
        for i in range(n):
            next = codes[-1][::]
            next[i] = next[i] ^ 1
            if next in codes:
                continue
            self.dfs(n, codes + [next], res)

    def dfs(self, n, codes):
        if len(codes) == pow(2, n):
            return codes
        for i in range(n):
            next = codes[-1][::]
            next[i] = next[i] ^ 1
            if next in codes: #   <-
                # if all next failed, we won't go to return line, and will return None
                continue
            return self.dfs(n, codes + [next])

    # recursion
    # https://leetcode.wang/leetCode-89-Gray-Code.html
    def grayCode2(self, n):
        res = [0]
        for i in range(n):
            extra = 1 << i
            for j in reversed(range(len(res))):
                # len(res) stays the same during iteration
                res.append(res[j] + extra)
        return res

    #Formula of Gray Code generation
    # Gi = Bi+1 ^ Bi
    def grayCode3(self, n):
        gray = []
        for binary in range(1 << n):
            gray.append(binary ^ (binary >> 1))
        return gray


print(Solution().grayCode3(3))