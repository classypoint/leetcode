# 2
def TreeHeight():
    num = int(input())
    # num = "0\t1\n'0\t2\n1\t3\n1\t4\n"
    tree = {'0':[1,0]}
    for i in range(num-1):
        parent, children = input().split()
        if parent in tree and tree[parent][1] < 2:
            tree[children] = [tree[parent][0]+1,0]
            tree[parent][1] += 1
    depth = [x[0] for x in tree.values()]
    print(max(depth))

# num = "0\t1\n'0\t2\n1\t3\n1\t4\n"
def getTreeHeight():
    num = 5
    # key: node label, [nodeHeight, nodeChildNum]
    tree = {'0': [1, 0]}
    x, y = ['0', '0', '1', '1'], ['1', '2', '3', '4']
    for i in range(num-1):
        # parent,children = input().split()
        parent, children = x[i], y[i]
        # prune invalid branch
        if parent in tree and tree[parent][1] < 2:
            tree[children] = [tree[parent][0]+1, 0]
            tree[parent][1] += 1
    depth = [x[0] for x in tree.values()]
    print(max(depth))
# getTreeHeight()

# 3
def reverseSentence():
    print(' '.join(input().split(' ')[::-1]))

def reverseSentence():
    sentence = "Hello groundhog day"
    res = ' '.join(sentence.split(' ')[::-1])
    return res
# reverseSentence()

x= list(range(501))
# x = [0,1,2,3,4,5]
def remove(x):
    while len(x) > 1:
        y = []
        for i in x:
            if x.index(i) % 2 == 0:
                y.append(i)
        for j in y:
            x.remove(j)
remove(x)