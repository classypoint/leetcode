# https://leetcode.com/problems/edit-distance/
import heapq

class Solution(object):
    #     A*
    def minDistance(self, word1, word2):
        """
        :type word1: str
        :type word2: str
        :rtype: int
        """
        pathTo = {}
        dist = {word1: 0}
        fringe = []
        visited = []
        heuristic = {}

        heapq.heappush(fringe, (0, word1))

        while not fringe:
            curWord = heapq.heappop()[1]

            if curWord == word2:
                break

            visited.append(curWord)
            for nbr in self.getAdj(curWord):
                if nbr not in heuristic:
                    heuristic[nbr] = self.getDistance(nbr, word2)
                newDist = dist[curWord] + 1 + heuristic[nbr]
                if nbr not in visited:
                    # if nbr not in visited and ((curWord not in nbr) or (nbr != pathTo[curWord])):
                    if (nbr not in dist) or (dist[nbr] < newDist):
                        if nbr not in dist:
                            heapq.heappush(fringe, (newDist, nbr))
                        else:
                            heapq.heapreplace(fringe, (newDist, nbr))
                        dist[nbr] = newDist
                        pathTo[nbr] = curWord

        return self.getPathLen(word2, pathTo)

    def getDistance(self, s1, s2):
        '''return the distance between two words'''
        if len(s1) < len(s2):
            return self.getDistance(s2, s1)

        # len(s1) >= len(s2)
        if len(s2) == 0:
            return len(s1)

        previous_row = range(len(s2) + 1)
        for i, c1 in enumerate(s1):
            current_row = [i + 1]
            for j, c2 in enumerate(s2):
                insertions = previous_row[
                                 j + 1] + 1  # j+1 instead of j since previous_row and current_row are one character longer
                deletions = current_row[j] + 1  # than s2
                substitutions = previous_row[j] + (c1 != c2)
                current_row.append(min(insertions, deletions, substitutions))
            previous_row = current_row

        return previous_row[-1]

    def getAdj(self, word):
        '''return a list of words that are nbrs of input word'''
    #     TOO MANY POSSIBILITIES, FAILED

    def getPathLen(word, pathTo):
        l = 0
        while word in pathTo:
            l += 1
            word = pathTo[word]
        return l

    #     DP-iteration
    def minDistance2(self, word1, word2):
        """
        :type word1: str
        :type word2: str
        :rtype: int
        """
        m = len(word1)
        n = len(word2)

        # dp[i][j] minimal distance from word1[:i] to word2[:j]
        dp = [[0 for j in range(n + 1)] for i in range(m + 1)]

        for i in range(m + 1):
            dp[i][0] = i
        for j in range(n + 1):
            dp[0][j] = j

        for i in range(1, m + 1):
            for j in range(1, n + 1):
                if word1[i - 1] == word2[j - 1]:
                    dp[i][j] = dp[i - 1][j - 1]
                else:
                    dp[i][j] = min(dp[i][j - 1], dp[i - 1][j], dp[i - 1][j - 1]) + 1

        return dp[m][n]

#     DP-recursion
    def minDistance3(self, word1, word2):
        def distance_helper(i, j):
            if i == 0:
                return j
            if j == 0:
                return i
            if word1[i-1] == word2[j-1]:
                return distance_helper(i-1, j-1)
            else:
                return min(distance_helper(i, j-1), distance_helper(i-1, j), distance_helper(i-1, j-1)) + 1

        return distance_helper(len(word1), len(word2))



s = Solution()
# s.getDistance("rorse", "rose")
# s.getDistance("rorse", "tose")
# s.minDistance2("horse", "ros")
print(s.minDistance2("horse", "ros"))