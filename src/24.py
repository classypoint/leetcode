# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    # recursion
    def swapPairs(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        if not head or not head.next:
            return head
        node = head.next
        head.next = self.swapPairs(head.next.next)
        node.next = head
        return node

    # pre -> a -> b -> b.next to pre -> b -> a -> b.next
    def swapPairs2(self, head):
        # pre, pre.next = self, head
        pre = self
        # ##add next attribute to self (which refers to
        # the object instance itself, i.e.,below s)
        pre.next = head
        while pre.next and pre.next.next:
            a = pre.next
            b = a.next
            pre.next, b.next, a.next = b, a, b.next
            # ##pass by reference, just rebind pre,
            # self.next is still b and won't change again
            pre = a
        return self.next

    def swapPairs3(self, head):
        dummy = ListNode(-1)
        cur = dummy
        dummy.next = head

        while not cur.next and not cur.next.next:
            first = cur.next
            second = cur.next.next
            first.next = second.next
            second.next = first
            cur.next = second
            cur = cur.next.next

        return dummy.next

s = Solution()
l = ListNode(1)
l.next = ListNode(2)
l.next.next = ListNode(3)
l.next.next.next = ListNode(4)
p = s.swapPairs3(l)
while p:
    print(p.val)
    p = p.next
