class Solution(object):
    def jump1(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        # o(n^2)
        # d[i] is the least num of steps needed to reach ith position
        n = len(nums)
        # d = [0 for i in range(n)]
        d = [0] * n

        for i in range(1, n):
            step = n
            for j in range(i):
                if j + nums[j] >= i and d[j] + 1 < step:
                    step = d[j] + 1
            d[i] = step

        return d[n - 1]

    def jump2(self, nums):
        # from right to left, always find the leftmost item within reach
        # if nums = [1, 1, 1, 1...] will be o(n^2)
        # proof: the steps taken to reach ith pos <= jth, i <= j
        pos = len(nums) - 1
        step = 0
        while pos != 0:
            for i in range(pos):
                if nums[i] >= pos - i:
                    pos = i
                    step += 1
                    break

        return step

    # note that the level order of item and order in num is corresponding
    def jump3(self, nums):
    # O(n) BFS, nodes i in level i are all the nodes that can be
    # reached in i-1th jump
        n = len(nums)
        if n < 2: return 0
        level = 0
        currentMax = 0
        i = 0
        nextMax = 0

        while currentMax - i + 1 > 0:#nodes count of current level>0
            level += 1
            # for i in range(currentMax+1):
            while i <= currentMax:
                # traverse current level, and update the max reach of next level
                nextMax = max(nextMax, nums[i] + i)
                if nextMax >= n-1:
                    # if last element is in level+1, then the min jump = level
                    return level
                i += 1
            currentMax = nextMax

        return 0

    # https://leetcode.com/problems/jump-game-ii/discuss/18028/O(n)-BFS-solution
    # https://leetcode.wang/leetCode-45-Jump-Game-II.html
    def jump(self, nums):
        steps, curEnd, curFarthest = 0, 0, 0
        for i in range(len(nums) - 1):
            curFarthest = max(curFarthest, i + nums[i])
            if i == curEnd:
                steps += 1
                curEnd = curFarthest
        return steps


s = Solution()
res = s.jump([2, 3, 1, 1, 4])
print(res)
# s.jump([1, 1, 1, 1])
