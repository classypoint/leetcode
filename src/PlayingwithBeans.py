class Solution:
    def playwithBeans(self, n, p, x):
        res = []
        def dfs(path, cur, count):
            if count == 0:
                if cur == p:
                    res.append(path)
                return
            # if we should stop the first time bean is passed back to p
            if count < x and cur == p:
                res.append(path)
            #     then we need a break here
            for i in range(1, n+1):
                # if cur == p:
                #     if i != cur and (i % cur == 0 or cur % i == 0):
                #         dfs(path+[i], i, count-1)
                # elif i % cur == 0 or cur % i == 0:
                #     dfs(path+[i], i, count-1)
                if i != cur and (i % cur == 0 or cur % i == 0):
                    dfs(path+[i], i, count-1)
        dfs([], p, x)
        return res

s = Solution()
# s.playwithBeans(3, 2, 2)
s.playwithBeans(10, 3, 4)