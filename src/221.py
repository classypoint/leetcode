class Solution(object):
    def maximalSquare(self, matrix):
        """
        :type matrix: List[List[str]]
        :rtype: int
        """
        #         dp[i][j] stores the maximal square length whose right-bottom endpoint is at (i, j)
        maxLen = 0
        if not matrix:
            return maxLen
        n = len(matrix)
        m = len(matrix[0])
        # dp = [[matrx[i][j] for j in range(len(matrix[0]))] for i in range(len(matrix))]
        dp = [[0 for j in range(m)] for i in range(n)]
        for i in range(n):
            for j in range(m):
                if i == 0 or j == 0:
                    dp[i][j] = int(matrix[i][j])
                    maxLen = maxLen if maxLen > dp[i][j] else dp[i][j]
                    continue
                if matrix[i][j] == '1':
                    if dp[i-1][j-1] == 0:
                        dp[i][j] = 1
                    elif dp[i - 1][j] >= dp[i - 1][j - 1] and dp[i][j - 1] >= dp[i - 1][j - 1]:
                        dp[i][j] = dp[i - 1][j - 1] + 1
                    else:
                        dp[i][j] = min(dp[i - 1][j], dp[i][j - 1]) + 1
                    maxLen = maxLen if maxLen > dp[i][j] else dp[i][j]

        return maxLen*maxLen

s = Solution()
s.maximalSquare([["1","0","1","0","0"],["1","0","1","1","1"],["1","1","1","1","1"],["1","0","0","1","0"]])