class Solution(object):
    def strStr(self, haystack, needle):
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """
        l1, l2 = len(haystack), len(needle)
        if l2 == 0: return 0

        for i in range(l1 - l2 + 1):
            for j in range(l2):
                if needle[j] != haystack[i+j]:
                    break
                if j == l2 - 1:
                    return i
        return -1

    def strStr2(self, haystack, needle):
        for i in range(len(haystack) - len(needle)+1):
            if haystack[i:i+len(needle)] == needle:
                return i
        return -1

s = Solution()
print(s.strStr("abcdesf", "abcdesf"))