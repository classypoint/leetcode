class Solution(object):
    #     note "" is 0 but " " is 1
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        if not s: return 0

        visited = dict()
        start = 0
        end = 0
        maxLen = 0

        while end < len(s):
            if s[end] not in visited or visited[s[end]] == -1:
                visited[s[end]] = end
                maxLen = end - start + 1 if end - start + 1 > maxLen else maxLen
            else:
                start = visited[s[end]] + 1
                # clean visited
                for v in visited:
                    if visited[v] < start:
                        # visited.pop(v)
                        visited[v] = -1
                visited[s[end]] = end
            end += 1

        return maxLen

s = Solution()
s.lengthOfLongestSubstring("tmmzuxt")
s.lengthOfLongestSubstring("")
s.lengthOfLongestSubstring(" ")
s.lengthOfLongestSubstring("au")