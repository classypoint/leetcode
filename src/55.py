class Solution(object):
    def canJump(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        n = len(nums)
        curEnd, curFurthest = 0, 0

        for i in range(n):
            curFurthest = max(curFurthest, i + nums[i])
            # if curFurthest == n - 1: break

            if i == curEnd:
                # haven't arrived at the end but cannot jump any further
                if curFurthest == curEnd and curEnd != n - 1: return False
                curEnd = curFurthest

        return True

    def canJump2(self, nums):
        maxPos = 0
        n = len(nums)

        for i in range(n):
            if maxPos < i: return False
            maxPos = max(maxPos, i + nums[i])

        return True

print(Solution().canJump2([1, 0]))