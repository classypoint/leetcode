def firstMissingPositive(nums):
    """
    :type nums: List[int]
    :rtype: int
     Basic idea:
    1. for any array whose length is l, the first missing positive must be in range [1,...,l+1],
        so we only have to care about those elements in this range and remove the rest.
    2. we can use the array index as the hash to restore the frequency of each number within
         the range [1,...,l+1]
    """
    nums.append(0)
    n = len(nums)
    for i in range(len(nums)): #delete those useless elements
        if nums[i]<0 or nums[i]>=n:
            nums[i]=0
    for i in range(len(nums)): #use the index as the hash to record the frequency of each number
        nums[nums[i]%n]+=n
    for i in range(1,len(nums)):
        if nums[i]//n==0:
            return i
    return n
# firstMissingPositive([3, 4, -1, 1])


def firstMissingPositive2(nums):
    """
    :type nums: List[int]
    :rtype: int
    """
    if not nums: return 1
    lst = list(range(1, len(nums) + 2))
    # remove appeared num from lst
    for num in nums:
        if num in lst:
            lst.remove(num)
    return lst[0]


def firstMissingPos(nums):
    # Put each number in its right place.
    # For example:
    # When we find 5, then swap it with A[4].
    # At last, the first place where its number is not right, return the place + 1.
    n = len(nums)
    for i in range(n):
        curr = nums[i]
        while curr > 0 and curr <= n and nums[curr - 1] != curr:
            next = nums[curr - 1]
            nums[curr - 1] = curr
            curr = next

    for i in range(n):
        if nums[i] != i + 1: return i + 1

    return n + 1
firstMissingPos([-1, 4, 1, 3])

def generate(numRows):
    """
    :type numRows: int
    :rtype: List[List[int]]
    """
    triangle = []
    if numRows == 0:
        return triangle
    if numRows == 1:
        return [[1]]
    if numRows == 2:
        return [[1], [1, 1]]
    for i in range(3, numRows + 1):

        row = []
        for j in range(i):
            if j == 0 or j == i - 1:
                row.append(j)
            else:
                row.append(triangle[i - 1][j - 1] + triangle[i - 1][j])
        triangle.append(row)
    return triangle
# generate(4)
