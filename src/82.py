# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    # iterative
    def deleteDuplicates(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        if not head:
            return head
        dummy = ListNode(-1)
        dummy.next = head
        pre = dummy
        cur = head
        while cur:
            while cur.next and cur.val == cur.next.val:
                cur = cur.next
            if pre.next is cur:
                pre = pre.next
            else:
                pre.next = cur.next
            cur = cur.next
        return dummy.next

    #recursive
    def deleteDuplicates2(self, head):
        if not head: return head

        if head.next and head.next.val == head.val:
            while head.next and head.next.val == head.val:
                head = head.next
            return self.deleteDuplicates2(head.next)

        head.next = self.deleteDuplicates2(head.next)
        return head


def getL():
    l = ListNode(1)
    l.next = ListNode(1)
    l.next.next = ListNode(1)
    l.next.next.next = ListNode(2)
    l.next.next.next.next = ListNode(3)
    l.next.next.next.next.next = ListNode(3)
    l.next.next.next.next.next.next = ListNode(4)
    return l

def printList(lst):
    while lst:
        print(lst.val)
        lst = lst.next

l = getL()
r = Solution().deleteDuplicates2(l)
printList(r)
#we are modifying l in-place, l and r can only be different at the front
