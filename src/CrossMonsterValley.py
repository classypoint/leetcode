class Solution:
    def __init__(self):
        self.cost = 0

    def calCoin(self, n, forces, coins):
        """
        Calculate minimum number of coins needed
        :param n:
        :param forces: List[Int] force of each monster
        :param coins: coin needed to bribe each monster
        :return: Int
        """
        # res = 0
        # power = 0
        # # greedy doesnot work
        # for i in range(n):
        #     if power < forces[i]:
        #         power += forces[i]
        #         res += coins[i]
        # return res
        self.cost = sum(coins)
        def dfs(i, curForce, curCost):
            if i == n:
                if curCost < self.cost:
                    self.cost = curCost
                return
            if curForce < forces[i]:
                dfs(i+1, curForce+forces[i], curCost+coins[i])
            else:
                dfs(i+1, curForce, curCost)
                dfs(i+1, curForce+forces[i], curCost+coins[i])
        dfs(0, 0, 0)
        return self.cost


s = Solution()
print(s.calCoin(3, [8, 5, 10], [1, 1, 2]))#2
print(s.calCoin(4, [1, 2, 4, 8], [1, 2, 1, 2]))#6
print(s.calCoin(4, [8, 5, 12, 15], [3, 4, 6, 5]))#6