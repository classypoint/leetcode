# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def generateTrees(self, n):
        """
        :type n: int
        :rtype: List[TreeNode]
        """
        # self.res = []
        # self.treeHelper2(1, n + 1)
        # return self.res

        return self.treeHelper(1, n)

    # WRONG
    def treeHelper2(self, left, right):
        if left == right:
            return None
        for m in range(left, right):
            root = TreeNode(m)
            root.left = self.treeHelper(left, m)
            root.right = self.treeHelper(m + 1, right)
            self.res.append(root)

    def treeHelper(self, start, end):
        # return a list of trees
        subs = []
        if start > end:
            subs.append(None)
            return subs
        if start == end:
            subs.append(TreeNode(start))
            return subs
        for m in range(start, end+1):
            left = self.treeHelper(start, m-1)
            right = self.treeHelper(m+1, end)

            for lnode in left:
                for rnode in right:
                    root = TreeNode(m)
                    root.left = lnode
                    root.right = rnode
                    subs.append(root)
        # DONOT FORGET RETURN HERE!
        return subs

    def generateTrees2(self, n):
        def helper(l, r, memo):
            if l > r:
                return [None]

            if (l, r) in memo:
                return memo[(l, r)]

            res = []
            for i in range(l, r + 1):
                lTrees = helper(l, i - 1, memo)
                rTrees = helper(i + 1, r, memo)
                for lNode in lTrees:
                    for rNode in rTrees:
                        root = TreeNode(i)
                        root.left = lNode
                        root.right = rNode
                        res.append(root)
            memo[(l, r)] = res
            return res

        if n == 0:
            return None

        memo = {}
        res = helper(1, n, memo)
        return res

s = Solution()
print(s.generateTrees(0))