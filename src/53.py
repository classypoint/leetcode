class Solution(object):
    def maxSubArray(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        #         dp: dp[i] represents the largest subarray sum ends at nums[i]
        # dp[i] = max(nums[i], nums[i] + dp[i - 1]
        # OR dp[i] = nums[i] + dp[i - 1] if dp[i-1] > 0 else nums[i]
        prevSum = nums[0]
        maxSum = prevSum

        for i in range(1, len(nums)):
            prevSum = prevSum + nums[i] if prevSum > 0 else nums[i]
            maxSum = max(maxSum, prevSum)

        return maxSum