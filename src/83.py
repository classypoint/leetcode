# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def deleteDuplicates(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        if not head or not head.next:
            return head
        dummy = ListNode(0)
        dummy.next = head
        ptr1, ptr2 = dummy, head
        while ptr2.next:
            if ptr2.val != ptr2.next.val:
                ptr1.next = ptr2
                # ptr1 = ptr1.next
                ptr1 = ptr2
            ptr2 = ptr2.next
        ptr1.next = ptr2
        return dummy.next

    def deleteDuplicates2(self, head):
        if not head: return head

        d = ListNode(0)
        p = d
        d.next = head
        c = head

        while c:
            while c.next and c.next.val == c.val:
                c = c.next
            if p.next is c:
                p = p.next
            else:
                p.next = c
                p = c
            c = c.next

        return d.next

    def deleteDuplicates3(self, head):
        cur = head
        while cur and cur.next:
            # delete next node if equal
            if cur.next.val == cur.val:
                cur.next = cur.next.next
            # if not equal move forward
            else:
                cur = cur.next
        return head

sol = Solution()
l = ListNode(1)
l.next = ListNode(1)
l.next.next = ListNode(2)

def printList(lst):
    while lst:
        print(lst.val)
        lst = lst.next

r = sol.deleteDuplicates2(l)
printList(r)

