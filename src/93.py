class Solution(object):
    # note 01.1.001.1  is invalid
    def restoreIpAddresses(self, s):
        """
        :type s: str
        :rtype: List[str]
        """
        res = []
        self.dfs(s, 0, "", res, 0)
        return res

    def dfs(self, s, start, path, res, count):
        if len(s) - start > 3 * (4 - count):
            return

        if start == len(s):
            if count == 4:
                res.append(path[1:])
            return

        # if start > len(s) or count == 4:
        if count == 4:
            return

        # add 1 digit
        self.dfs(s, start + 1, path + '.' + s[start], res, count + 1)

        if s[start] == '0':
            return

        # add 2 digits
        if start + 1 < len(s):
            self.dfs(s, start + 2, path + '.' + s[start:start+2], res, count + 1)

        # add 3 digits
        if start + 2 < len(s):
            num = int(s[start: start + 3])
            if 0 <= num <= 255:
                self.dfs(s, start + 3, path + '.' + s[start: start + 3], res, count + 1)

    def restoreIpAddresses2(self, s):
        def isValid(str):
            if len(str) == 0 or len(str) > 3 or (str[0] == '0' and len(str) > 1) or int(str) > 255:
                return False
            return True

        ret = []
        for i in range(1, min(4, len(s)-2)):
            for j in range(i+1, min(i+4, len(s)-1)):
                for k in range(j+1, min(j+4, len(s))):
                    s1, s2, s3, s4 = s[:i], s[i:j], s[j:k], s[k:]
                    if isValid(s1) and isValid(s2) and isValid(s3) and isValid(s4):
                        ret.append(s1+'.'+s2+'.'+s3+'.'+s4)

        return ret



s1 = "25525511135"
s2 = "0110011"
print(Solution().restoreIpAddresses2(s2))
