# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    #     (1)use slicing
    #     (2)use index as pointer (can store pos using hashmap)
    # (1)
    def buildTree2(self, preorder, inorder):
        """
        :type preorder: List[int]
        :type inorder: List[int]
        :rtype: TreeNode
        """
        if not preorder:
            return None

        #         build root
        root = TreeNode(preorder[0])

        if len(preorder) == 1:
            return root

        #         build sub-tree
        idx = inorder.index(preorder[0])#==> get index does lots of redundant work
        # ==> slicing takes up too much space
        left = self.buildTree(preorder[1:idx + 1], inorder[:idx])
        right = self.buildTree(preorder[idx + 1:], inorder[idx + 1:])

        root.left = left
        root.right = right
        return root


#     (2)pointer
    def buildTree(self, preorder, inorder):
        inMap = {}
        for i, v in enumerate(inorder):
            inMap[v] = i

        def build(preStart, inStart, subLen):
        # def build(prePtr, preLen, inPtr, inLen): same Len
            if subLen <= 0:
                return None

            rootVal = preorder[preStart]
            root = TreeNode(rootVal)
            if subLen == 1:
                return root

            left = build(preStart+1, inStart, inMap[rootVal]-inStart)
            right = build(preStart+inMap[rootVal]-inStart+1, inStart+1, subLen-inMap[rootVal]+inStart-1)
            root.left = left
            root.right = right
            return root

        return build(0, 0, len(preorder))


