class Solution(object):
    def solve(self, board):
        """
        :type board: List[List[str]]
        :rtype: None Do not return anything, modify board in-place instead.
        """
        if not board:
            return

        row = len(board)
        col = len(board[0])

        def dfs(i, j):
            if i < 0 or i >= row or j < 0 or j >= col:
                return
            if board[i][j] == 'O':
                board[i][j] = '1'
                dfs(i, j-1)
                dfs(i, j+1)
                dfs(i-1, j)
                dfs(i+1, j)
        # first check border and flip all O on border and connected to border to 1
        for j in range(col):
            if board[0][j] == 'O':
                dfs(0, j)
            if board[row-1][j] == 'O':
                dfs(row-1, j)
        for i in range(row):
            if board[i][0] == 'O':
                dfs(i, 0)
            if board[i][col-1] == 'O':
                dfs(i, col-1)
        # flip all O to X
        for i in range(row):
            for j in range(col):
                if board[i][j] == 'O':
                    board[i][j] = 'X'
        #             merge to up
        # flip all 1 back to O
        for i in range(row):
            for j in range(col):
                if board[i][j] == '1':
                    board[i][j] = 'O'

s = Solution()
# b = [["X","X","X","X"],["X","O","O","X"],["X","X","O","X"],["X","O","X","X"]]
b = [["X","O","X"],["O","X","O"],["X","O","X"]]
s.solve(b)
print(b)