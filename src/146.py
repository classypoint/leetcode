class LRUCache(object):

    def __init__(self, capacity):
        """
        :type capacity: int
        """
        self.map = dict()
        self.capacity = capacity
        self.size = 0
        # self.head = None  # ListNode if our listnode must contain key
        self.head = ListNode(-1, -1)
        # self.head.next is first item
        self.last = None

    def moveToEnd(self, node):
        '''move node to the last of list'''
        if node != self.last:
            #  connecting prev and next
            node.prev.next = node.next
            node.next.prev = node.prev
            #  add to rear
            self.last.next = node
            node.prev = self.last
            self.last = node
            node.next = None

#         we need a doubly linked list to update order for get operation
    def get(self, key):
        """
        :type key: int
        :rtype: int
        """
        if key in self.map:
            cur = self.map[key]
            # if cur != self.last:
            #     #   updating order
            #     #  connecting prev and next
            #     cur.prev.next = cur.next
            #     cur.next.prev = cur.prev
            #     #  add to rear
            #     self.last.next = cur
            #     cur.prev = self.last
            #     self.last = cur
            #     cur.next = None
            self.moveToEnd(cur)
            return cur.val
        return -1

    def put(self, key, value):
        """
        :type key: int
        :type value: int
        :rtype: None
        """
        node = ListNode(key, value)
        # if first item
        if self.size == 0:
            self.head.next = node
            node.prev = self.head
            self.last = node
            self.map[key] = node
            self.size += 1
        # adding a key
        elif key not in self.map:
            self.map[key] = node
            self.last.next = node
            node.prev = self.last
            self.last = node
            if self.size < self.capacity:
                self.size += 1
            else:
                #our head cannot be key ==> self.head = self.map[head].next
                self.head = self.head.next
                del self.map[self.head.key]
        # updating the val of exising key
        else:
            cur = self.map[key]
            cur.val = value
            # if cur != self.last:
            #     #   updating order
            #     #  connecting prev and next
            #     cur.prev.next = cur.next
            #     cur.next.prev = cur.prev
            #     #  add to rear
            #     self.last.next = cur
            #     cur.prev = self.last
            #     self.last = cur
            #     cur.next = None
            self.moveToEnd(cur)


class ListNode(object):
    def __init__(self, key, val):
        self.key = key
        self.val = val
        self.next = None

# Your LRUCache object will be instantiated and called as such:
# obj = LRUCache(capacity)
# param_1 = obj.get(key)
# obj.put(key,value)
cache = LRUCache(2)
# cache.put(1, 1)
# cache.put(2, 2)
# cache.get(1)
# cache.put(3, 3)
# cache.get(2)
# cache.put(4, 4)
# cache.get(1)
# cache.get(3)
# cache.get(4)
cache.put(2, 1)
cache.put(2, 2)
cache.get(2)
cache.put(1, 1)
cache.put(4, 1)
cache.get(2)