# https://leetcode.com/problems/word-break/discuss/43808/Simple-DP-solution-in-Python-with-description
class Solution:
    def word_break2(s, words):
        d = [False] * len(s)
        for i in range(len(s)):
            for w in words:
                if w == s[i-len(w)+1:i+1] and (d[i-len(w)] or i-len(w) == -1):
                    d[i] = True
        return d[-1]

    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: List[str]
        :rtype: bool
        """
        #        boolean dp[i] represents if s[:i+1] can be made up of wordDict
        dp = [False for i in range(len(s))]

        for i in range(len(dp)):
            for j in range(i + 1):
                if s[j:i + 1] in wordDict and (j == 0 or dp[j-1]):
                    dp[i] = True
                    break

        return dp[-1]
s = Solution()
s.wordBreak("leetcode", ["leet","code"])