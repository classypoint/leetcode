# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def insertionSortList(self, head):#will not change the head outside
        """
        :type head: ListNode
        :rtype: ListNode
        """
        if not head or not head.next:
            return head
        sorted = ListNode(63)
        sorted.next = ListNode(head.val)
        head = head.next
        while(head):
            insertNode = ListNode(head.val)
            self.insertion(sorted, insertNode)
            head = head.next
        return sorted.next
    #(in-place)insert node into sorted list starting from head(non-empty)
    def insertion(self, prev, node):
        #starting at the end, then swap backward
        #starting at the front, then insert forward
        head = prev.next
        while node.val >= head.val:
            prev = head
            head = head.next
            if not head:
                break
        prev.next = node
        node.next = head

class Solution2:
    def insertionSortList(self, head):
        cur = dummy = ListNode(0)
        while head:
            if cur and cur.val > head.val:  # reset pointer only when new number is smaller than pointer value
                cur = dummy
            while cur.next and cur.next.val < head.val:  # classic insertion sort to find position
                cur = cur.next
            cur.next, cur.next.next, head = head, cur.next, head.next  # insert
        return dummy.next


sol = Solution()

l = ListNode(4)
l.next = ListNode(2)
l.next.next = ListNode(1)
l.next.next.next = ListNode(3)
s = sol.insertionSortList(l)

s2 = Solution2()
s2.insertionSortList(l)