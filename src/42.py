def trap(height):
    """
    :type height: List[int]
    :rtype: int
    """
    ans = 0
    for i in range(1, len(height) - 1):
        leftMax, rightMax = 0, 0
        # for j in reversed(list(range(j)))
        for j in range(i):
            leftMax = max(leftMax, height[j])
        for j in range(i + 1, len(height)):
            rightMax = max(rightMax, height[j])
        ans += max(0, min(leftMax, rightMax) - height[i])
    return ans

# trap([0,1,0,2,1,0,1,3,2,1,2,1])


def trap2(height):
    """
    :type height: List[int]
    :rtype: int
    """
    ans = 0
    leftMaxArray, rightMaxArray = [], []
    leftMax, rightMax = 0, 0
    for i in range(len(height)):
        leftMax = max(height[i], leftMax)
        # leftMaxArray[i] = leftMax
        leftMaxArray.append(leftMax)
    # for x in height:
    #     leftMax = max(x, leftMax)
    #     leftMax.append(leftMax)
    for y in reversed(height):
        rightMax = max(y, rightMax)
        rightMaxArray.append(rightMax)
    rightMaxArray = list(reversed(rightMaxArray))
    for i in range(len(height)):
        ans += max(0, min(leftMaxArray[i], rightMaxArray[i]) - height[i])
    return ans
# trap2([0,1,0,2,1,0,1,3,2,1,2,1])



def trap3(height):
    """
    instead of calculating area by height*width, we can think it in a cumulative way.
    In other words, sum water amount of each bin(width=1).
    Search from left to right and maintain a max height of left and right separately,
    which is like a one-side wall of partial container. Fix the higher one and flow water from the lower part.
    For example, if current height of left is lower, we fill water in the left bin.
    Until left meets right, we filled the whole container.
    """
    n = len(height)
    left, right = 0, n - 1
    res = 0
    maxLeft, maxRight = 0, 0

    while left <= right:
        if height[left] <= height[right]:
            if height[left] >= maxLeft: maxLeft = height[left]
            else: res += maxLeft - height[left]
            left += 1
        else:
            if height[right] >= maxRight: maxRight = height[right]
            else: res += maxRight - height[right]
            right -= 1

    return res
trap3([0,1,0,2,1,0,1,3,2,1,2,1])


# https://leetcode.com/problems/trapping-rain-water/discuss/17391/Share-my-short-solution.
def trap4(height):
    left, right = 0, len(height) - 1
    maxLeft, maxRight = 0, 0
    res = 0

    while left <= right:
        maxLeft = max(maxLeft, height[left])
        maxRight = max(maxRight, height[right])

        if maxLeft < maxRight:
            # If leftMax is smaller, use left bar as current container rim
            res += maxLeft - height[left]
            left += 1
        else:
            res += maxRight - height[right]
            right -= 1

    return res
