class Solution:
    def minEatingSpeed(self, piles, H):
        """
        :type piles: List[int]
        :type H: int
        :rtype: int
        """
        K = max(sum(piles)//H, 1)#find minimum K
        h = 0
        for pile in piles:
            if pile % K!=0:
                h += (pile//K + 1)
            else:
                h += pile//K
        while (h > H):
            K += 1
            h = 0
            for pile in piles:
                if pile % K!=0:
                    h += (pile//K + 1)
                else:
                    h += pile//K
        return K