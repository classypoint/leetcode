# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def removeNthFromEnd(self, head, n):
        """
        :type head: ListNode
        :type n: int
        :rtype: ListNode
        """
        length = self.size(head)
        sentinel = ListNode(63)
        sentinel.next = head
        ptr = sentinel
        for i in range(length - n):
            ptr = ptr.next
        ptr.next = ptr.next.next
        return sentinel.next
    def size(self, head):
        if not head.next:
            return 1
        return 1 + self.size(head.next)


    def removeNthFromEnd2(self, head, n):
        dummy = ListNode(-1)
        dummy.next = head
        fast, slow = dummy, dummy

        for i in range(n - 1):
            fast = fast.next

        while fast:
            fast = fast.next
            slow = slow.next
        slow.next = slow.next.next

        return dummy.next

s = Solution()
l = ListNode(1)
p = l
for i in range(2, 6):
    p.next = ListNode(i)
    p = p.next
s.removeNthFromEnd2(l, 2)