class Solution(object):
    # >>> TLE >>>
    def uniquePathsWithObstacles(self, obstacleGrid):
        """
        :type obstacleGrid: List[List[int]]
        :rtype: int
        """
        self.count = 0
        if obstacleGrid[-1][-1] == 1 or obstacleGrid[0][0] == 1: return self.count

        self.backtrace(obstacleGrid, 0, 0, len(obstacleGrid) - 1, len(obstacleGrid[0]) - 1)
        return self.count

    def backtrace(self, grid, row, col, m, n):
        if row == m and col == n:
            self.count += 1
            return
        # DOWN
        if row < m and grid[row + 1][col] == 0:
            self.backtrace(grid, row + 1, col, m, n)
        # RIGHT
        if col < n and grid[row][col + 1] == 0:
            self.backtrace(grid, row, col + 1, m, n)

    def uniquePathsWithObstacles2(self, obstacleGrid):
        # dp[i][j] stores # of ways to reach grid[i][j]
        # dp = 0 if unreachable
        m, n = len(obstacleGrid), len(obstacleGrid[0])
        dp = [[0]*n for _ in range(m)]

        # start at obstacle
        if obstacleGrid[0][0] == 1: return 0
        dp[0][0] = 1
        # filling first row
        for j in range(1, n):
            if obstacleGrid[0][j] == 0 and dp[0][j-1] != 0:
                dp[0][j] = 1
        # filling first col
        for i in range(1, m):
            if obstacleGrid[i][0] == 0 and dp[i-1][0] != 0:
                dp[i][0] = 1
        #remaining
        for i in range(1, m):
            for j in range(1, n):
                if obstacleGrid[i][j] == 0:
                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1]

        return dp[-1][-1]


grid = [
  [0,0,0],
  [0,1,0],
  [0,0,0]
]
grid1 = [[1, 0]]
print(Solution().uniquePathsWithObstacles2(grid))