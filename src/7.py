class Solution(object):
    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        res = 0
        while x != 0:
            tail = x % 10
            newRes = res * 10 + tail
            if (newRes - tail) / 10 != res:
                return 0
            res = newRes
            x = x / 10
        return res
s = Solution()
s.reverse(-12)