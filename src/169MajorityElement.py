#Sol 1: use a dictionary to store the frequency of each element
def majorityElement(nums):
	numdic = {}
	for n in nums:
		if n not in numdic:
			numdic[n] = 1
		else:
			numdic[n] += 1
	for v in numdic:
		if numdic[v] > len(nums)/2:
			return v
print(majorityElement([3,2,3]))

#Sol 2:O(n) time and O(1) space
'''1)According to problem Major element appears more than ⌊ n/2 ⌋ times
2)Count is taken as 1 because 1 is the least possible count for any number ,
3)Major element is updated only when count is 0 which means --Array has got as many non major elements as major element
4) Check this case 1,1,3,3,5,3,3 ---Majority element is 1 initially count becomes 0 at after 2nd 3 and 5 is made as majority element with count=1
and finally 3 is made as major element.'''
def majorityElement(nums):
	major, count = nums[0], 1
	for i in range(len(nums)):
		if count == 0:
			count += 1
			major = nums[i]
		elif major == nums[i]:
			count += 1
		else:
			count -= 1
	return major

#Sol 3: one line:NOTICE that the majority element always exist in the array,so that the middle always is the answer
def majorityElement(nums):
	return sorted(nums[len(nums)/2])

#Other
# two pass + dictionary
def majorityElement1(self, nums):
    dic = {}
    for num in nums:
        dic[num] = dic.get(num, 0) + 1
    for num in nums:
        if dic[num] > len(nums)//2:
            return num
    
# one pass + dictionary
def majorityElement2(self, nums):
    dic = {}
    for num in nums:
        if num not in dic:
            dic[num] = 1
        if dic[num] > len(nums)//2:
            return num
        else:
            dic[num] += 1 

# TLE
def majorityElement3(self, nums):
    for i in xrange(len(nums)):
        count = 0
        for j in xrange(len(nums)):
            if nums[j] == nums[i]:
                count += 1
        if count > len(nums)//2:
            return nums[i]
            
# Sotring            
def majorityElement4(self, nums):
    nums.sort()
    return nums[len(nums)//2]
    
# Bit manipulation    
def majorityElement5(self, nums):
    bit = [0]*32
    for num in nums:
        for j in xrange(32):
            bit[j] += num >> j & 1
    res = 0
    for i, val in enumerate(bit):
        if val > len(nums)//2:
            # if the 31th bit if 1, 
            # it means it's a negative number 
            if i == 31:
                res = -((1<<31)-res)
            else:
                res |= 1 << i
    return res
            
# Divide and Conquer
def majorityElement6(self, nums):
    if not nums:
        return None
    if len(nums) == 1:
        return nums[0]
    a = self.majorityElement(nums[:len(nums)//2])
    b = self.majorityElement(nums[len(nums)//2:])
    if a == b:
        return a
    return [b, a][nums.count(a) > len(nums)//2]
    
# the idea here is if a pair of elements from the
# list is not the same, then delete both, the last 
# remaining element is the majority number
def majorityElement(self, nums):
    count, cand = 0, 0
    for num in nums:
        if num == cand:
            count += 1
        elif count == 0:
            cand, count = num, 1
        else:
            count -= 1
    return cand