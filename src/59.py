class Solution(object):
    def generateMatrix(self, n):
        """
        :type n: int
        :rtype: List[List[int]]
        """
        # declaration
        matrix = [[0 for _ in range(n)] for _ in range(n)]

        if n == 0:
            return matrix

        rowStart, rowEnd, colStart, colEnd = 0, n - 1, 0, n - 1
        num = 1

        # while rowStart <= rowEnd and colStart <= colEnd:
        while num <= n * n:
            for i in range(colStart, colEnd + 1):
                matrix[rowStart][i] = num
                num += 1
            rowStart += 1

            for i in range(rowStart, rowEnd + 1):
                matrix[i][colEnd] = num
                num += 1
            colEnd -= 1

            if rowStart <= rowEnd:
                for i in reversed(range(colStart, colEnd + 1)):
                    matrix[rowEnd][i] = num
                    num += 1
            rowEnd -= 1

            if colStart <= colEnd:
                for i in reversed(range(rowStart, rowEnd + 1)):
                    matrix[i][colStart] = num
                    num += 1
            colStart += 1

        return matrix

    def generateMatrix2(self, n):
        A = [[0] * n for _ in range(n)]
        i, j, di, dj = 0, 0, 0, 1
        for k in range(n * n):
            A[i][j] = k + 1
            if A[(i + di) % n][(j + dj) % n]:
                di, dj = dj, -di
            i += di
            j += dj
        return A


print(Solution().generateMatrix2(4))
