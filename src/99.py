# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    # all possible cases:
    # 1. root and left node is swapped, to recover, swap root with the biggest node in left tree
    # 2. root and right node is swapped
    # 3. left node and right node is swapped
    # 4. two left nodes are swapped 4.right
    def recoverTree(self, root):
        """
        :type root: TreeNode
        :rtype: None Do not return anything, modify root in-place instead.
        """
        if not root: return
        def getMaxOfBST(node):
            if not node:
                return node
            maxL = getMaxOfBST(node.left)
            maxR = getMaxOfBST(node.right)
            if maxL and maxL.val > node.val:
                node = maxL
            if maxR and maxR.val > node.val:
                node = maxR
            return node

        def getMinOfBST(node):
            if not node:
                return node
            minL = getMinOfBST(node.left)
            minR = getMinOfBST(node.right)
            res = node
            if minL and minL.val < res.val:
                res = minL
            if minR and minR.val < res.val:
                res = minR
            return res

        maxLeft = getMaxOfBST(root.left)
        minRight = getMinOfBST(root.right)
        # 3
        if maxLeft and minRight:
            if maxLeft.val > root.val and minRight.val < root.val:
                minRight.val, maxLeft.val = maxLeft.val, minRight.val
        if maxLeft:
            # 1
            if maxLeft.val > root.val:
                maxLeft.val, root.val = root.val, maxLeft.val

        if minRight:
            if minRight.val < root.val:
                minRight.val, root.val = root.val, minRight.val

        self.recoverTree(root.left)
        self.recoverTree(root.right)

    def recoverTree2(self, root):
        self.first = None
        self.second = None
        self.pre = None
        self.inorderTraversal(root)
        self.first.val, self.second.val = self.second.val, self.first.val

    def inorderTraversal(self, root):
        if not root:
            return
        self.inorderTraversal(root.left)
        # >>>>
        if self.pre and root.val < self.pre.val:
            # meet reverse pair for the first time
            if not self.first:
                self.first = self.pre
                self.second = root
            else:
                self.second = root
        self.pre = root
        # >>>
        self.inorderTraversal(root.right)
