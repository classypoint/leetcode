# DFS
import collections
class Solution:
    # topological sort: reverse of post-order dfs
    def findOrder(self, numCourses, prerequisites):
        # to
        dic = collections.defaultdict(set)
        # from
        neigh = collections.defaultdict(set)
        for i, j in prerequisites:
            dic[i].add(j)
            neigh[j].add(i)
        #     actually a queue...
        stack = [i for i in range(numCourses) if not dic[i]]
        # stack stores courses ready to study (all pres are studied)
        res = []
        # reverse
        while stack:
            node = stack.pop()
            res.append(node)
            for i in neigh[node]:
                dic[i].remove(node)
                if not dic[i]:
                    # preq of course i is all added to stack
                    stack.append(i)
            dic.pop(node)
        return res if not dic else []

s  = Solution()
pre = [[1,0],[2,0],[3,1],[3,2]]
# pre = [[1,0],[2,1],[1,2]]
s.findOrder(4, pre)