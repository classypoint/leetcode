class Solution(object):
    def divide(self, dividend, divisor):
        """
        :type dividend: int
        :type divisor: int
        :rtype: int
        """
        positive = (dividend < 0) is (divisor < 0)
        dividend, divisor = abs(dividend), abs(divisor)
        res = 0
        while dividend >= divisor:
            temp, i = divisor, 1
            while dividend >= temp:
                dividend -= temp
                res += i
                i <<= 1
                temp <<= 1
        if not positive:
            res = -res
        return min(max(-2147483648, res), 2147483647)

    def divide2(self, divident, divisor):
        INT_MIN = -pow(2, 31)
        # overflow
        if divident == INT_MIN and divisor == -1:
            return INT_MIN

        dvd, dvs = abs(divident), abs(divisor)
        res = 0
        sign = (divident < 0) == (divisor < 0)
        while dvd >= dvs:
            tmp, m = dvs, 1
            while (tmp << 1) <= dvd:
                tmp <<= 1
                m <<= 1
            dvd -= tmp
            res += m
        return sign*res

s = Solution()
s.divide2(8, 3)