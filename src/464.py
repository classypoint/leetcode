class Solution:
    def canIWin(self, maxChoosableInteger, desiredTotal):
        """
        :type maxChoosableInteger: int
        :type desiredTotal: int
        :rtype: bool
        """
        # if 1+2+3+....+maxChoosableInteger < desiredTotal, then no player can ever acquire desiredTotal, returning False
        if (1 + maxChoosableInteger) * maxChoosableInteger / 2 < desiredTotal:
            return False
        self.memo = {}
        return self.helper(range(1, maxChoosableInteger + 1), desiredTotal)

    def helper(self, nums, desiredTotal):
        """
        :param num:              available number pool
        :param desiredTotal:  desired total for victory
        """
        hash = str(nums)
        # this is memoization, if this subproblem has already been calculated then return directly
        if hash in self.memo:
            return self.memo[hash]
            # this is the base case of the problem. if it is my turn and the max number in the array is enough to reach the desiredTotal , then i can force a win by simply picking this max number
        if nums[-1] >= desiredTotal:
            return True
        # pick a number, if the opponent can't force a win with the rest, that means i can force a win with picking this number
        for i in range(len(nums)):
            # pick each number in the number pool, and see using that if we attain desired Total
            if not self.helper(list(nums[:i]) + list(nums[i + 1:]), desiredTotal - nums[i]):
                self.memo[hash] = True
                return True
        # #if there is no number that i can pick to force a win, then this array, total combination is false.
        self.memo[hash] = False
        return False

s = Solution()
s.canIWin(5,10)