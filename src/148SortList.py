# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None
#o(nlogn) time, o(1) memory -- heapsort
class Solution:
    def sortList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        def leftChildIndex(i):
            return 2 * i + 1
        def rightChildIndex(i):
            return 2 * i + 2
        def swap(i, j):
            heap[i], heap[j] = heap[j], heap[i]
        # sink node on index i
        def sink(i):
            if leftChildIndex(i) >= self.size and rightChildIndex(i) >= self.size:
                return
            elif leftChildIndex(i) < self.size and rightChildIndex(i) >= self.size:
                if heap[leftChildIndex(i)] > heap[i]:
                    swap(i, leftChildIndex(i))
                else:
                    return
            elif rightChildIndex(i) < self.size and leftChildIndex(i) >= self.size:
                if heap[rightChildIndex(i)] > heap[i]:
                    swap(i, rightChildIndex(i))
                else:
                    return
            else:
                if heap[leftChildIndex(i)] >= heap[rightChildIndex(i)]:
                    if heap[i] < heap[leftChildIndex(i)]:
                        swap(i, leftChildIndex(i))
                        sink(leftChildIndex(i))
                    else:
                        return
                else:
                    if heap[i] < heap[rightChildIndex(i)]:
                        swap(i, rightChildIndex(i))
                        sink(rightChildIndex(i))
                    else:
                        return
        # in-place max-heapify of input list listHeap
        def heapify():
            for i in reversed(range(self.size)):
                sink(i)
        #delete the root and put it at the end
        def remove():
            swap(0, self.size - 1)
            self.size -= 1
            sink(0)
        def heapsort():
            for i in range(num):
                remove()
        # linked list to array, starting from index 0
        heap = []
        while (head):
            heap.append(head.val)
            head = head.next
        self.size = len(heap)  # keep track of heap size
        num = len(heap)  # invariant list length
        heapify()
        heapsort()
        # array to linked list
        sorted = ListNode(0)
        ptr = sorted
        for x in heap:
            ptr.next = ListNode(x)
            ptr = ptr.next
        return sorted.next

sol = Solution()
# sol.heapify([1,3,5,2,4,7])
L = ListNode(4)
L.next = ListNode(2)
L.next.next = ListNode(1)
L.next.next.next = ListNode(3)
s = sol.sortList(L)
