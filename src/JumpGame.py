def canJump(nums):
	"""
	Sol2: Dynamic programming:Top Down
	"""
	"""
	def isGood(j):
		if (j == len(nums)-1):
			return 'G'
		elif nums[j] == 0:
			return 'B'
		else:
			for p in range(j+1, j+nums[j]+1):#min(j+nums[j],len(nums))
				if (memo[p] == 'B'):
					continue
				else:
					return 'G'
			return 'B'
	memo = [None]*len(nums)
	for i in range(len(nums)):
		pos = len(nums)-1-i
		memo[pos] = isGood(pos)
	dic = {'G':True,'B':False}
	return dic[memo[0]]
	"""
	"""Sol 4: Greedy"""
	leftmostGood = len(nums)-1
	for i in range(1,len(nums)):
		pos = len(nums)-1-i
		if nums[pos] + pos >= leftmostGood:
			leftmostGood = pos
	if leftmostGood == 0:
		return True
	else:
		return False
print(canJump([2,3,1,1,4]))
