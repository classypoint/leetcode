class Solution(object):
    # use two set to mark col and row index that need to be set to 0
    # O(m + n)
    def setZeroes(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: None Do not return anything, modify matrix in-place instead.
        """
        zeroRow, zeroCol = set(), set()
        row, col = len(matrix), len(matrix[0])
        for i in range(row):
            for j in range(col):
                if matrix[i][j] == 0:
                    zeroRow.add(i)
                    zeroCol.add(j)

        for i in zeroRow:
            matrix[i] = [0] * col

        for j in zeroCol:
            for i in range(row):
                matrix[i][j] = 0

    #use the first row and first col and mark O(1)
    def setZeroes2(self, matrix):
        firstColZero = False
        R, C = len(matrix), len(matrix[0])
        for i in range(R):
            if matrix[i][0] == 0:
                firstColZero = True
            for j in range(1, C):
                if matrix[i][j] == 0:
                    matrix[i][0] = 0
                    matrix[0][j] = 0

        for i in range(1, R):
            for j in range(1, C):
                if matrix[i][0] == 0 or matrix[0][j] == 0:
                    matrix[i][j] = 0

        if matrix[0][0] == 0:
            for j in range(C):
                matrix[0][j] = 0

        if firstColZero:
            for i in range(R):
                matrix[i][0] = 0
mtx = [
  [1,1,1],
  [1,0,1],
  [1,1,1]
]
mtx2 = [
  [0,1,2,0],
  [3,4,5,2],
  [1,3,1,5]
]
s = Solution()
s.setZeroes2(mtx2)
print(mtx2)