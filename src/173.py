# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class BSTIterator2(object):

    def __init__(self, root):
        """
        :type root: TreeNode
        """
        self.items = []
        self.inorder(root)
        self.ptr = 0

    def inorder(self, root):
        if not root:
            return
        self.inorder(root.left)
        self.items.append(root)
        self.inorder(root.right)

    def next(self):
        """
        @return the next smallest number
        :rtype: int
        """
        self.ptr += 1
        return self.items[self.ptr-1].val

    def hasNext(self):
        """
        @return whether we have a next smallest number
        :rtype: bool
        """
        return self.ptr < len(self.items)


class BSTIterator:
    # use stack to store directed left children from root; when next() be called,
    # pop one element and process its right child as new root
    # O(h) memo, haxNext() o(1), next() o(h)
    # @param root, a binary search tree's root node
    def __init__(self, root):
        self.stack = list()
        self.pushAll(root)

    # @return a boolean, whether we have a next smallest number
    def hasNext(self):
        return self.stack

    # @return an integer, the next smallest number
    def next(self):
        tmpNode = self.stack.pop()
        self.pushAll(tmpNode.right)
        return tmpNode.val

    def pushAll(self, node):
        while node is not None:
            self.stack.append(node)
            node = node.left

# Your BSTIterator object will be instantiated and called as such:
root = TreeNode(7)
root.left = TreeNode(3)
root.right = TreeNode(15)
root.right.left = TreeNode(9)
root.right.right = TreeNode(20)

root.left.left = TreeNode(1)
root.left.right = TreeNode(6)

obj = BSTIterator(root)
param_1 = obj.next()
obj.next()
param_2 = obj.hasNext()
obj.next()
obj.hasNext()
obj.next()
obj.hasNext()
obj.next()
obj.hasNext()

