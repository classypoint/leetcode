# https://leetcode.com/contest/weekly-contest-128/problems/capacity-to-ship-packages-within-d-days/
class Solution(object):
    #     IDEA
    # first try DP, do not find sub-problem
    # NOTICE that given a capacity, the minimum day to ship all items can be obtained using greedy
    # ALSO capacity must be within range(max(weights), sum(weights)), do a binary search
    #  note wee want the least capacity whose days are le D, need to find the leftmost item

    def shipWithinDays(self, weights, D):
        """
        :type weights: List[int]
        :type D: int
        :rtype: int
        """
        caps = list(range(max(weights), sum(weights) + 1))

        l = 0
        r = len(caps) - 1

        while l < r:
            mid = (l + r) // 2
            # if self.getDays(weights, caps[mid]) > D:
            #     l = mid + 1
            # elif self.getDays(weights, caps[mid]) < D:
            #     r = mid - 1
            # elif mid == 0 or caps[mid] != caps[mid-1]:
            #     return caps[mid]
            # else:
            #     r = mid - 1
            d = self.getDays(weights, caps[mid])
            if d <= D:
                r = mid
            else:
                l = mid + 1

        return caps[l]

    def getDays(self, weights, cap):
        '''return the [least] number of days needed to ship all packages with a
        capacity of cap'''
        if cap <= 0:
            return -1

        days = 0
        totalDayWeight = 0

        for weight in weights:
            newWeight = totalDayWeight + weight
            if newWeight > cap:
                days += 1
                totalDayWeight = weight
            else:
                totalDayWeight = newWeight

        return days + 1


s = Solution()
s.shipWithinDays([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 5)
# s.getDays([1, 2, 3, 1, 1], 3)
# s.getDays([3,2,2,4,1,4], 6)
# s.getDays([1,2,3,4,5,6,7,8,9,10], 15)