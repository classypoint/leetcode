class Solution:
    def __init__(self):
        self.num2letter = {'2': 'abc', '3': 'def', '4': 'ghi', '5': 'jkl',
                           '6': 'mno', '7': 'pqrs', '8': 'tuv', '9': 'wxyz'}

    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        map_ = {
            '2': 'abc',
            '3': 'def',
            '4': 'ghi',
            '5': 'jkl',
            '6': 'mno',
            '7': 'pqrs',
            '8': 'tuv',
            '9': 'wxyz'
        }
        result = []

        def make_combinations(i, cur):
            if i == len(digits):
                if len(cur) > 0:
                    result.append(''.join(cur))
                return
            for ch in map_[digits[i]]:
                cur.append(ch)
                make_combinations(i + 1, cur)
                cur.pop()

        make_combinations(0, [])

        return result

    def letterCombinations2(self, digits):
        mapping = {'2': 'abc', '3': 'def', '4': 'ghi', '5': 'jkl',
                   '6': 'mno', '7': 'pqrs', '8': 'tuv', '9': 'wxyz'}
        if len(digits) == 0:
            return []
        if len(digits) == 1:
            return list(mapping[digits[0]])
        prev = self.letterCombinations(digits[:-1])
        additional = mapping[digits[-1]]
        # res = []
        # for c in additional:
        #     for s in prev:
        #         res.append(s+c)
        # return res
        return [s + c for s in prev for c in additional]
        # return [c + s for s in additional for c in prev]


    def letterCombinations3(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        ret = []
        if not digits:  return ret

        self.dfs(0, digits, "", ret)
        return ret

    def dfs(self, start, digits, path, res):
        if start == len(digits):
            res.append(path)
            return
        for letter in self.num2letter[digits[start]]:
            self.dfs(start + 1, digits, path + letter, res)

s = Solution()
print(s.letterCombinations2('23'))