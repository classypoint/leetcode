import sys
import math
# https://leetcode.com/problems/maximum-gap/discuss/50643/bucket-sort-JAVA-solution-with-explanation-O(N)-time-and-space
# The maximum gap will be no smaller than ceiling[(max - min ) / (N - 1)]" because sum of gaps = max-min

# Let gap = ceiling[(max - min ) / (N - 1)].
# We divide all numbers in the array into n-1 buckets, where k-th bucket
# contains all numbers in [min + (k-1)gap, min + k*gap).
# Since there are n-2 numbers that are not equal min or max
# and there are n-1 buckets, at least one of the buckets are empty.
# We only need to store the largest number and the smallest number in each bucket.

# After we put all the numbers into the buckets.
# We can scan the buckets sequentially and get the max gap.
class Solution(object):
    def maximumGap2(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if not nums or len(nums) < 2:
            return 0

        minVal = min(nums)
        maxVal = max(nums)
        n = len(nums)

        # the minimum possible gap, ceiling of the integer division
        # (n + d - 1) // d
        gap = (maxVal - minVal + n - 2) // (n - 1)
        # store the min value in that bucket
        bucketsMIN = [sys.maxsize for i in range(n - 1)]
        bucketsMAX = [-sys.maxsize-1 for i in range(n - 1)]
        # put numbers into buckets
        for i in nums:
            if i == minVal or i == maxVal:
                continue
            #     index of the right position in the buckets
            idx = (i - minVal) // gap
            bucketsMIN[idx] = min(i, bucketsMIN[idx])
            bucketsMAX[idx] = max(i, bucketsMAX[idx])
        # scan the buckets for the max gap
        maxGap = -sys.maxsize - 1
        prev = minVal
        for i in range(n-1):
            if bucketsMIN[i] == sys.maxsize and bucketsMAX[i] == -sys.maxsize - 1:
                # empty bucket
                continue
            #     min value minus the previous value is the current gap
            maxGap = max(maxGap, bucketsMIN[i] - prev)
            # update previous bucket valud
            prev = bucketsMAX[i]
        # update the final max value gap
        maxGap = max(maxGap, maxVal - prev)
        return maxGap

    def maximumGap(self, num):
        if len(num) < 2 or min(num) == max(num):
            return 0
        a, b = min(num), max(num)
        size = math.ceil((b - a) / (len(num) - 1))
        bucket = [[None, None] for _ in range((b - a) // size + 1)]
        for n in num:
            b = bucket[(n - a) // size]
            b[0] = n if b[0] is None else min(b[0], n)
            b[1] = n if b[1] is None else max(b[1], n)
        bucket = [b for b in bucket if b[0] is not None]
        return max(bucket[i][0] - bucket[i - 1][1] for i in range(1, len(bucket)))

s = Solution()
s.maximumGap2([3, 6, 8, 1])
s.maximumGap2([1, 2, 4, 7])