#Sol 1: Time limit exceed
"""
def canCompleteCircuit(gas, cost):
    def start(index):
        #type int
        #rtype boolean
        #returns if starting from index is good
        tank = gas[index]
        for i in range(len(gas)):
            if index == len(gas) - 1:
                next = 0
            else:
                next = index+1
            if tank - cost[index] >= 0:
                tank = tank - cost[index] + gas[next]
                index = next
            else:
                return False
        return True
    for i in range(len(gas)):
        if start(i):
            return i
        else:
            continue
    return -1
canCompleteCircuit([3,3,4],[3,4,4])
"""
def canCompleteCircuit(gas, cost):
    start = len(gas) - 1
    end = 0
    sum = gas[start] - cost[start]
    while (start > end):
        if sum>=0 :
            sum += gas[end] - cost[end]
            end += 1
        else:
            start -= 1
            sum += gas[start] - cost[start]
    #return start if sum>=0 else -1
    if sum >= 0:
        return start
    else:
        return -1
canCompleteCircuit([1,2,3,4,5],[3,4,5,1,2])