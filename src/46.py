# https://leetcode.com/problems/permutations/
class Solution(object):
    def permute(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        res = []

        #         size: len(visited)
        def dfs(size, path, visited):
            if size == len(nums):
                res.append(path)
                return

            for i in range(len(nums)):
                if i not in visited:
                    dfs(size + 1, path + [nums[i]], visited + [i])

        def dfs2(pt, size, path, visited):
            if size == len(nums)-1:
                res.append(path + nums[pt])
                return

            newvisited = visited + [pt]
            for i in range(len(nums)):
                if i not in newvisited:
                    dfs2(i, size+1, path+[nums[pt]], newvisited)


        #       do not specify enter point in dfs
        # dfs(0, [], [])

        # sepecify
        for i in range(len(nums)):
            dfs2(i, 0, [], [])
        return res

    # >>>>>>>>>method 2>>>>>>>>> dfs with return
    def permute2(self, nums):
        # return self.dfs(nums, [])
        return self.dfs2(nums, [], 0)

    def dfs(self, nums, visited):
        """
        return the total permutations using num in nums that's not in visited
        :param nums:
        :param visited: List[int]
        :return: List[List[int]]
        """
        res = []
        if len(visited) == len(nums):
            return res
        for i in range(len(nums)):
            if i not in visited:
                combs = self.dfs(nums, visited + [i])
                if not combs:
                    # suppose we only have one element in nums, then combs is None
                    # so need to handle this condition here
                    res.append([nums[i]])
                for comb in combs:
                    res.append([nums[i]] + comb)
        return res


s = Solution()
print(s.permute2([1]))