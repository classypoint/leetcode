#Sol 1:
'''
def rotate(nums, k):
    newposition = {}
    for i, n in enumerate(nums):
        newposition[n] = (i + k) % len(nums)
    temp = list(newposition.items())
    temp = sorted(temp, key = lambda x: x[1])
    new = [x[0] for x in temp]
    return new
'''
#Sol 2: In-place
#Won't work for len(nums)%k == 0
def rotate(nums, k):
    k = k % len(nums);
    indice = 0
    temp = nums[0]
    for i in range(len(nums)):
        nums[(indice+k)%len(nums)], temp = temp, nums[(indice+k)%len(nums)]
        indice = (indice+k)%len(nums)
    return nums

#Sol 3
def rotate2(nums, k):
    n = len(nums)
    k = k % n
    temp = nums[:]
    for i in range(n):
        indice = (i+k)%n
        nums[indice] = temp[i]
    return nums
rotate2([1,2,3,4,5,6],2)