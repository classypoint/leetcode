# https://leetcode.com/problems/combination-sum/
class Solution(object):
    def combinationSum(self, candidates, target):
        """
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        candidates = sorted(candidates)
        ret = []

        # starting from position no fronter than pt, cur path sum is val, cur path is path
        # note the diff with starting from exactly pt
        def dfs(pt, val, path):
            if val > target:
                return
            if val == target:
                ret.append(path)
                return
            for i in range(pt, len(candidates)):
                # pruning, must add candidates[i]
                if val + candidates[i] > target:
                    break
                dfs(i, val + candidates[i], path + [candidates[i]])

        dfs(0, 0, [])
        return ret

    def combinationSum2(self, candidates, target):
        list.sort(candidates)
        res = []
        self.dfs_prune(candidates, target, 0, [], res)
        return res

    def dfs(self, candidates, target, start, path, res):
        if target <= 0:
            if target == 0:
                res.append(path)
            return
        for i in range(start, len(candidates)):
            self.dfs(candidates, target - candidates[i], i, path + [candidates[i]], res)
    #    faster than dfs
    def dfs_prune(self, candidates, target, start, path, res):
        if target == 0:
            res.append(path)
            return
        for i in range(start, len(candidates)):
            if target < candidates[i]:
                break
            self.dfs(candidates, target - candidates[i], i, path + [candidates[i]], res)