# >>>method 1 TLE O(n)
def mySqrt(x):
    for i in range(0,x//2+2):
        if i*i == x:
            return i
        elif i*i > x:
        	return i-1
# print(mySqrt(0))

# Newton method X(n+1) = X(n)+(S-X(n)^2)/2*X(n)
def mySqrt2(x):
	if x == 0 or x == 1:
		return x
	r1 = x
	r2 = r1+(x-r1*r1)/(2*r1)
	while(abs(r1-r2)>=1):
		#r1, r2 = r2, r1+(x-r1*r1)/(2*r1)
		r1 = r2
		r2 = r1+(x-r1*r1)/(2*r1)
	return int(r2)

# print(mySqrt(20))

# >>> Binary Search O(log(n)) >>>
def mySqrt3(x):
	l, r = 1, x
	while True:
		mid = l + (r - l) // 2
		if mid > x / mid:
			r = mid - 1
		elif mid + 1 > x / (mid + 1):
			return mid
		else:
			l = mid + 1
print(mySqrt3(20))

# still Newton
# https://math.mit.edu/~stevenj/18.335/newton-sqrt.pdf
# f(x) = x^2 - a = 0   Xn+1 = Xn - f(Xn)/f(Xn)'
# Xn+1 = 1/2 (Xn + a/Xn)
def mySqrt4(x):
	r = x
	while r * r > x:
		r = (r + x / r) / 2
	return r