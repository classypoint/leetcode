#90
class Solution(object):
    # backtracing - new
    def subsets(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        ret = []

        def dfs(ptr, path):
            if ptr == len(nums):
                ret.append(path)
                return
            dfs(ptr + 1, path)#do not contain current element
            dfs(ptr + 1, path + [nums[ptr]])# contain current element

        dfs(0, [])
        return ret

    # backtracing - modify in-place
    def subsets2(self, nums):
        ret = []

        def dfs(ptr, path):
            if ptr == len(nums):
                ret.append(path[::])#NOTE, deep copy
                return
            dfs(ptr + 1, path)
            path.append(nums[ptr])#modify path
            dfs(ptr + 1, path)
            path.remove(nums[ptr])#revert back

        dfs(0, [])
        return ret

    # backtracing
    def subsets3(self, nums):
        ans = []
        def dfs(start, path):
            ans.append(path)
            for i in range(start, len(nums)):
                dfs(i + 1, path + [nums[i]])
        dfs(0, [])
        return ans

    # backtraicing - with ret
    def subsets4(self, nums):
        if not nums:
            return []
        res = []
        tmp = self.subsets3(nums[1:])
        if not tmp:#handle empty set
            res.append(tmp)
            res.append([nums[0]])
        for sub in tmp:
            res.append(sub)
            res.append([nums[0]] + sub)
        return res

    # bit mask
    def subsets5(self, nums):
        ans = []
        bit_nums = len(nums)
        ans_nums = 1 << bit_nums #2^n
        for i in range(ans_nums):
            tmp = []
            count = 0 #digit in nums
            i_copy = i #shift
            while i_copy != 0:
                if (i_copy & 1) == 1:#if cur digit is 1
                    tmp.append(nums[count])
                count += 1
                i_copy = i_copy >> 1 #move to right
            ans.append(tmp)
        return ans


l = [1, 2, 3]
print(Solution().subsets5(l))