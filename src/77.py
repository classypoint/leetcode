class Solution(object):
    # backtracing
    def combine1(self, n, k):
        """
        :type n: int
        :type k: int
        :rtype: List[List[int]]
        """
        res = []
        self.dfs([], res, 1, n + 1, k)
        return res

    def dfs(self, path, res, l, r, k):
        if k == 0:
            res.append(path)
            return
        # if l == r:
        #     return
        # for i in range(l, r):
        for i in range(l, r-k+1):
            # self.dfs(path + [i], res, l + 1, r, k - 1)
            self.dfs(path + [i], res, i + 1, r, k - 1)

    # iteration
    # https://leetcode.com/problems/combinations/discuss/26992/Short-Iterative-C%2B%2B-Answer-8ms
    def combine2(self, n, k):
        ans = []
        path = [0] * k
        i = 0
        while i >= 0:
            # increment element at index i
            path[i] += 1
            # move index to the left if the elem at index i > n
            if path[i] > n:
                i -= 1
            # if the index is at the end of path, then (as the other
            # condition are obeyed, we know we have a valid comb, so add it to res
            elif i == k - 1:
                ans.append(path[::])
            # move index to the right and set the element at that index
            # equal to the element at the previous index
            #
            # due to the increment at the beginning of this while loop,
            # we ensure that the element at this index will be at least one more
            # than its neighbor to the left
            else:
                i += 1
                path[i] = path[i - 1]
        return ans

    # recursion
    def combine3(self, n, k):
        # C(n, k) = C(n - 1, k - 1) + C(n - 1, k)
        if n == k:
            return [list(range(1, n + 1))]
        if k == 1:
            return [[i] for i in range(1, n + 1)]

        return self.combine3(n - 1, k) + [i + [n] for i in self.combine3(n - 1, k - 1)]

    # ?dp | dfs with return
    def combine4(self, n, k):
        dp = [[] for _ in range(k + 1)]
        for i in range(1, n + 1):
            temp = dp[0][::]
            for j in range(1, min(i, k)):
                last = temp
                if not dp[j]:
                    temp = dp[j][::]
                if i <= j:
                    dp[j] = []
                for lst in last:
                    dp[j].append(lst + [i])
        return dp[k]

s = Solution()
print(s.combine4(4, 2))