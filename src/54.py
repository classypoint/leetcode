class Solution(object):
    def spiralOrder(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[int]
        """
        rowBegin, rowEnd, colBegin, colEnd = 0, len(matrix), 0, len(matrix[0])
        res = []
        while rowBegin < rowEnd and colBegin < colEnd:
            #             move right
            for j in range(colBegin, colEnd):
                res.append(matrix[rowBegin][j])
            rowBegin += 1
            #             move down
            if colBegin < colEnd:
                for i in range(rowBegin, rowEnd):
                    res.append(matrix[i][colEnd - 1])
                colEnd -= 1
            #             move left
            if rowBegin < rowEnd:
                for j in reversed(range(colBegin, colEnd)):
                    res.append(matrix[rowEnd - 1][j])
                rowEnd -= 1
            #             up
            if colBegin < colEnd:
                for i in reversed(range(rowBegin, rowEnd)):
                    res.append(matrix[i][colBegin])
                colBegin += 1

        return res

mtx = [
 [ 1, 2, 3, 4],
 [ 5, 6, 7, 8],
 [ 9, 10, 11, 12]
]
mtx2 = [[3],[2]]
print(Solution().spiralOrder(mtx2))
