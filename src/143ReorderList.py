class ListNode:
	def __init__(self, x):
		self.val = x
		self.next = None
def reorderList(head):
	#1 Find the middle of the list
	pslow = head
	pfast = head
	while (not pfast.next and not pfast.next.next):
		pslow = pslow.next
		pfast = pfast.next.next
	#2 Reverse the after after middle 1->2->3->4->5->6 to 1->2->3->6->5->4
	prev1 = pslow#3
	cur1 = pslow .next
	while (not cur1.next):
		cur2 = cur1.next
		cur1.next = cur2.next
		cur2.next = prev1.next
		prev1.next = cur2
	#3 Start reorder one by one 1->2->3->6->5->4 to 1->6->2->5->3->4
	p1 = head
	p2 = prev1.next
	while (p1 != prev1):
		prev1 = p2.next
		p2.next = p1.next
		p1.next = p2
		p1 = p2.next
		p2 = prev1.next
