# 33
class Solution(object):
    def search(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: bool
        """
        start, end = 0, len(nums) - 1
        while start <= end:
            mid = (start + end) // 2
            if target == nums[mid]: return True

            # left side is ordered
            if nums[start] < nums[mid]:
                # target here
                if target >= nums[start] and target < nums[mid]:
                    end = mid - 1
                else:
                    start = mid + 1

            elif nums[start] == nums[end]:
                start += 1

            # right side is ordered
            else:
                if target > nums[mid] and target <= nums[end]:
                    start = mid + 1
                else:
                    end = mid - 1
        return False

print(Solution().search([1, 3, 1, 1, 1, 1], 3))