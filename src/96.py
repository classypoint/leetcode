class Solution(object):
    def numTrees(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n == 0: return 0
        mem = dict()
        return self.helper(n, mem)
    # in 95, we need to return each tree, so pass in start, end
    # here we only care about num rather than explicit range, so just
    # pass in # of tree nodes
    def helper(self, n, mem):
        if n in mem:
            return mem[n]

        ans = 0
        if n == 0 or n == 1:
            return 1

        for i in range(1, n+1):
            left = self.helper(i - 1, mem)
            right = self.helper(n - i, mem)
            ans += left * right

        mem[n] = ans
        return ans

    def numTrees2(self, n):
        ans = 1
        for i in range(1, n + 1):
            ans = ans * (n + i) / i
        return int(ans / (n + 1))

print(Solution().numTrees2(3))