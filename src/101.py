# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

#Sol1: check if tree is the symmetric of itself
class Solution:
    def isSymmetric(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        def isMirrow(r1, r2):
            if (r1 == None and r2 == None):
                return True
            if (r1 == None or r2 == None):
                return False
            return (r1.val == r2.val) and isMirrow(r1.right, r2.left) and isMirrow(r1.left, r2.right)
        return isMirrow(root, root)

#Sol2: iterative