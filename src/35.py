def searchInsert(nums, target):
    """
    :type nums: List[int]
    :type target: int
    :rtype: int
    """
    lo, hi = 0, len(nums) - 1
    while lo <= hi:
        mid = lo + ((hi - lo) >> 1)
        if nums[mid] == target:
            hi = mid - 1
        elif nums[mid] < target:
            lo = mid + 1
        else:
            hi = mid - 1
    return lo


def searchInsert2(nums, target):
    """
    :type nums: List[int]
    :type target: int
    :rtype: int
    """
    lo, hi = 0, len(nums)
    while lo < hi:
        mid = lo + ((hi - lo) >> 1)
        if nums[mid] < target:
            lo = mid + 1
        else:
            hi = mid
    return lo