class Solution(object):
    def plusOne(self, digits):
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        digits = digits[::-1]
        res = []
        carry = 1

        for digit in digits:
            num = digit + carry
            res.append(num % 10)
            carry = num // 10
        if carry: res.append(carry)

        return res[::-1]

    def plusOne2(self, digits):
        for i in reversed(range(len(digits))):
            if digits[i] < 9:
                digits[i] += 1
                break
            else:
                digits[i] = 0

        if digits[0] == 0:
            digits.insert(0, 1)

        return digits


print(Solution().plusOne2([9, 9]))
