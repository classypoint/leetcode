class Solution:
    def maxSlidingWindow(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: List[int]
        """
        maxList = []
        if not nums:
            pass
        elif len(nums) < k:
            maxList.append(max(nums))
        else:
            for i in range(len(nums)-k+1):
                maxList.append(max(nums[i:i+k]))
        return maxList
s = Solution()
print(s.maxSlidingWindow([],1))