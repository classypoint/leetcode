import re
#Sol 1
class Solution:
    def countAndSay(self, n):
        """
        :type n: int
        :rtype: str
        """
        i = 1
        csay = '1'
        while i<n:
            csay = self.count(csay)
            i += 1
        return csay
    def count(self, temp):
        """
        :type temp: str
        :rtype say: str(the next sequence number)
        """
        pair = []#[(frequency,num)...]
        for x in temp:
            if pair:
                if x not in pair[-1]:
                    pair.append([1,x])#tuple does not support item assignment
                else:
                    pair[-1][0] = pair[-1][0] + 1
            else:
                pair.append([1,temp[0]])
        say = ''
        for t in pair:
            say += str(t[0])+t[1]
        return say
# print(Solution().countAndSay(6))
#Sol 2
def countAndSay(n):
    """
    :type n: int
    :rtype: str
    """
    def cns(str_):
        res = ''
        str_ += '#'
        c = 1
        for i in range(len(str_) - 1):
            if str_[i] == str_[i + 1]:
                c += 1
                continue
            else:
                res += str(c) + str_[i]
                c = 1
        return res

    start = '1'
    for i in range(n - 1):
        start = cns(start)
    return start

#Sol 3 Regular Expression
def countAndSay2(n):
    s = '1'
    for _ in range(n - 1):
        s = re.sub(r'(.)\1*', lambda m: str(len(m.group(0))) + m.group(1), s)
    return s


def countAndSay3(n):
    def helper(arr):
        buff = []
        count = 1
        for i in range(len(arr) - 1):
            if arr[i] != arr[i + 1]:
                buff.append(str(count))
                buff.append(arr[i])
                count = 1
            else:
                count += 1
        buff.append(str(count))
        buff.append(arr[-1])
        return buff

    res = ['1']
    for i in range(n - 1):
        res = helper(res)

    return ''.join(res)

print(countAndSay3(1))
print(countAndSay3(2))
print(countAndSay3(3))
print(countAndSay3(4))