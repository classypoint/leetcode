class Solution(object):
    # compare all words digit by digit -- append
    def longestCommonPrefix(self, strs):
        """
        :type strs: List[str]
        :rtype: str
        """
        res = ""

        for i in range(len(strs[0])):
            c = strs[0][i]

            for str in strs:
                if i >= len(str) or str[i] != c:
                    return res
            res += c

        return res

    # digit by digit -- remove
    def longestCommonPrefix2(self, strs):
        res = ""
        if not strs:
            return res

        res = strs[0]
        for str in strs[1:]:

            for i in range(len(res)):
                if i >= len(str) or str[i] != res[i]:
                    res = res[:i]
                    break

        return res

s = Solution()
s.longestCommonPrefix2(["flower","flow","flight"])