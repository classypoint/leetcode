# https://leetcode.com/problems/container-with-most-water/
class Solution(object):
    def maxArea1(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        # start from two ends
        i, j = 0, len(height) - 1
        water = 0
        while i < j:
            # update maxArea, note this should be at the front
            newArea = min(height[i], height[j]) * (j - i)
            water = newArea if newArea > water else water
            # if left bar is shorter, the only possible way of
            # increasing area is to increase i (move first bar to the right)
            if height[i] < height[j]:
                i += 1
            #     OPT: instead of just move one step, we move right until...
            else:
                j -= 1

        return water

    def maxArea2(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        i, j = 0, len(height) - 1
        area = 0
        while i < j:
            h = min(height[i], height[j])
            area = max((j - i) * h, area)
            if height[i]< height[j]:
                while height[i] <= h and i < j:
                    i += 1
            else:
                while height[j] <= h and i < j:
                    j -= 1
        return area

    def maxArea(self, height):
        i, j, water = 0, len(height) - 1, 0
        while i < j:
            h = min(height[i], height[j])
            water = max(water, (j - i) * h)
            # avoid writing explicitly which side is taller
            # only one of the two while's will only be excuted
            while height[i] <= h and i < j:
                i += 1
            while height[j] <= h and i < j:
                j -= 1

        return water