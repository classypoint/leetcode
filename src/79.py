class Solution:
    def exist(self, board, word):
        """
        :type board: List[List[str]]
        :type word: str
        :rtype: bool
        """
        # use a list to track used items
        def dfs(i, j, match, used):
            # i, j is the current letter row and col, match is the next substring
            # to match in word, used is used item
            if not match:
                #         nothing more to match
                return True
            if i < 0 or i == len(board) or j < 0 or j == len(board[0]):
                #         reach premise of board
                return False
            # should not return (None)
            if board[i][j] != match[0]:
                return False
            if board[i][j] == match[0]:
                # check if next element(up or down or left or right) is used or not
                up = dfs(i-1, j, match[1:], used+[(i,j)]) if (i-1,j) not in used else False
                down = dfs(i+1, j, match[1:], used+[(i,j)]) if (i+1,j) not in used else False
                left = dfs(i, j-1, match[1:], used+[(i,j)]) if (i,j-1) not in used else False
                right = dfs(i, j+1, match[1:], used+[(i,j)]) if (i,j+1) not in used else False
                return up or down or left or right
        row, col = len(board), len(board[0])
        if row*col < len(word):
            return False
        for i in range(row):
            for j in range(col):
                # dfs(board[i][j], word, [])
                if dfs(i, j, word, []):
                    return True
        return False

    def exist2(self, board, word):
    # modify current(used) element in board--need to revert it back after or use copy
        def dfs(i, j, toMatch, board):
            if not toMatch:
                return True
            if i < 0 or i == len(board) or j < 0 or j == len(board[0]):
                return False
            if board[i][j] != toMatch[0]:
                return False
            # if board[i][j] == toMatch[0]:
            temp, board[i][j] = board[i][j], None
            up = dfs(i-1, j, toMatch[1:], board)
            # up = dfs(i-1, j, toMatch[1:], board[:])
            down = dfs(i+1, j, toMatch[1:], board)
            left = dfs(i, j-1, toMatch[1:], board)
            right = dfs(i, j+1, toMatch[1:], board)
            # so that parallel <- ->...do not affect each other using board, and when
            # whole dfs exits, the board is unchanged
            board[i][j] = temp
            return up or down or left or right
        row, col = len(board), len(board[0])
        if row*col < len(word):
            return False
        for i in range(row):
            for j in range(col):
                # everytime use a new start we will get a untouched board
                # originBoard = board[::]
                if dfs(i, j, word, board):
                    return True
        return False

    def exist3(self, board, word):
        m, n = len(board), len(board[0])
        for i in range(m):
            for j in range(n):
                if board[i][j] == word[0]:
                    if self.backtrace(board, i, j, 0, word):
                        return True

        return False

    def backtrace(self, board, i, j, ptr, word):
        if ptr == len(word):
            return True
        if i < 0 or i == len(board) or j < 0 or j == len(board[0]):
            return False
        if board[i][j] != word[ptr]:
            return False
        tmp = board[i][j]
        board[i][j] = '.'
        flag = self.backtrace(board, i - 1, j, ptr + 1, word) or \
               self.backtrace(board, i + 1, j, ptr + 1, word) or \
               self.backtrace(board, i, j - 1, ptr + 1, word) or \
               self.backtrace(board, i, j + 1, ptr + 1, word)
        board[i][j] = tmp
        return flag

s = Solution()
s.exist2([
  ['A','B','C','E'],
  ['B','F','C','S'],
  ['A','D','E','E']
],'BAD')