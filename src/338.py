class Solution(object):
    def countBits2(self, num):
        """
        :type num: int
        :rtype: List[int]
        """
        res = [0]
        while len(res) <= num:
            res += [i+1 for i in res]
        return res[:num+1]

    def countBits(self, num):
        """
        :type num: int
        :rtype: List[int]
        """
        if num == 0:
            return 0

        res = [0 for i in range(num + 1)]
        res[1] = 1

        j = 2
        while j <= num:
            for i in range(j):
                res[j + i] = res[i] + 1
                if i + j == num:
                    return res
            j *= 2
s = Solution()
s.countBits2(5)
# s.countBits(1)