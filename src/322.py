class Solution:
    def coinChange(self, coins, amount):
        """
        :type coins: List[int]
        :type amount: int
        :rtype: int
        """
        res = []
        self.dfs(coins, [], amount, res)
        return res
        # return len(min(res, key = len))

    #path is current path, amount is targeted amount, res is feasible path
    #use amount- i as target each time
    # def dfs(self, coins, path, amount, res):
    #     for i in coins:
    #         if sum(path) + i == amount:
    #             path.append(i)
    #             res.append(path)
    #             return
    #         elif sum(path) + i < amount:
    #             path.append(i)
    #             self.dfs(coins, path, amount, res)
    #         else:
    #             return
    def dfs(self, coins, path, amount, res):
        for i in coins:
            if i == amount:
                # path.append(i)
                res.append(path+[i])
                return
            elif i < amount:
                # path.append(i)
                self.dfs(coins, path+[i], amount-i, res)
            else:
                # return
                pass
s = Solution()
# assert(s.coinChange([1,2,5], 11) == 3)
s.coinChange([1,2,5], 3)
s.coinChange([5,2,1], 3)


def reverseString(s):
    """
    :type s: str
    :rtype: str
    """
    if not s:
        return s
    s = list(s)
    i, j = 0, len(s) - 1
    while i != j:
        s[i], s[j] = s[j], s[i]
        i += 1
        j -= 1
    return ''.join(s)
reverseString("A man a plan a canal: Panama")