class Solution(object):
    def bitwiseComplement(self, N):
        """
        :type N: int
        :rtype: int
        """
        bits = []
        while N:
            bits.append(N & 1 ^ 1)
            N = N >> 1

        res = 0
        bits = bits[::-1]
        for bit in bits:
            res *= 2
            res += bit

        return res

    # def numPairsDivisibleBy60(self, time):
    #     """
    #     :type time: List[int]
    #     :rtype: int
    #     """
    #     pair = 0
    #     n = len(time)
    #     for i in range(n-1):
    #         for j in range(i+1, n):
    #             if (time[i] + time[j]) % 60 == 0:
    #                 pair += 1
    #     return pair

    def numPairsDivisibleBy60(self, time):
        """
        :type time: List[int]
        :rtype: int
        """
        if len(time) < 2:
            return 0

        pair = 0
        buffer = {}

        for s in time:
            if s % 60 in buffer:
                buffer[s % 60] += 1
            else:
                buffer[s % 60] = 1

        for v in buffer:
            if v == 30:
                pair += v * (v - 1) / 2
            elif v < 30 and 60 - v in buffer:
                pair += buffer[v] * buffer[60 - v]

        return pair


s = Solution()
# s.bitwiseComplement(10)
s.numPairsDivisibleBy60([30,20,150,100,40])

