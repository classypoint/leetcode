"""Sol 1: Recursion"""
"""
def hasPathSum(root, sum):
	if not root:
		return False
	if not root.left and not root.right and root.val == sum:
		return True
	sum -= root.val
	return hasPathSum(root.left, sum) or hasPathSum(root.right, sum)
"""
"""Sol 2: Iterative DFS"""
def hasPathSum(root,sum):
	if root is None:
		return False
	stack = [(root, sum)]
	while stack:
		node, _sum = stack.pop()
		if node.left is node.right is None and node.val == _sum:
			return True
		if node.left:
			stack.append(node.left,_sum-node.value)
		if node.right:
			stack.append(node.right,_sum-node.value)
	return False
"""Sol 3"""
"""
class Solution:
    # @param root, a tree node
    # @param sum, an integer
    # @return a boolean
    def hasPathSum(self, root, sum):
        # Don't assume the tree is a binary search tree - no faster code
        # Don't assume the val is non-negative, otherwise, faster algorithm to cut branch before reach leaf
        # (0) current node is a None node
        if root is None:  
            return False
        
        # Depth first search
        sum = sum - root.val
        # (1) current node is a leaf node
        if (root.left is None) and (root.right is None): # leaf node
            if sum == 0:
                return True
            else:
                return False  # for this branch, no match sum
                
        # (2) current node is NOT a leaf node
        return Solution().hasPathSum(root.left, sum) or Solution().hasPathSum(root.right, sum)
"""