class Solution(object):
    def int2bin(self, n):
        return self.int2bin(n >> 1) + [n & 1] if n > 1 else [1]

    def addBinary2(self, a, b):
        """
        :type a: str
        :type b: str
        :rtype: str
        """
        s = int(a, base=2) + int(b, base=2)
        bins = self.int2bin(s)
        bins = [str(x) for x in bins]
        return "".join(bins)


    def addBinary(self, a, b):
        i, j = len(a) - 1, len(b) - 1
        buff = []
        carry = 0
        while i >= 0 or j >= 0:
            # if there is a carry from the last addition, add it to carry
            sum = carry
            if j >= 0:
                # we subtract '0' to get the int value of the char from the ascii
                sum += ord(b[j]) - ord('0')
                j -= 1
            if i >= 0:
                sum += ord(a[i]) - ord('0')
                i -= 1
            # if sum==2 or sum==0 append 0 cause 1+1=0 in this case as this is base 2
            # (just like 1+9 is 0 if adding ints in columns)
            buff.append(str(sum & 0x1))
            # if sum==2 we have a carry, else no carry 1/2 rounds down to 0 in integer arithematic
            carry = sum >> 1
        if carry:
            # leftover carry, add it
            buff.append(str(carry))
        return "".join(buff[::-1])


s = Solution()
s.addBinary('11', '11')