class Solution(object):
    def fullJustify(self, words, maxWidth):
        """
        :type words: List[str]
        :type maxWidth: int
        :rtype: List[str]
        """
        res, cur, num_of_letters = [], [], 0
        for w in words:
            if num_of_letters + len(w) + len(cur) > maxWidth:
                if len(cur) == 1:
                    res.append(cur[0] + ' ' * (maxWidth - num_of_letters))
                else:
                    num_spaces = maxWidth - num_of_letters
                    space_between_words, num_extra_spaces = divmod(num_spaces, len(cur) - 1)
                    for i in range(num_extra_spaces):
                        cur[i] += ' '
                    res.append((' ' * space_between_words).join(cur))
                cur, num_of_letters = [], 0
            cur += [w]
            num_of_letters += len(w)
        res.append(' '.join(cur) + ' ' * (maxWidth - num_of_letters - len(cur) + 1))
        return res


words = ["This", "is", "an", "example", "of", "text", "justification."]
w2 = ["What","must","be","acknowledgment","shall","be"]
w3 = ["Science","is","what","we","understand","well","enough","to","explain",
         "to","a","computer.","Art","is","everything","else","we","do"]
print(Solution().fullJustify(words, 16))
# print(Solution().fullJustify(w2, 16))
# print(Solution().fullJustify(w3, 20))