class Solution:
    def hIndex(self, citations):
        """
        :type citations: List[int]
        :rtype: int
        """
        # citations.sort()#this will affect original citations
        l = sorted(citations, reverse=True)
        i = 0
        while(i < len(l) and l[i] >= i):
            i += 1
        return i