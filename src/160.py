# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def nodeToList(self, head):
        res = []
        while head:
            res.append(head.val)
            head = head.next
        return res
    def get(self, root, n):
        if n == 0:
            return root
        return self.get(root.next, n-1)
        # for i in range(n):
        #     root = root.next
        # return root
    def getIntersectionNode(self, headA, headB):
        """
        :type head1, head1: ListNode
        :rtype: ListNode
        """
        if not headA or not headB:
            return None
        lstA = self.nodeToList(headA)
        lstB = self.nodeToList(headB)
        flag = True
        if len(lstA) > len(lstB):
            # temp, lstA, lstB = headB, lstB, lstA
            lstA, lstB = lstB, lstA
            flag = False
        i, j = len(lstA) - 1, len(lstB) - 1
        while i >= 0:
            if lstA[i] == lstB[j]:
                i -= 1
                j -= 1
            else:
                break
        # inter = self.get(temp, i+1)
        inter = self.get(headA, i+1) if flag else self.get(headB, i+1)
        return inter if i < len(lstA)-1 else None
s = Solution()
h2 = ListNode(2)
h2.next = ListNode(7)
h1 = ListNode(1)
h1.next = ListNode(3)
h1.next.next = ListNode(5)
h1.next.next.next = ListNode(7)
ans = s.getIntersectionNode(h1, h2)

