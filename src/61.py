# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    # one way is to first get list length l, then reconnect from l - k th node
    # OR we can use fast | slow pointer to get the last kth node directly
    def rotateRight(self, head, k):
        """
        :type head: ListNode
        :type k: int
        :rtype: ListNode
        """
        if not head: return head
        l = self.getListLen(head)
        k %= l
        if k == 0 or l <= 1: return head

        dummy = ListNode(-1)

        # reconnect
        tail = self.getNthNode(head, l)
        start = self.getNthNode(head, l - k)
        dummy.next = start.next
        start.next = None
        tail.next = head

        return dummy.next

    def getListLen(self, head):
        l = 0
        while head:
            head = head.next
            l += 1
        return l

    def getNthNode(self, head, n):
        for i in range(n - 1):
            head = head.next
        return head

l = ListNode(1)
l.next = ListNode(2)
Solution().rotateRight(l, 0)