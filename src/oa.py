import sys


def swap(nums, i, j):
    nums[i], nums[j] = nums[j], nums[i]


def getReverse(nums):
    # use insertion sort
    pair = 0
    for i in range(1, len(nums)):
        cur = i
        while cur > 0 and nums[cur] < nums[cur - 1]:
            swap(nums, cur, cur - 1)
            pair += 1
            cur -= 1
    return pair


# if __name__ == "__main__":
#     shelves = {}
#     n = int(sys.stdin.readline().strip())
#     k = int(sys.stdin.readline().strip())
#     for i in range(n):
#         line = sys.stdin.readline().strip()
#         shelf = list(map(int, line.split()))
#         # a list of list, representing all layers of shelf
#         shelves[i] = shelf
#     # get rever pair nums for each layer
shelves = {0:[1, 3, 2], 1:[2, 1, 0]}
revDict = {}
for i in shelves:
    pair = getReverse(shelves[i].copy())
    revDict[i] = pair
# sort layer id according to num of reversed pairs
order = list(revDict.keys())
order = sorted(order, key=lambda x: revDict[x])
# form new order
ans = []
for i in order:
    ans.append(shelves[i])
print(ans)
