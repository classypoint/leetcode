class Solution(object):
    #recursion
    def isScramble(self, s1, s2):
        """
        :type s1: str
        :type s2: str
        :rtype: bool
        """
        if s1 == s2:
            return True
        # Sanity Check
        if len(s1) != len(s2):
            return False
        # If # of chars are the same
        count_buff = [0] * 26
        for i in range(len(s1)):
            count_buff[ord(s1[i]) - 97] += 1
            count_buff[ord(s2[i]) - 97] -= 1
        for c in count_buff:
            if c != 0:
                return False

        for i in range(1, len(s1)):#if i==0, we are not reducing the problem, dead loop
            if self.isScramble(s1[:i], s2[:i]) and self.isScramble(s1[i:], s2[i:]):
                return True
            if self.isScramble(s1[i:], s2[:len(s2) - i]) and self.isScramble(s1[:i], s2[len(s2) - i:]):
                return True
        return False

    #with memorization
    def isScramble2(self, s1, s2):
        mem = dict()
        return self.helper(s1, s2, mem)

    def helper(self, s1, s2, mem):
        k = s1 + "#" + s2
        if k in mem: return mem[k]

        if s1 == s2:
            mem[k] = True
            return True

        if len(s1) != len(s2):
            # mem[k] = False
            return False

        count_buff = [0] * 26
        for i in range(len(s1)):
            count_buff[ord(s1[i]) - 97] += 1
            count_buff[ord(s2[i]) - 97] -= 1
        for c in count_buff:
            if c != 0:
                mem[k] = False
                return False

        for i in range(1, len(s1)):
            if self.helper(s1[i:], s2[i:], mem) and self.helper(s1[:i], s2[:i], mem):
                mem[k] = True
                return True
            if self.helper(s1[i:], s2[:len(s2) - i], mem) and self.helper(s1[:i], s2[len(s2) - i:], mem):
                mem[k] = True
                return True

        mem[k] = False
        return False

print(Solution().isScramble2("great", "rgeat"))