# Definition for an interval.
class Interval:
	def __init__(self, s=0, e=0):
		self.start = s
		self.end = e

def merge(intervals):
    """
    :type intervals: List[Interval]
    :rtype: List[Interval]
    """
    intervals.sort(key=lambda x: x.start)
    merged = []
    for interval in intervals:
    	if not merged or merged[-1].end < interval.start:
    		merged.append(interval)
    	else:
    		merged[-1].end = max(merged[-1].end, interval.end)
    return merged

# def merge2(inter1,inter2):
# 	if inter2.end < inter1.start or inter2.start > inter1.end:
# 		return [inter1, inter2]
# 	else:
# 		inter = Interval(min(inter1.start,inter2.start),max(inter1.end,inter2.end))
# 		return inter


class Solution(object):
	def merge(self, intervals):
		"""
		:type intervals: List[List[int]]
		:rtype: List[List[int]]
		"""
		res = []
		intervals.sort(key=lambda x: x[0])

		for interval in intervals:
			# disjoint intervals
			if not res or res[-1][1] < interval[0]:
				res.append(interval)
			else:
				# ovelapping intervals
				res[-1][1] = max(res[-1][1], interval[1])

		return res

