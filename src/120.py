#Sol 1: Top down-Failed #use for to restrict loop time, not if to judge
def minimumTotal(triangle):
	def dfs(startrow, startcol,path,sum):
		if startrow >= len(triangle):
			sum.append(path)
			if startcol == len(triangle[len(triangle)-1]):
				sum.append(path)
				return min(sum)
			return
		#else:
			#path += triangle[startrow][startcol]#can't change path here
		dfs(startrow+1,startcol,path+triangle[startrow][startcol],sum)
		dfs(startrow+1, startcol+1, path+triangle[startrow][startcol],sum)
	return dfs(0,0,0,[])
minimumTotal([[2],[3,4]])

#Sol 2: DP-Bottom up
'''
def minimumTotal(self, triangle):
    f = [0] * (len(triangle) + 1)
    for row in triangle[::-1]:
        for i in xrange(len(row)):
            f[i] = row[i] + min(f[i], f[i + 1])
    return f[0]]
'''