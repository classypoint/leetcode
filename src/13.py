# https://leetcode.com/problems/roman-to-integer/
class Solution(object):
    def romanToInt(self, s):
        """
        :type s: str
        :rtype: int
        """
        map = {'I': 1, 'IV': 4, 'V': 5, 'IX': 9, 'X': 10, 'XL': 40,
               'L': 50, 'XC': 90, 'C': 100, 'CD': 400, 'D': 500, 'CM': 900, 'M': 1000}

        i = 0
        n = len(s)
        res = 0

        while i < n:
            if i < n - 1 and s[i:i + 2] in map:
                res += map[s[i:i + 2]]
                i += 2
            else:
                res += map[s[i]]
                i += 1

        return res


    def romanToInt2(self, s):
        roman = {'M': 1000, 'D': 500, 'C': 100, 'L': 50, 'X': 10, 'V': 5, 'I': 1}
        z = 0
        for i in range(0, len(s) - 1):
            if roman[s[i]] < roman[s[i + 1]]:
                z -= roman[s[i]]
            else:
                z += roman[s[i]]
        return z + roman[s[-1]]

s = Solution()
s.romanToInt('IV')
s.romanToInt('MCMXCIV')