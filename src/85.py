class Solution:
    def largestRectangleArea(self, heights):
        """
        :type heights: List[int]
        :rtype: int
        """
        # mid = heights.index(min(heights))
        # self.threePart(heights[:mid], heights[mid+1:])
        return self.threePart(heights)

    # def threePart(self, left, mid, right):
    def threePart(self, heights):
        if not heights:
            return 0
        val = min(heights)
        mid = heights.index(val)
        left = heights[:mid]
        right = heights[mid + 1:]
        # if not left and not right:
        #     return val
        # if not left:
        #     return max(val*(len(right)+1), self.threePart(right))
        return max(val * len(heights), self.threePart(left), self.threePart(right))

    def maximalRectangle(self, matrix):
        """
        :type matrix: List[List[str]]
        :rtype: int
        """
        if not matrix:
            return 0

        maxArea = 0
        col = len(matrix[0])
        lessFromLeft = [-1] * col
        lessFromRight = [col] * col
        heights = [0] * col

        for i in range(len(matrix)):
            #update height of current level
            for j in range(col):
                if matrix[i][j] == '1':
                    heights[j] += 1
                else:
                    heights[j] = 0
            # update lessFromLeft
            boundary = -1 #last 0
            for j in range(col):
                if matrix[i][j] == '1':
                    lessFromLeft[j] = max(lessFromLeft[j], boundary)
                else:
                    lessFromLeft[j] = -1
                    boundary = j
            # update lessFromRight
            boundary = col
            for j in reversed(range(col)):
                if matrix[i][j] == '1':
                    lessFromRight[j] = min(lessFromRight[j], boundary)
                else:
                    lessFromRight[j] = col
                    boundary = j
            # update all area
            for j in reversed(range(col)):
                area = (lessFromRight[j] - lessFromLeft[j] - 1) * heights[j]
                maxArea = max(area, maxArea)

        return maxArea



s = Solution()
# s.largestRectangleArea([2,1,5,6,2,3])
mtx = [["1","0","1","0","0"],["1","0","1","1","1"],["1","1","1","1","1"],["1","0","0","1","0"]]
print(s.maximalRectangle(mtx))