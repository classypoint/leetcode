# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
#Sol 1:  Top Down
#check the heights of the two sub trees are no bigger than 1, and both left sub tree
# and right sub tree are also balanced
'''
class Solution:
    def isBalanced(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        if root == None:
        	return True
        left = depth(root.left)
        right = depth(root.right)
        return abs(left - right) <= 1 and isBalanced(root.left) and isBalanced(root.right)

    def depth(rootnode):
    	if rootnode == None:
    		return 0
    	return max(depth(rootnode.left), depth(rootnode.right)) + 1 
'''
#Sol 2: Bottom up
class Solution:
	def dfsHeight(self, rootnode):
		if root == None:
			return 0
		leftHeight = self.dfsHeight(rootnode.left)
		if leftHeight == -1:
			return -1
		rightHeight = self.dfsHeight(rootnode.right)
		if rightHeight == -1:
			return -1
		if (abs(leftHeight - rightHeight) > 1):
			return -1
		return max(leftHeight, rightHeight) + 1
	def isBalanced(self, root):
		return self.dfsHeight(root)!= -1
#Sol 2.1:
'''
class Solution(object):
    def isBalanced(self, root):
            
        def check(root):
            if root is None:
                return 0
            left  = check(root.left)
            right = check(root.right)
            if left == -1 or right == -1 or abs(left - right) > 1:
                return -1
            return 1 + max(left, right)
            
        return check(root) != -1
'''
#Sol 3: Iterative
class Solution(object):
    def isBalanced(self, root):
        stack, node, last, depths = [], root, None, {}
        while stack or node:
            if node:
                stack.append(node)
                node = node.left
            else:
                node = stack[-1]
                if not node.right or last == node.right:
                    node = stack.pop()
                    left, right  = depths.get(node.left, 0), depths.get(node.right, 0)
                    if abs(left - right) > 1: return False
                    depths[node] = 1 + max(left, right)
                    last = node
                    node = None
                else:
                    node = node.right
        return True