#Sol 1--Exceed Time Limit
def climbStairs(n):
    #dp[i] = dp[i-1]+dp[i-1] The last step before reaching the final one is either n-1 or n-2
    if n == 1:
        return 1
    if n == 2:
        return 2
    return climbStairs(n-1) + climbStairs(n-2)

#Sol 2
def climbStairs2(n):
    if n < 3: return n
    b1, b2 = 1, 2
    for i in range(n - 2):
        b1, b2 = b2, b1 + b2
    return b2
# climbStairs(35)

# faster than 2
def climbStairs3(n):
    dp = [0] * n
    if n > 0:
        dp[0] = 1
    if n > 1:
        dp[1] = 2
    for i in range(2, n):
        dp[i] = dp[i - 1] + dp[i - 2]

    return dp[-1]