import math

class Solution(object):
    # >>> method 1 TLE >>>
    def getPermutation(self, n, k):
        """
        :type n: int
        :type k: int
        :rtype: str
        """
        perms = []
        self.dfs(n, [], perms)
        return perms[k - 1]

    # return all permutations in ascending order
    # we do not need a start as we can use num in any order
    # here path also records visited num
    def dfs(self, n, path, res):
        if len(path) == n:
            res.append(path)

        for i in range(n):
            if i+1 not in path:
                self.dfs(n, path + [i+1], res)

    # >>> method 2 >>> grouping
    def getPermutation2(self, n, k):
        nums = list(range(1, n + 1))
        factorial = 1
        for i in range(n):
            factorial *= (i + 1)
        ans = []
        k -= 1

        for _ in reversed(range(1, n+1)):
            factorial //= len(nums)
            groupNum = k // factorial
            num = nums[groupNum]
            nums.pop(groupNum)#index not object
            k %= factorial
            ans.append(num)

        return ans

    def getPermutation3(self, n, k):
        nums = list(map(str, range(1, 10)))
        k -= 1
        factorial = [1] * n
        for i in range(1, n):
            factorial[i] = factorial[i - 1] * i
        res = []
        for i in range(n):
            index = k // factorial[n - 1 - i]
            res.append(nums[index])
            nums.remove(nums[index])
            k = k % factorial[n - 1 - i]
        return ''.join(res)

    def getPermutation4(self, n, k):
        numbers = list(range(1, n+1))
        permutation = ''
        k -= 1
        while n > 0:
            n -= 1
            # get the index of current digit
            index, k = divmod(k, math.factorial(n))
            permutation += str(numbers[index])
            # remove handled number
            numbers.remove(numbers[index])

        return permutation


print(Solution().getPermutation4(3, 3))
# ans = []
# Solution().dfs(2, [], ans)
# print(ans)