class Solution(object):
    def canThreePartsEqualSum(self, A):
        """
        :type A: List[int]
        :rtype: bool
        """
        if not A or len(A) < 3:
            return False
        s = sum(A)
        if s % 3 != 0:
            return False
        # for i in range(len(A) - 2):
        #     for j in range(i+1, len(A) - 1):
        #         if sum(A[:i+1]) == s / 3 and sum(A[i+1:j+1]) == s / 3:
        #             return True
        # return False
        dp = [A[0] for i in range(len(A))]
        for i in range(1, len(dp)):
            dp[i] = dp[i-1] + A[i]
        for i in range(len(dp)-2):
            if dp[i] == s // 3:
                for j in range(i+1, len(dp) - 1):
                    if dp[j] == 2*s // 3:
                        return True
        return False

    def smallestRepunitDivByK(self, K):
        """
        :type K: int
        :rtype: int
        """
    def maxScoreSightseeingPair(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        # res = 0
        # for i in range(len(A)-1):
        #     for j in range(i+1, len(A)):
        #         if A[i] + A[j] +i - j> res:
        #             res = A[i] + A[j] + i - j
        # return res
        i = 0
        j = len(A) - 1
        left = i
        right = j
        while i < j:
            if A[i] > A[left] + left - i:
                left = i
            if A[j] > A[right] + j - right:
                right = j
            i += 1
            j -= 1
        return A[left] + A[right] + left - right


s = Solution()
s.canThreePartsEqualSum([0,2,1,-6,6,-7,9,1,2,0,1])
# s.maxScoreSightseeingPair([8,1,5,2,6])
s.maxScoreSightseeingPair([2, 2, 2])