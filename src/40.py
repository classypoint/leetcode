class Solution:
    # use HashSet to eliminate duplicate results
    def combinationSum2_slow(self, candidates, target):
        """
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        res = []
        list.sort(candidates)

        def dfs(ptr, curSum, path):
            if curSum >= target:
                if curSum == target and path not in res:
                    res.append(path)
                return
            if ptr == len(candidates):
                return
            for i in range(ptr, len(candidates)):
                dfs(i + 1, curSum + candidates[i], path + [candidates[i]])

        dfs(0, 0, [])
        return res


    def combinationSum(self, candidates, target):
        # Sorting is really helpful, se we can avoid over counting easily
        candidates.sort()
        result = []
        self.combine_sum(candidates, 0, [], result, target)
        return result

    def combine_sum(self, nums, start, path, result, target):
        # Base case: if the sum of the path satisfies the target, we will consider
        # it as a solution, and stop there
        if not target: #target == 0
            result.append(path)
            return

        for i in range(start, len(nums)):
            # Very important here! We don't use `i > 0` because we always want
            # to count the first element in this recursive step even if it is the same
            # as one before. To avoid overcounting, we just ignore the duplicates
            # after the first element.
            if i > start and nums[i] == nums[i - 1]:
                continue

            # If the current element is bigger than the assigned target, there is
            # no need to keep searching, since all the numbers are positive
            if nums[i] > target:
                break

            # We change the start to `i + 1` because one element only could
            # be used once
            self.combine_sum_2(nums, i + 1, path + [nums[i]],
                               result, target - nums[i])

    # basically the same as combinationSum, just move the helper function as an
    # inner function
    def combinationSum2(self, candidates, target):
        """
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        candidates.sort()
        res = []
        def dfs(start, count, path, res):
            if count == target:
                res.append(path)
                return
            for i in range(start, len(candidates)):
                if count + candidates[i] > target:
                    break
                if i != start and candidates[i] == candidates[i - 1]:
                    continue
                dfs(i + 1, count + candidates[i], path + [candidates[i]], res)
        dfs(0, 0, [], res)
        return res

s = Solution()
# s.combinationSum2([10, 1, 2, 7, 6, 1, 5], 8)
# s.combinationSum2([2, 5, 2, 1, 2], 5)
# s.combinationSum2([2, 2, 2, 1, 3], 4)
s.combinationSum2([1, 1, 1], 2)