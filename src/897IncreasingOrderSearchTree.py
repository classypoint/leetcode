class TreeNode:
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None
#Sol 1
'''
def increasingBST(root):
	#first inorder, then reconstruct with only right subtree
	def dfs(node, l = []):
		 if node.left:
		 	dfs(node.left, l)
		 l.append(node.val)
		 if node.right:
		 	dfs(node.right, l)
		 return l
	l = dfs(root)
	node = TreeNode(l[0])
	cur = node
	for i in l[1:]:
		rightsub = TreeNode(i)
		cur.right = rightsub
		cur = rightsub
	return node
'''
#Sol 2
def increasingBST(root):
	#traverse in increasing order
	def inorder(node):
		if node:
			yield from inorder(node.left)
			yield node.val
			yield from inorder(node.right)
	ans = cur = TreeNode(None)
	for v in inorder(root):
		cur.right = cur = TreeNode(v)
		#cur.right = TreeNode(v)
		#cur = cur.right
	return ans.right
