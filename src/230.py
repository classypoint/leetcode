# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
# inorder traversal BST, the right order of an element is the number of items to the left of it
class Solution(object):
    def kthSmallest(self, root, k):
        """
        :type root: TreeNode
        :type k: int
        :rtype: int
        """
        #         inorder traversal first k elements
        # return self.inorder(root, k, 0)
        self.res = 0
        self.count = 0
        self.inorder(root, k, 0)
        # self.inorder2(root, k)
        return self.res

    # can you spot the WRONG?
    # the count will be reset everytime we return to the outter recursion, so need to use global
    def inorder(self, root, k, count):
        if not root:
            return

        self.inorder(root.left, k, count)

        count += 1
        if count == k:
            # return root.val
            self.res = root.val
        # res.append(root.val)

        self.inorder(root.right, k, count)

    def inorder2(self, root, k):
        if self.count > k:
            return

        if not root:
            return

        self.inorder2(root.left, k)

        self.count += 1
        if self.count == k:
            # return root.val
            self.res = root.val
        # res.append(root.val)

        self.inorder2(root.right, k)

s = Solution()
root = TreeNode(3)
root.left = TreeNode(1)
root.left.right = TreeNode(2)
root.right = TreeNode(4)

root.left.left = TreeNode(5)
root.left.left.left = TreeNode(6)
root.left.left.right = TreeNode(7)
s.kthSmallest(root, 1)