class Solution(object):
#     base 10  -> base 26, note no 0 here
    def convertToTitle2(self, num):
        """
        :type n: int
        :rtype: str
        """
        # return "" if num == 0 else self.convertToTitle((num - 1) / 26) + chr((num - 1) % 26 + ord('A'))
        if num == 0:
            return ""
        else:
            f = self.convertToTitle((num - 1) / 26)
            l = chr((num - 1) % 26 + ord('A'))
            return f + l

# https://leetcode.com/problems/excel-sheet-column-title/discuss/51404/Python-solution-with-explanation
    def convertToTitle(self, num):
        capitals = [chr(x) for x in range(ord('A'), ord('Z') + 1)]
        result = []
        while num > 0:
            result.append(capitals[(num - 1) % 26])
            num = (num - 1) // 26
        result.reverse()
        return ''.join(result)

    # def convertToTitle(self, num):
    #     capitals = [chr(x) for x in range(ord('A'), ord('Z') + 1)]
    #     capitals.insert(0, capitals.pop())
    #     result = []
    #     while num > 0:
    #         result.append(capitals[num % 26])
    #         num = num // 26
    #     result.reverse()
    #     return ''.join(result)

s = Solution()
print(s.convertToTitle(1))
print(s.convertToTitle(26))
print(s.convertToTitle(27))
print(s.convertToTitle(28))