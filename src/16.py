class Solution(object):
    def threeSumClosest1(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        # if not target or len(target) < 3:
        #     return
        n = len(nums)
        res = nums[0] + nums[1] + nums[2]
        #         brutal force
        for i in range(n - 2):
            for j in range(i + 1, n - 1):
                for k in range(j + 1, n):
                    curSum = nums[i] + nums[j] + nums[k]
                    if abs(curSum - target) < abs(res - target):
                        res = curSum
        return curSum


    def threeSumClosest2(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        nums = sorted(nums)

        def twoSum(ptr):
            i, j = 0, len(nums)-1
            if ptr == i:
                i += 1
            elif ptr == j:
                j -= 1
            goal = target - nums[ptr]

            res = nums[i] + nums[j]

            while i < j and i < len(nums) and j >= 0:
                if abs(nums[i] + nums[j] - goal) < abs(res - goal):
                    res = nums[i] + nums[j]

                if nums[i] + nums[j] < goal:
                    i += 1
                    if i == ptr:
                        i += 1
                elif nums[i] + nums[j] > goal:
                    j -= 1
                    if j == ptr:
                        j -= 1
                else:
                    return target

            return res + nums[ptr]

        ret = nums[0] + nums[1] + nums[2]
        for i in range(len(nums)):
            temp = twoSum(i)
            if temp == target:
                return temp
            if abs(temp - target) < abs(ret - target):
                ret = temp
        return ret


    def threeSumClosest(self, nums, target):
        list.sort(nums)
        res = nums[0] + nums[1] + nums[2]

        for i in range(len(nums) - 2):
            start = i + 1
            end = len(nums) - 1
            while start < end:
                sum = nums[i] + nums[start] + nums[end]
                if sum > target:
                    end -= 1
                elif sum < target:
                    start += 1
                else:
                    return target

                if abs(sum - target) < abs(res - target):
                    res = sum
        return res

s = Solution()
# s.threeSumClosest([-1, 2, 1, -4], 1)
s.threeSumClosest([0, 2, 1, -3], 1)