#Sol 1:
'''
def compareVersion(version1, version2):
	version1 = version1.split(sep = '.')
	version2 = version2.split(sep = '.')
	for i,v in enumerate(version1):
		if int(v) > int(version2[i]):#list out of range
			return 1
		elif int(v) < int(version2[i]):
			return -1
		elif len(version2) <= i:#corner situation
			return 1
		else:
			continue
	if len(version2) > len(version1):
		return -1
	else:
		return 0
compareVersion("1.0.1","1")
'''
#Sol 2:
def compareVersion(version1, version2):
	version1 = [int(v) for v in version1.split(sep = '.')]
	version2 = [int(v) for v in version2.split(sep = '.')]
	for i in range(max(len(version1),len(version2))):
		v1 = version1[i] if i < len(version1) else 0
		v2 = version2[i] if i < len(version2) else 0
		if v1 > v2:
			return 1
		elif v1 < v2:
			return -1
	return 0
		
