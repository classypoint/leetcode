class Solution:
    def isHappy(self, n):
        """
        :type n: int
        :rtype: bool
        """
        def getDigits(num):
            digits = []
            while num:
                digits.append(num%10)
                num = num // 10
            return digits
        squares = []
        while n not in squares:
            squares.append(n)
            dgts = getDigits(n)
            square = 0
            for dgt in dgts:
                square += dgt*dgt
            if square == 1:
                return True
            n = square
        return False
s = Solution()
s.isHappy(19)