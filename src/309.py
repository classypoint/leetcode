class Solution(object):
    def maxProfit(self, prices):
        """
        :type prices: List[int]
        :rtype: int
        """
        # s0[i] - maximal profits if reset on day i
#         s1: buy, s2:sell
#         s0[0] = 0, s1[0]=-prices[0], s2[0] do not exist(cannot sell on first day)
#         s0[i] = max(s0[i-1], s2[i-1]); s1[i] = max(s1[i-1], s0[i-1]-prices[i]); s2[i]=s1[i]+prices[i]
        if not prices:
            return 0
        s0_prev = 0
        s1_prev = -prices[0]
        for i in range(1, len(prices)):
            # s0 = max(s0_prev, s2_prev)
            s0 = max(s0_prev, s2_prev) if i > 1 else s0_prev
            s1 = max(s1_prev, s0_prev-prices[i])
            s2 = s1_prev+prices[i]
            s0_prev = s0
            s1_prev = s1
            s2_prev = s2
        return max(s0, s2)