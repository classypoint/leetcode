# Definition for a binary tree node.
import collections


class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def zigzagLevelOrder(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        res = []
        if not root:
            return res

        res = self.bfs(root)
        i = 1
        while i < len(res):
            res[i] = res[i][::-1]
            i += 2
        return res

    def bfs(self, root):
        results = []

        queue = collections.deque()
        queue.append(root)

        while queue:
            size = len(queue)
            level_values = []
            for i in range(size):
                node = queue.popleft()
                level_values.append(node.val)
                if node.left:
                    queue.append(node.left)
                if node.right:
                    queue.append(node.right)

            results.append(level_values[:])

        return results

s = Solution()
t = TreeNode(3)
t.left = TreeNode(9)
t.right = TreeNode(2)
# t.right.left = TreeNode(7)
# t.right.right = TreeNode(5)
t.left.left = TreeNode(1)
t.left.right = TreeNode(4)
s.bfs(t)
s.zigzagLevelOrder(t)