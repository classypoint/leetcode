class Solution(object):
    def sortColors(self, nums):
        """
        :type nums: List[int]
        :rtype: None Do not return anything, modify nums in-place instead.
        """
        # actually we only need zero_count, one_count
        r, g, b = 0, 0, 0
        for num in nums:
            if num == 0: r += 1
            elif num == 1: g += 1
            else: b += 1

        for i in range(r):
            nums[i] = 0
        for i in range(r, r + g):
            nums[i] = 1
        for i in range(r + g, len(nums)):
            nums[i] = 2

    def sortColors2(self, nums):
        # one pass, insertion sort
        i = 0
        while i < len(nums):
            j = i
            while j > 0 and nums[j] < nums[j - 1]:
                nums[j], nums[j - 1] = nums[j - 1], nums[j]
                j -= 1
            i += 1

    def sortColors3(self, nums):
        #all before Zero pointer should be 0; all after Two pointer should be 2
        zeroPos = 0
        twoPos = len(nums) - 1
        i = 0
        while i <= twoPos:
            if nums[i] == 0:
                temp = nums[zeroPos]
                nums[zeroPos] = 0
                nums[i] = temp
                zeroPos += 1
            elif nums[i] == 2:
                temp = nums[twoPos]
                nums[twoPos] = 2
                nums[i] = temp
                twoPos -= 1
                i -= 1
            i += 1

    def sortColors4(self, nums):
        zeroEnd, oneEnd, twoEnd = -1, -1, -1
        for num in nums:
            # if num == 0:
            #     twoEnd += 1
            #     nums[twoEnd] = 2
            #     oneEnd += 1
            #     nums[oneEnd] = 1
            #     zeroEnd += 1
            #     nums[zeroEnd] = 0
            # elif num == 1:
            #     twoEnd += 1
            #     nums[twoEnd] = 2
            #     oneEnd += 1
            #     nums[oneEnd] = 1
            # else:
            #     twoEnd += 1
            #     nums[twoEnd] = 2
            twoEnd += 1
            nums[twoEnd] = 2
            if num == 0:
                oneEnd += 1
                nums[oneEnd] = 1
                zeroEnd += 1
                nums[zeroEnd] = 0
            elif num == 1:
                oneEnd += 1
                nums[oneEnd] = 1


nums = [0,1,0,2,1,2,2,2]
Solution().sortColors4(nums)
print(nums)