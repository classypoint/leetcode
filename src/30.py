import collections

class Solution(object):
    def findSubstring(self, s, words):
        """
        :type s: str
        :type words: List[str]
        :rtype: List[int]
        """
        if words == []:
            return []
        counts = dict(collections.Counter(words))
        # res = set([])
        res = []
        n, num, word_len = len(s), len(words), len(words[0])
        for i in range(n - num * word_len + 1):
            seen = dict()
            j = 0
            while j < num:
                word = s[i + j * word_len:i + (j + 1) * word_len]
                if word in counts:
                    seen[word] = seen.get(word, 0) + 1#if word not in seen, return 0
                    if seen[word] > counts[word]:
                        break
                else:
                    break
                j += 1
            if j == num:
                # res.add(i)
                res.append(i)

        # return list(res)
        return res


    def findSubstring2(self, s, words):
        """
        :type s: str
        :type words: List[str]
        :rtype: List[int]
        """
        if not s or words == []:
            return []
        lenstr = len(s)
        lenword = len(words[0])
        lensubstr = len(words) * lenword
        # times = {}
        # for word in words:
        #     if word in times:
        #         times[word] += 1
        #     else:
        #         times[word] = 1
        times = dict(collections.Counter(words))
        ans = []
        for i in range(min(lenword, lenstr - lensubstr + 1)):
            self.findAnswer(i, lenstr, lenword, lensubstr, s, times, ans)
        return ans

    def findAnswer(self, strstart, lenstr, lenword, lensubstr, s, times, ans):
        wordstart = strstart
        curr = {}
        while strstart + lensubstr <= lenstr:
            word = s[wordstart:wordstart + lenword]
            wordstart += lenword
            if word not in times:
                strstart = wordstart
                curr.clear()
            else:
                # if word in curr:
                #     curr[word] += 1
                # else:
                #     curr[word] = 1
                curr[word] = curr.get(word, 0) + 1
                while curr[word] > times[word]:
                    curr[s[strstart:strstart + lenword]] -= 1
                    strstart += lenword
                if wordstart - strstart == lensubstr:
                    ans.append(strstart)

s = Solution()
s.findSubstring2("barfoothefoobarman", ["foo","bar"])
s.findSubstring2("barfoobarthe", ["foo","bar"])