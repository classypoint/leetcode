class ListNode:
	def __init__(self, x):
		self.val = x
		self.next = None

def listToNum(l):
	i, num = 0, 0
	ptr1, ptr2 = l, l
	while ptr:
		i += 1
		ptr = ptr.next
	for k in range(i):
		num += ptr.val*(10**(n-i-1))
	return num

def numToList(num):
	last,  num = num %10, num // 10
	list = ListNode(last)
	while(num):
		last, num = num % 10, num // 10
		nlist = ListNode(last)
		nlist.next = list
		list = nlist
	return nlist

def addTwoNumbers(l1, l2):
	num1 = listToNum(l1)
	num2 = listToNum(l2)
	sum = num1 + num2
	return numToList(sum)