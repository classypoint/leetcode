# https://leetcode.com/problems/longest-valid-parentheses/
# https://leetcode.com/problems/longest-valid-parentheses/discuss/14126/My-O(n)-solution-using-a-stack
class Solution(object):
    def longestValidParentheses(self, s):
        """
        :type s: str
        :rtype: int
        """
        #         stack contains (0, i), 0 represents '(' and 1 for ')'
        # IMPROVE: stack only contains index, use s[index] to obtain '(' or ')'
        stack = []

        for i, char in enumerate(s):
            if char == '(':
                stack.append((0, i))
            elif not stack or stack[-1][0] == 1:
                stack.append((1, i))
            else:
                #    match a closed pair
                stack.pop()

        #      now stack remains all unvalid parentheses position
        if not stack:   return len(s)
        #     obtain max interval
        # or start = -1, end = len(s) so intval always equals pos - start - 1
        start = 0
        end = len(s) - 1
        maxLen = 0
        for tp, pos in stack:
            curLen = pos - start
            maxLen = curLen if curLen > maxLen else maxLen
            start = pos + 1
        if end - stack[-1][1] + 1 > maxLen:
            maxLen = end - stack[-1][1]

        return maxLen

    def longestValidParentheses2(self, s):
        """
        :type s: str
        :rtype: int
        """
        if not s or len(s) < 2:
            return 0
        # stack stores indexes of all invalid bracket
        stack = []
        for i, c in enumerate(s):
            if c == '(':
                stack.append(i)
            elif len(stack) > 0 and s[stack[-1]] == '(':
                stack.pop()
            else:
                stack.append(i)
        # all brackets all valid
        if len(stack) == 0:
            return len(s)
        # find maximal index interval(i.e., valid brackets len)
        # maxLen = 0
        # for i in range(1, len(stack)):
        #     maxLen = max(maxLen, stack[i] - stack[i - 1] - 1)
        # maxLen = max(maxLen, len(s) - stack[-1] - 1)
        # maxLen = max(maxLen, stack[0])
        # return maxLen
        # alternatively
        rear = len(s)
        longest = 0
        while stack:
            head = stack.pop()
            longest = max(longest, rear - head - 1)
            rear = head
        longest = max(longest, head)
        return longest

s = Solution()
s.longestValidParentheses2("(()")