class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

#This is incredibly slow
def oddEvenList(head):
    sentinel = ListNode(63)
    sentinel.next = head
    #at least 3 nodes
    if (not sentinel.next) or (not sentinel.next.next) or (not sentinel.next.next.next):
        return sentinel.next
    ptr = sentinel.next
    nex = ptr.next
    prev = sentinel
    cur = ptr
    inc = 2
    q = ptr
    #note the order, when q is null, we won't go to q.next
    while (q and q.next):
        #find next odd position
        for i in range(inc):
            cur = cur.next
            prev = prev.next
        q = cur.next#q null:done; q.next is null, we are also done:
        #reconnect
        ptr.next = cur
        cur.next = nex
        prev.next = q
        #relabel
        prev = ptr
        ptr = cur
        nex = ptr.next
        inc += 1
    return sentinel.next
        
L = ListNode(5)
for i in range(1, 5):
    head = ListNode(5-i)
    head.next = L
    L = head
H = oddEvenList(L)