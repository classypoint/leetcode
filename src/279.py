class Solution:
    def numSquares(self, n):
        """
        :type n: int
        :rtype: int
        """
#         BFS
        if n <= 2:
            return n
        lst = []
        i = 1
        while i*i <= n:
            lst.append(i*i)
            i += 1
        toCheck = {n}
        depth = 0
        while(toCheck):
            depth += 1
            fringe = []
            for x in toCheck:
                for y in lst:
                    if x == y:
                        return depth
                    if x < y:
                        break
                    fringe.append(x-y)
            toCheck = fringe
        return depth
s = Solution()
s.numSquares(12)