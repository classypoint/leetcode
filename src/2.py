# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        dummy = ListNode(-1)
        ptr = dummy
        c = 0

        while l1 and l2:
            val = (l1.val + l2.val) % 10
            ptr.next = ListNode(val + c)
            c = (l1.val + l2.val) // 10
            ptr = ptr.next
            l1 = l1.next
            l2 = l2.next

        while l1:
            val = (l1.val + c) % 10
            c = (l1.val + c) // 10
            ptr.next = ListNode(val)
            ptr = ptr.next
            l1 = l1.next

        while l2:
            val = (l2.val + c) % 10
            c = (l2.val + c) // 10
            ptr.next = ListNode(val)
            ptr = ptr.next
            l2 = l2.next

        if c != 0:
            ptr.next = ListNode(c)

        return dummy.next

s = Solution()
l1 = ListNode(9)
l1.next = ListNode(9)
l1.next = ListNode(9)
l2 = ListNode(9)
s.addTwoNumbers(l1, l2)