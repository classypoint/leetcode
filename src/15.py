class Solution:
    # https: // www.geeksforgeeks.org / find - a - triplet - that - sum - to - a - given - value /
    # ===========(1) wrong============
    # def threeSum(self, nums):
    #     """
    #     :type nums: List[int]
    #     :rtype: List[List[int]]
    #     """
    #     res = []
    #     nums = sorted(nums)
    #     self.dfs(nums, 0, 0, [], res)
    #     return res
    # def dfs(self, nums, depth, val, path, res):
    #     if val < 0:
    #         return
    #         # break
    #     if depth == 3:#last round depth
    #         if val == 0 and path not in res:
    #         # if val == 0:
    #         #     if not res or path != res[-1]:
    #             res.append(path)
    #         return
    #     for i in range(len(nums)):
    #         if val - nums[i] < 0:
    #             break#abandon trees
    #         self.dfs(nums[i+1:], depth+1, val-nums[i], path+[nums[i]], res)

    # ==============(2) not unique, did not abandon trees=====
    # def threeSum(self, nums):
    #     """
    #     :type nums: List[int]
    #     :rtype: List[List[int]]
    #     """
    #     self.nums = nums
    #     self.res = []
    #     if len(nums) < 3:
    #         return self.res
    # -> precisely start from i
    #     for i in range(len(nums) - 2):
    #         self.dfs(i, 0, 0, [])
    #     return self.res
    #
    # def dfs(self, start, depth, val, path):
    #     # if pos >= len(self.nums):
    #     #     return
    #     if depth == 3:
    #         if (val == 0):
    #             self.res.append(path)
    #         return
    # -> break: WRONG! OUT OF LOOP
    #     for i in range(start + 1, len(self.nums)):
    #         print(i)
    #         self.dfs(i, depth + 1, val - self.nums[start], path + [self.nums[start]])

    #     =====(3) TIME LIMIT=================
    # this is actually o(n^3), would rather do a triple for-loop for simplicity
    #     def threeSum(self, nums):
    #         """
    #         :type nums: List[int]
    #         :rtype: List[List[int]]
    #         """
    #         self.nums = sorted(nums)
    #         self.res = []
    #         if len(nums) < 3:
    #             return self.res
    #    ->abadon     # for i in range(len(nums) - 2):
    #                 #     self.dfs(i, [])
    #         # start from i or after i
    #         self.dfs(0, [])
    #         return self.res
    #
    #     def dfs(self, start, path):
    #         if len(path) == 3:
    #             if sum(path) == 0 and path not in self.res:
    #                 self.res.append(path)
    #             return
    #         if start >= len(self.nums):
    #             return
    #         # for i in range(start + 1, len(self.nums)):
    #         for i in range(start, len(self.nums)):
    #             if sum(path)+self.nums[start] > 0:
    #                 break
    #             self.dfs(i + 1, path + [self.nums[i]])

    # def threeSum(self, nums):
    #     """
    #     :type nums: List[int]
    #     :rtype: List[List[int]]
    #     """
    #     if len(nums) < 3:
    #         return []
    #     nums.sort()
    #     res = set()
    #     for i, v in enumerate(nums[:-2]):
    #         if i >= 1 and v == nums[i - 1]:
    #             continue
    #         d = {}
    #         for x in nums[i + 1:]:
    #             if x not in d:
    #                 d[-v - x] = 1
    #             else:
    #                 res.add((v, -v - x, x))
    #     return map(list, res)

    def threeSum(self, nums):
        res = []
        nums = sorted(nums)

        for i in range(len(nums)-2):
            if nums[i] > 0: return res
            if i > 0 and nums[i] == nums[i - 1]: continue

            l = i + 1
            r = len(nums) - 1
            while l < r:
                curSum = nums[i] + nums[l] + nums[r]
                if curSum > 0:
                    r -= 1
                elif curSum < 0:
                    l += 1
                else:
                    res.append([nums[i], nums[l], nums[r]])
                    while nums[l+1] == nums[l]:
                        l += 1
                        if l >= len(nums) - 1: break
                        # if l >= len(nums) - 1: return res
                    while nums[r-1] == nums[r]:
                        r -= 1
                        if r <= i: break
                    l += 1
                    r -= 1
        return res


s = Solution()
# s.threeSum([2,0,-2,-5,-5,-3,2,-4])
# s.threeSum([0, 0, 0])
# s.threeSum([-1,0,1,2,-3,-1])
s.threeSum2([-2,0,1,1,2])
# s.threeSum([-4,-2,-2,-2,0,1,2,2,2,3,3,4,4,6,6])