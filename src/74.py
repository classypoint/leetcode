class Solution:
    # n * m matrix, conversion with array
    # matrix[x][y] = a[x*m + y], a[x] = matrix[x/m][x%y]
    # treat it a sorted list and binary search
    # o(log(mn))
    def searchMatrix3(self, matrix, target):
        n, m = len(matrix), len(matrix[0])
        l, r = 0, n * m - 1
        while l <= r:
            mid = l + ((r - l) >> 1)
            if matrix[mid // m][mid % m] < target:
                l = mid + 1
            elif matrix[mid // m][mid % m] > target:
                r = mid -1
            else:
                return True

        return False

    def searchMatrix(self, matrix, target):
        # why faster than above?
        n, m = len(matrix), len(matrix[0])
        l, r = 0, n * m - 1
        while l != r:
            mid = (l + r) >> 1
            if matrix[mid // m][mid % m] < target:
                l = mid + 1
            else:
                r = mid
        return matrix[r // m][r % m] == target

    #     start from left-bottom, O(n*m)
    def searchMatrix2(self, matrix, target):
        """
        :type matrix: List[List[int]]
        :type target: int
        :rtype: bool
        """
        if not matrix:
            return False

        row, col = len(matrix), len(matrix[0])
        i, j = row - 1, 0

        while i >= 0 and j < col:
            if matrix[i][j] < target:
                j += 1
            elif matrix[i][j] > target:
                i -= 1
            else:
                return True

        return False


s = Solution()
# s.searchMatrix([[1,3,5,7],[10,11,16,20],[23,30,34,50]], 3)
# s.searchMatrix([[-10,-8,-6,-4,-3],[0,2,3,4,5],[8,9,10,10,12]], 0)
s.searchMatrix([[0, 1, 2],[3, 4, 5]], 2)
s.searchMatrix3([[0, 1, 2],[3, 4, 5]], 2)