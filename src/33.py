# 81

# https://leetcode.wang/leetCode-33-Search-in-Rotated-Sorted-Array.html
# first find rotating point
def search(nums, target):
    """
    :type nums: List[int]
    :type target: int
    :rtype: int
    """
    n = len(nums)
    lo, hi = 0, n - 1
    # find the index of the smallest value using binary search
    # loop will terminate since mid < hi, and lo or hi will shrink by at least 1
    # Proof by contradiction that mid < hi: if mid==hi, then lo==hi and loop would have been terminated
    while lo < hi:#note the diff l12, l20
        mid = (lo + hi) // 2
        if nums[mid] > nums[hi]: lo = mid + 1
        else: hi = mid
    # lo == hi is the index of the smallest value and also the place of rotation
    rot = lo
    lo, hi = 0, n - 1
    # The usual binary search and accounting for rotation
    while lo <= hi:
        mid = (lo + hi) // 2
        realmid = (mid + rot) % n
        if nums[realmid] == target: return realmid
        if nums[realmid] < target: lo = mid + 1
        else: hi = mid - 1
    return -1


def search2(nums, target):
    start, end = 0, len(nums) - 1
    while start < end:
        mid = (start + end) // 2
        if nums[mid] > nums[end]:
            # e.g., 3, 4, 5, 6, 1, 2
            if target > nums[mid] or target <= nums[end]:
                start = mid + 1
            else:
                end = mid
        else:
            # 5, 6, 1, 2, 3, 4
            if target > nums[mid] and target <= nums[end]:
                start = mid + 1
            else:
                end = mid

    if start == end and target != nums[start]: return -1
    return start

# https://leetcode.com/problems/search-in-rotated-sorted-array/discuss/14435/Clever-idea-making-it-simple
def search3(nums, target):
    lo, hi = 0, len(nums) - 1
    while lo <= hi:
        mid = lo + (hi - lo) // 2
        # if nums[mid] and target are at the same side, just take the value
        if (nums[mid] < nums[0]) is (target < nums[0]):
            val = nums[mid]
        # padding left to -INF
        elif target < nums[0]:   val = -float('inf')
        # padding right to INF
        else:   val = float('inf')

        if val < target:
            lo = mid + 1
        elif val > target:
            hi = mid - 1
        else:
            return mid
    return -1

# similar to 3
def search4(nums, target):
    lo, hi = 0, len(nums)#be careful of hi here
    while lo < hi:#< not <=
        mid = lo + (hi - lo) // 2
        if (nums[mid] < nums[0]) is (target < nums[0]):
            val = nums[mid]
            if val < target:    lo = mid + 1
            elif val > target:  hi = mid # <-
            else:   return mid

        elif target < nums[0]: lo = mid + 1
        else:   hi = mid # <-
    return -1


# at least half of the arr is ordered
def search5(nums, target):
    start, end = 0, len(nums) - 1
    while start <= end:
        mid = (start + end) // 2
        if target == nums[mid]: return mid
        #left side is ordered
        if nums[start] <= nums[mid]:
            #target here
            if target >= nums[start] and target < nums[mid]:
                end = mid - 1
            else:
                start = mid + 1
        #right side is ordered
        else:
            if target > nums[mid] and target <= nums[end]:
                start = mid + 1
            else:
                end = mid - 1
    return -1


# search([4, 5, 6, 7, 0, 1, 2], 1)
print(search3([4, 5, 6, 7, 0, 1, 2], 9))