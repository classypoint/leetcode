def reachNumber(self, target):
	temp = abs(target)
	k = 0
	while (temp > 0):
		k += 1
		temp -= k
	if temp == 0:
		return k
	delta = -temp
	if delta%2 == 0:
		return k
	else:
		delta = delta + k + 1
		if delta %2 == 0:
			return k+1
		else:
			return k+2