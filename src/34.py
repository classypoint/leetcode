# https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/
class Solution(object):
    # method1: do two bs, one for first appearing pos, the other for last appearing pos
    # method 2
    def searchRange(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        def search(n):
            lo, hi = 0, len(nums)
            while lo < hi:
                mid = (lo + hi) / 2
                if nums[mid] >= n:
                    hi = mid
                else:
                    lo = mid + 1
            return lo

        lo = search(target)
        return [lo, search(target + 1) - 1] if target in nums[lo:lo + 1] else [-1, -1]

    def searchRange2(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        lo = self.firstGreaterEqual(nums, target)
        if lo >= len(nums) or nums[lo] != target:#handle empty set
        # if nums[lo] != target:
            return [-1, -1]
        else:
            return [lo, self.firstGreaterEqual(nums, target + 1) - 1]

    def firstGreaterEqual(self, nums, target):
        # end = len(nums) not len(nums)-1, very important here. As this will give u the right pos if target > nums[-1]
        start, end = 0, len(nums)
        while start < end:
            mid = start + ((end - start) >> 1)
            if nums[mid] < target:
                start = mid + 1
            else:
                end = mid
        return start

s = Solution()
# s.searchRange2([4, 4, 5, 6, 7, 7, 8, 8, 8], 9)
# s.searchRange2([1], 1)
s.firstGreaterEqual([1], 2)
s.firstGreaterEqual([1], 1)
# s.firstGreaterEqual([4, 4, 5, 6, 7, 7, 7], 8)
