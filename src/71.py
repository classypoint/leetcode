class Solution(object):
    def simplifyPath(self, path):
        """
        :type path: str
        :rtype: str
        """
        res = []
        arr = path.split('/')
        for i in range(len(arr)):
            if not arr[i] or arr[i] == '.' or arr[i] == '/':
                continue
            if arr[i] == '..':
                if res:
                    # if curretnly higher than /, go up by deleting present last path
                    res.pop()
            else:
                res.append(arr[i])

        return '/' + '/'.join(res)

p1 = "/home//foo/"
p2 = "/a/./b/../../c/"
p3 = "/a/../../b/../c//.//"
p4 = "/a//b////c/d//././/.."
print(Solution().simplifyPath(p1))