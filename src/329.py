class Solution(object):

    def longestIncreasingPath1(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: int
        """
        self.maxLen = 0

        if not matrix:
            return 0

        self.row = len(matrix)
        self.col = len(matrix[0])

        for i in range(self.row):
            for j in range(self.col):
                self.dfs(matrix, i, j, 0)
        return self.maxLen


    def dfs1(self, matrix, i, j, pathLen):
        # void dfs
        self.maxLen = pathLen + 1 if pathLen + 1 > self.maxLen else self.maxLen

        if j < self.col - 1 and matrix[i][j] < matrix[i][j + 1]:
            self.dfs1(matrix, i, j + 1, pathLen + 1)
        if j > 0 and matrix[i][j] < matrix[i][j - 1]:
            self.dfs1(matrix, i, j - 1, pathLen + 1)
        if i > 0 and matrix[i][j] < matrix[i - 1][j]:
            self.dfs1(matrix, i - 1, j, pathLen + 1)
        if i < self.row - 1 and matrix[i][j] < matrix[i + 1][j]:
            self.dfs1(matrix, i + 1, j, pathLen + 1)


    def longestIncreasingPath(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: int
        """
        maxLen = 1

        if not matrix:
            return 0
        row = len(matrix)
        col = len(matrix[0])
        cache = [[0 for i in range(col)] for j in range(row)]
        for i in range(row):
            for j in range(col):
                maxLen = max(maxLen, self.dfs(matrix, i, j, cache))
        return maxLen


    def dfs(self, matrix, i, j, cache):
        # memorized dfs
        if cache[i][j] > 0:
            return cache[i][j]

        row = len(matrix)
        col = len(matrix[0])

        maxLen = 1
        # we donot need to worry about revisiting since if we move from a -> b, then
        # a < b, so so won't do b -> a
        if j < col - 1 and matrix[i][j] < matrix[i][j + 1]:
            maxLen = max(maxLen, self.dfs(matrix, i, j + 1, cache)+1)
        if j > 0 and matrix[i][j] < matrix[i][j - 1]:
            maxLen = max(maxLen, self.dfs(matrix, i, j - 1, cache)+1)
        if i > 0 and matrix[i][j] < matrix[i - 1][j]:
            maxLen = max(maxLen, self.dfs(matrix, i - 1, j, cache)+1)
        if i < row - 1 and matrix[i][j] < matrix[i + 1][j]:
            maxLen = max(maxLen, self.dfs(matrix, i + 1, j, cache)+1)

        cache[i][j] = maxLen

        return maxLen

s = Solution()
s.longestIncreasingPath([
  [9,9,4],
  [6,6,8],
  [2,1,1]
] )
s.longestIncreasingPath([
  [3,4,5],
  [3,2,6],
  [2,2,1]
] )
