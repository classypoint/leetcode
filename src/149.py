# Definition for a point.
class Point:
    def __init__(self, a=0, b=0):
        self.x = a
        self.y = b
    def __eq__(self, other):
        return self.x == other.x and self.y == other.y
    def __hash__(self):
        return hash(self.x, self,y)
# calculate the slope and intercep between any 2 pairs, the (a,b) that occur the most is the line we want
# y = ax + b
from collections import defaultdict
class Line:
    # handle vertical and horizontal line; no duplicate points
    def getParams(self, p1, p2):
        if p1.x == p2.x:
            return(None, p1.x)
        if p1.y == p2.y:
            # return(0, None)
            return(p2.y, None)
        a = (p2.y - p1.y)/(p2.x - p1.x)
        b = p1.y - p1.x*a
        return (a, b)
    def isOnline(self, p, params):
        if params[0] != None and params[1] != None:
            if p.x*params[0] + params[1] == p.y:
                return True
        #vertical line
        elif params[0] == None:
            if p.x == params[1]:
                return True
        # horizontal
        else:
            if p.y == params[0]:
                return True
        return False
class Solution:
    def maxPoints(self, points):
        """
        :type points: List[Point]
        :rtype: int
        """
        # filter duplicate nodes
        if not points:
            return 0
        if len(points) == 1:
            return 1
        if len(points) == 2:
            return 2
        # points = list(set(points))
        unipoints, duppoints = [], []
        for point in points:
            if point in unipoints:
                duppoints.append(point)
            else:
                unipoints.append(point)
        points = unipoints
        # k: (a, b) for line, v:frequency
        d = defaultdict(int)
        line = Line()
        for i in range(len(points)):
            for j in range(i + 1, len(points)):
                k = line.getParams(points[i], points[j])
                d[k] += 1
        v=list(d.values())#frequency
        k=list(d.keys())#a, b
        p = max(v)
        lp = k[v.index(p)]
        # this is pair num p= C(n, 2)
        c = int((1+(1+8*p)**0.5)/2)
        for pt in duppoints:
            if line.isOnline(pt, lp):
                c += 1
        return c
obj = Solution()
# obj.maxPoints([[0,0],[1,1],[0,0]])
obj.maxPoints([Point(0,0), Point(1,1), Point(0,0)])