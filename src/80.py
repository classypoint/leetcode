# 26
class Solution(object):
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) < 2: return len(nums)

        slow, fast = 0, 1
        maxDup = 2
        # how many times fast element occur
        dupCount = 1
        while fast < len(nums):
            if nums[fast] != nums[slow]:
                slow += 1
                nums[slow] = nums[fast]
                dupCount = 1
            elif dupCount < maxDup:
                slow += 1
                nums[slow] = nums[fast]
                dupCount += 1
            fast += 1
        return slow + 1

    def removeDuplicates2(self, nums):
        i = 0
        for n in nums:
            if i < 2 or n > nums[i - 2]:
                nums[i] = n
                i += 1
        return i

nums = [0,0,1,1,1,1,2,3,3]
print(Solution().removeDuplicates2(nums))
print(nums)