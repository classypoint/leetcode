class Solution(object):
    # wildcard matching
    # ? can match any single char,  * can match 0, 1 or multiple chars

    def isMatch(self, str, pattern):
        """
        :type str: str
        :type pattern: str
        :rtype: bool
        """
        #         s is pointer to str, p is pointer to pattern
        #     Goal, s, p both to the end
        # match is current matched position in s, starIdx is pos of * in p
        s, p, matchedS, starInP = 0, 0, 0, -1

        #         iterate over whole string
        while s < len(str):
            # if current char matches
            if p < len(pattern) and (str[s] == pattern[p] or pattern[p] == '?'):
                s += 1
                p += 1
            # if meets * and it matches ' ', recordes position of *
            # in startIdx, record current position in string and j++
            elif p < len(pattern) and pattern[p] == '*':
                starInP = p
                matchedS = s
                p += 1
            # cur char does not match and no '*', backtracing
            # here we use '*' to match one character
            elif starInP != -1:
                p = starInP + 1
                matchedS += 1#all before match in s in matched (exclusive)
                s = matchedS
            # cur char does not match and no recorded '*', return False
            else:
                return False

        #         deal with extra '*' at the end of s
        while (p < len(pattern) and pattern[p] == '*'):
            p += 1

        return p == len(pattern)


    # O(m*n)
    def isMatch2(self, str, pattern):
        s, p = len(str), len(pattern)
        dp = [[False for _ in range(p + 1)] for _ in range(s + 1)]

        # initialize first row and first col
        dp[0][0] = True
        for j in range(1, p + 1):
            # if dp[0][j - 1] == True and pattern[j - 1] == '*':  dp[0][j] = True
            if pattern[0][j - 1] == '*': dp[0][j] = True
            else: break
        # for i in range(1, s + 1): dp[i][0] == False

        for i in range(1, s + 1):
            for j in range(1, p + 1):
                # if dp[i - 1][j - 1] == True and (str[i - 1] == pattern[j - 1] or pattern[j - 1] == '?'):
                #     dp[i][j] = True
                # elif pattern[j - 1] == '*' and (dp[i - 1][j] == True or dp[i][j - 1] == True):
                #     dp[i][j] = True

                # >>>>>better
                if str[i-1] == pattern[j-1] or pattern[j-1] == '?':
                    dp[i][j] = dp[i-1][j-1]
                elif pattern[j-1] == '*':
                    dp[i][j] = dp[i-1][j] or dp[i][j-1]
                # else:
                #     dp[i][j] = False

        return dp[s][p]

s = Solution()
res = s.isMatch2("abcdc", "a*c")
print(res)