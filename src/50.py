def myPow2(x, n):
    """
    :type x: float
    :type n: int
    :rtype: float
    """
    if n < 0:
        x = 1 / x
        n = -n
    ans = 1
    while n:
        if n & 1:
            ans *= x
        x *= x
        n = n // 2
    return ans

def myPow(x, n):
    if not n: return 1
    if n < 0: return 1 / myPow(x, -n)
    if n % 2: return x * myPow(x, n - 1)
    return myPow(x * x, n // 2)

print(myPow(2, 10))