# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
class Solution:
    def flatten(self, root):
        """
        :type root: TreeNode
        :rtype: void Do not return anything, modify root in-place instead.
        """
        # dfs traversal
        nodes = []
        self.dfs(root, nodes)
        return nodes
    def dfs(self, root, nodes):
        if not root:
            return
        self.dfs(root.left, nodes)
        self.dfs(root.right, nodes)
        nodes.append(root.val)
s = Solution()
l = TreeNode(1)
l.left = TreeNode(2)
l.right = TreeNode(5)
l.left.left = TreeNode(3)
l.left.right = TreeNode(4)
l.right.right = TreeNode(6)
s.flatten(l)