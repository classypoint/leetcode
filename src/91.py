#62, 70, 509
class Solution(object):
    def numDecodings(self, s):
        """
        :type s: str
        :rtype: int
        """
        dp = [0] * (len(s) + 1)
        dp[-1] = 1
        if s[-1] != '0':
            dp[len(s) - 1] = 1
        j = len(s) - 2
        while j >= 0:
            if s[j] == '0':
                j -= 1
                continue
            if s[j] == '1' or s[j + 1] <= '6':
                dp[j] = dp[j + 1] + dp[j + 2]
            else:
                dp[j] = dp[j + 1]
            j -= 1
        return dp[0]

print(Solution().numDecodings('27'))