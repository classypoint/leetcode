# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    # WRONG
    def isValidBST(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        if not root:
            return True
        # we not only need root.val > root.left.val bue need the root.val > node.val
        # for any node in left subtree
        if root.left and ((root.val <= root.left.val) or not self.isValidBST(root.left)):
            return False
        if root.right and ((root.val >= root.right.val) or not self.isValidBST(root.right)):
            return False
        return True

    def isValidBST2(self, root):
        def getMax(node):
            max = node.val
            while node.right:
                max = node.right.val
                node = node.right
            return max

        def getMin(node):
            min = node.val
            while node.left:
                min = node.left.val
                node = node.left
            return min

        if not root:
        # if not root or (not root.left and not root.right):
        # thus we do not need the final return True
            return True

        if root.left:
            if self.isValidBST2(root.left):
                maxVal = getMax(root.left)
                if root.val <= maxVal:
                    return False
            else:
                return False

        if root.right:
            if self.isValidBST2(root.right):
                minVal = getMin(root.right)
                if root.val >= minVal:
                    return False
            else:
                return False

        return True

    # inorder traversal
    def isValidBST3(self, root):
        if not root:
            return True
        stack = []
        pre = None
        while root or stack:
            while root:
                stack.append(root)
                root = root.left
            root = stack.pop()#popped root should be increasing
            # use pre to store last root
            if pre and root.val <= pre.val:
                return False
            pre = root

            root = root.right
        return True

    # range preorder traversal
    # left child: (left boundary of father, father)
    # right child: (father, right boundary of father)
    def isValidBST4(self, root):
        return self.helper(root, None, None)

    def helper(self, node, minVal, maxVal):
        if not node: return True

        if minVal is not None and node.val <= minVal:
            return False
        if maxVal is not None and node.val >= maxVal:
        # if maxVal and node.val >= maxVal:
        # BE CAREFUL,  maxVal == 0 is False
            return False

        return self.helper(node.left, minVal, node.val) and \
               self.helper(node.right, node.val, maxVal)

    def isValidBST5(self, root):
        return True

t = TreeNode(0)
t.right = TreeNode(-1)
print(Solution().isValidBST4(t))
