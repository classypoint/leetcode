class Solution:
    def generateParenthesis(self, n):
        """
        :type n: int
        :rtype: List[str]
        """
        res = []
        eval = {"(": 1, ")": -1}
        self.dfs(n, 0, 0, "", res, eval)
        return res

    def dfs(self, n, sum, depth, path, res, eval):
        # if sum < 0:
        #     return
        # # this makes sure that we won't reach depth == 7
        # if depth == 6:
        #     if sum == 0:
        #         res.append(path)
        #     return
        if sum < 0 or depth > 2 * n:
            return
        if sum == 0 and depth == 2 * n:
            res.append(path)
            return
        for i in ["(", ")"]:
            # sum += eval[i]
            # depth += 1
            # path += i
            # self.dfs(n, sum, depth, path, res, eval)
            self.dfs(n, sum + eval[i], depth + 1, path + i, res, eval)


    # METHOD 2
    def generateParenthesis2(self, n):
        """
        :type n: int
        :rtype: List[str]
        """
        res = []
        self.dfs3(n, 0, 0, '', res)
        return res

    # l: num of left brackets, r:num of right brackets
    def dfs2(self, n, l, r, path, res):
        # if l < r or l + r > 2 * n:
        #     return
        if l < r or l > n or r > n:
            return
        if l == n and r == n:
            res.append(path)
            return
        self.dfs2(n, l + 1, r, path + '(', res)
        self.dfs2(n, l, r + 1, path + ')', res)

    def dfs3(self, n, l, r, path, res):
        if l == n and r == n:
            res.append(path)
            return
        if l < n:
            self.dfs3(n, l + 1, r, path + '(', res)
        if l > r:
            self.dfs3(n, l, r + 1, path + ')', res)

s = Solution()
s.generateParenthesis2(3)