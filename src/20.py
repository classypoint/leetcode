class Solution(object):
    def isValid(self, s):
        """
        :type s: str
        :rtype: bool
        """
        left = {'(':0, '[':1, '{':2}
        right = {')':0, ']':1, '}':2}
        stack = []
        for char in s:
            if char in left:
                stack.append(char)
            else:
                if not stack or left[stack.pop()] != right[char]:
                    return False
        # return True
        return not stack


    def isValid2(self, s):
        stack = []
        for c in s:
            if c == '(':    stack.append(')')
            elif c == '[':  stack.append(']')
            elif c == '{':  stack.append('}')
            elif not stack or stack.pop() != c:
                return False

        return not stack


    def isValid3(self, s):
        stack = []
        dict = {"]":"[", "}":"{", ")":"("}
        for char in s:
            if char in dict.values():
                stack.append(char)
            elif char in dict.keys():
                if stack == [] or dict[char] != stack.pop():
                    return False
            else:
                return False
        return stack == []

s = Solution()
s.isValid('()')
s.isValid(']')
s.isValid('{')