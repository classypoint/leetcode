import heapq

# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

    def __lt__(self, other):
        return self.val < other.val


class minHeap(object):#Node object
    def __init__(self):
        self.arr = []
        self.size = 0

    def add(self, node):
        self.arr.append(node)
        self.size += 1
        self.swim(self.size - 1)

    def pop(self):
        temp = self.arr[0]
        self.arr[0] = self.arr[-1]
        self.arr.pop()
        self.size -= 1
        self.sink(0)
        return temp

    # JAVA version
    # def add2(self, node):
    #     # self.arr.append(node) <- use index
    #     self.arr[self.size] = node
    #     self.size += 1
    #     self.swim(self.size - 1)
    #
    # def pop2(self):
    #     temp = self.arr[0]
    #     self.arr[0] = self.arr[-1]
    #     # self.arr.pop()       <- so no need to pop here when deleting
    #     self.size -= 1
    #     self.sink(0)
    #     return temp

    def swim(self, pos):
        if pos == 0: return
        p = self.parent(pos)#The comparison is not generic
        if self.arr[pos].val < self.arr[p].val:
            self.swap(pos, p)
            self.swim(p)

    def sink(self, pos):
        l = self.left(pos)
        r = self.right(pos)

        if l >= self.size:#no child
            return
        if r >= self.size:#just left child
            if self.arr[pos].val > self.arr[l].val:
                self.swap(pos, l)
        else:#both left and right child
            if self.arr[l].val <= self.arr[r].val:
                if self.arr[pos].val > self.arr[l].val:
                    self.swap(pos, l)
                    self.sink(l)
            else:
                if self.arr[pos].val > self.arr[r].val:
                    self.swap(pos, r)
                    self.sink(r)

    def left(self, idx):
        return 2 * idx + 1

    def right(self, idx):
        return 2 * idx + 2

    def parent(self, idx):
        return (idx - 1) // 2

    def swap(self, id1, id2):
        temp = self.arr[id1]
        self.arr[id1] = self.arr[id2]
        self.arr[id2] = temp


def testMinHeap():
    h = minHeap()
    h.add(ListNode(3))
    h.add(ListNode(1))
    h.pop()
    h.add(ListNode(2))
    h.add(ListNode(5))


class Solution(object):
    # method 1 self-built minheap
    # def mergeKLists(self, lists):
    #     """
    #     :type lists: List[ListNode]
    #     :rtype: ListNode
    #     """
    #     # if len(lists) < 2: return lists
    #     # if len(lists) == 0: return None
    #     if len(lists) == 1: return lists[0]
    #
    #     res = ListNode(0)
    #     ptr = res
    #     heap = minHeap()
    #     for list in lists:
    #         if list:
    #             heap.add(list)
    #
    #     while heap.size != 0:
    #         cur = heap.pop()
    #         ptr.next = ListNode(cur.val)
    #         ptr = ptr.next
    #         if cur.next:
    #             heap.add(cur.next)
    #
    #     return res.next
    # ========method 2 PQ, KlogN use tuple to cmp
    # def mergeKLists(self, lists):
    #     """
    #     :type lists: List[ListNode]
    #     :rtype: ListNode
    #     """
    #     heap = []
    #     ret = ListNode(-1)
    #     ptr = ret
    #
    #     for lst in lists:
    #         heapq.heappush(heap, (lst.val, lst))
    #
    #     while heap:
    #         cur = heapq.heappop(heap)[1]
    #         ptr.next = cur
    #         ptr = ptr.next
    #         if cur.next:
    #             heapq.heappush(heap, (cur.next.val, cur.next))
    #
    #     return ret.next
    # method 3, use __cmp__ as heap comparator
    # !!!note this will change input list!!!
    def mergeKLists(self, lists):
        """
        :type lists: List[ListNode]
        :rtype: ListNode
        """
        heap = []
        ret = ListNode(-1)
        ptr = ret

        for lst in lists:
            if lst:
                heapq.heappush(heap, lst)

        while heap:
            cur = heapq.heappop(heap)
            ptr.next = cur
            ptr = ptr.next
            if cur.next:
                heapq.heappush(heap, cur.next)

        return ret.next

s = Solution()
l1 = ListNode(1)
l1.next = ListNode(4)
l1.next.next = ListNode(5)
l2 = ListNode(1)
l2.next = ListNode(3)
l2.next.next = ListNode(4)
l3 = ListNode(2)
l3.next = ListNode(6)
res = s.mergeKLists([l1, l2, l3])
# res = s.mergeKLists([None, None])
# res = s.mergeKLists([None])
