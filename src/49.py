# https://leetcode.com/problems/group-anagrams/
import collections
class Solution(object):
#     union
    def groupAnagrams(self, strs):
        """
        :type strs: List[str]
        :rtype: List[List[str]]
        """
        ret = []
        for str in strs:
            isUnioned = False
            for group in ret:
                if self.isAnagrams(str, group[0]):
                    isUnioned = True
                    group.append(str)
                    break
            if not isUnioned:
                ret.append([str])

        return ret
        # parent = list(range(len(strs)))

# https://www.geeksforgeeks.org/check-whether-two-strings-are-anagram-of-each-other/
# (1) sort then cmp one by one
# (2) count num of each char: a.dict: use char as key, # of appearance as value; b.use ord(char) in
# dex in array
    def isAnagrams(self, str1, str2):
        if len(str1) != len(str2):  return False
        #
        # arr = [0 for i in range(len(str1))]
        # for i in range(len(str1)):
        #     # TODO: mapping to resolve conflict
        #     arr[str1[i]] += 1
        #     arr[str2[i]] -= 1
        #
        # for count in arr:
        #     if count != 0:
        #         return False
        #
        # return True

        # count_buffer = {}
        # for i in range(len(str1)):
        #     if str1[i] in count_buffer:
        #         count_buffer[str1[i]] += 1
        #     else:
        #         count_buffer[str1[i]] = 1
        #     if str2[i] in count_buffer:
        #         count_buffer[str2[i]] -= 1
        #     else:
        #         count_buffer[str2[i]] = -1

        count_buffer = collections.defaultdict(int)
        for i in range(len(str1)):
            count_buffer[str1[i]] += 1
            count_buffer[str2[i]] -= 1

        for char in count_buffer:
            if count_buffer[char] != 0:
                return False
        return True

    def groupAnagrams2(self, strs):
        map = dict()
        for str in strs:
            key = ''.join(sorted(str))
            # all anagrams are stored under this key in the map
            if key in map: map[key].append(str)
            else: map[key] = [str]

            # alternatively
            # key = tuple(sorted(str))
            # map[key] = map.get(key, []) + [str]

        return list(map.values())


s = Solution()
s.isAnagrams("abe", "eba")
print(s.groupAnagrams2(["eat", "tea", "tan", "ate", "nat", "bat"]))
