def rotate_new(matrix):
    """
    :type matrix: List[List[int]]
    :rtype: void Do not return anything, modify matrix in-place instead.
    """
    # equation orig (i, j) -> (j, n - i)

    # >>>>>method 1 NOT in-place
    # mTranspose = [[matrix[j][i] for j in range(len(matrix[0]))] for i in range(len(matrix))]
    # matrix = [mTranspose[i][::-1] for i in range(len(mTranspose))]

    n = len(matrix) - 1
    matrix = [[matrix[n - j][i] for j in range(n)] for i in range(n)]
    return matrix

# clockwise rotate
# first reverse up to down, then swap the symmetry (order can reverse)
# 1 2 3     7 8 9     7 4 1
# 4 5 6  => 4 5 6  => 8 5 2
# 7 8 9     1 2 3     9 6 3

#  * anticlockwise rotate
#  * first reverse left to right, then swap the symmetry
#  * 1 2 3     3 2 1     3 6 9
#  * 4 5 6  => 6 5 4  => 2 5 8
#  * 7 8 9     9 8 7     1 4 7
def rotate(matrix):
    matrix.reverse()
    # i, j = 0, len(matrix) - 1
    # while i < j:
    #     temp = matrix[i]
    #     matrix[i] = matrix[j]
    #     matrix[j] = temp
    #     i += 1
    #     j -= 1

    for i in range(len(matrix)):
        for j in range(i+1, len(matrix[0])):
            matrix[i][j], matrix[j][i] = matrix[j][i], matrix[i][j]


def rotate2(matrix):
    for i in range(len(matrix)):
        for j in range(i + 1, len(matrix[0])):
            matrix[i][j], matrix[j][i] = matrix[j][i], matrix[i][j]
    for l in matrix:
        l.reverse()

s = [[1,2,3],[4,5,6],[7,8,9]]
rotate2(s)
print(s)