#Sol 1: union cluster
'''
def findCircleNum(M):
    def unioncluster(p1,p2):
        if p1 != p2:
            p1, p2 = min(p1,p2), max(p1,p2)
            for frd in cluster[p2]:
                cluster[p1].append(frd)
                pos[frd] = p1
            cluster[p2] = []
    n = len(M)
    m = 0
    cluster = [[j] for j in range(n)]#merge cluster, cluster contains cluster element
    pos = [j for j in range(n)]#pos contains which cluster current person is in
    for i in range(n):
        for j in range(i+1, n):
            if M[i][j] == 1:
                unioncluster(pos[i],pos[j])#cluster[j]
                #pos[j] = i
    for temp in cluster:
        if len(temp) >= 1:
            m += 1
    return m
    '''
#Sol 1: dfs
def findCircleNum(A):
    N = len(A)
    seen = set()
    def dfs(node):#find every node connected to it using dfs
        for nei, adj in enumerate(A[node]):
            if adj and nei not in seen:#adj = 1 means connection exists
                seen.add(nei)
                dfs(nei)
    ans = 0
    for i in range(N):
        if i not in seen:
            dfs(i)
            ans += 1
    return ans

#findCircleNum([[1,0,0,0,0,0,0,0,0,1,0,0,0,0,0],[0,1,0,1,0,0,0,0,0,0,0,0,0,1,0],[0,0,1,0,0,0,0,0,0,0,0,0,0,0,0],[0,1,0,1,0,0,0,1,0,0,0,1,0,0,0],[0,0,0,0,1,0,0,0,0,0,0,0,1,0,0],[0,0,0,0,0,1,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,1,0,0,0,0,0,0,0,0],[0,0,0,1,0,0,0,1,1,0,0,0,0,0,0],[0,0,0,0,0,0,0,1,1,0,0,0,0,0,0],[1,0,0,0,0,0,0,0,0,1,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,1,0,0,0,0],[0,0,0,1,0,0,0,0,0,0,0,1,0,0,0],[0,0,0,0,1,0,0,0,0,0,0,0,1,0,0],[0,1,0,0,0,0,0,0,0,0,0,0,0,1,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,1]])
findCircleNum([[1,1,1],[1,1,1],[1,1,1]])
#findCircleNum([[1,0,0,1],[0,1,1,0],[0,1,1,1],[1,0,1,1]])
