def rob(nums):
#f(0) = nums[0]; f(1) = max(nums[0], nums[1]); f(k) = max(f(k-1), f(k-2)+nums[k])
    """sum1, sum2 = 0, 0
    for i in range(len(nums)):
        if i%2== 0:
            sum1 += nums[i]
        else:
            sum2 += nums[i]
    return max(sum1, sum2)"""
    last, now = 0, 0
    for i in nums:
        last, now = now, max(last+i, now)
    return now