# https://leetcode.com/problems/next-permutation/
class Solution(object):
    def nextPermutation(self, nums):
        """
        :type nums: List[int]
        :rtype: None Do not return anything, modify nums in-place instead.
        """
        #         idea:
        # (1)iterate i through nums reversely, find the first i s.t. nums[i] > nums[i-1], i-= 1, then i is where we want to increase
        # (2)swap nums[i] with nums[j] i.e., minimal num in range (i+1:) that's bigger than nums[i-1]
        # (3)after (2), nums[i+1:] is still non-decreasing, so reverse it
        if not nums or len(nums) == 1: return

        i = len(nums) - 1
        while nums[i] <= nums[i - 1]:
            i -= 1
            if i == 0:
                # WRONG rebind nums from inside, will not change nums from outside, has to be in-place
                # nums = nums[::-1]
                # break
                self.reverse(nums, 0, len(nums)-1)
                return
        i -= 1

        j = len(nums) - 1
        while nums[j] <= nums[i]:
            j -= 1

        # (2)        swap i and j
        self.swap(nums, i, j)
        # (3) easy to forget
        self.reverse(nums, i+1, len(nums)-1)

    #         self.revers
    def swap(self, nums, i, j):
        temp = nums[i]
        nums[i] = nums[j]
        nums[j] = temp

    def reverse(self, nums, head, rear):
        # head = 0
        # rear = len(nums) - 1
        while head < rear:
            self.swap(nums, head, rear)
            head += 1
            rear -= 1

    def nextPermutation2(self, nums):
        i = j = len(nums) - 1
        while i > 0 and nums[i - 1] >= nums[i]:
            i -= 1
        if i == 0:  # nums are in descending order
            nums.reverse()
            return
        k = i - 1  # find the last "ascending" position
        while nums[j] <= nums[k]:
            j -= 1
        nums[k], nums[j] = nums[j], nums[k]
        l, r = k + 1, len(nums) - 1  # reverse the second part
        while l < r:
            nums[l], nums[r] = nums[r], nums[l]
            l += 1;
            r -= 1

s = Solution()
# lst = [1, 3, 2]
lst = [3, 2, 1]
s.nextPermutation(lst)