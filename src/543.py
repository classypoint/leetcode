# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def diameterOfBinaryTree(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        self.ans = 0

        def depth(p):
            if not p: return 0
            left = depth(p.left)
            right = depth(p.right)
            # use ans to record diameter
            self.ans = max(self.ans, left + right)
            return 1 + max(left, right)

        depth(root)
        return self.ans
s = Solution()
l = TreeNode(1)
l.left = TreeNode(2)
l.right = TreeNode(3)
l.left.left = TreeNode(4)
l.left.right = TreeNode(5)
l.left.right.right = TreeNode(6)
l.left.right.right.right = TreeNode(7)
s.diameterOfBinaryTree(l)