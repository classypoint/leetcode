# https://blog.csdn.net/Antineutrino/article/details/6763722
class Solution(object):
    def evalRPN(self, tokens):
        """
        :type tokens: List[str]
        :rtype: int
        """
        stack = []
        ops = {"+": (lambda x, y: x + y), "-": (lambda x, y: x - y),
               "*": (lambda x, y: x * y), "/": (lambda x, y: (x // y
            if x*y >= 0 or x % y == 0 else x // y + 1))}
        for s in tokens:
            if s.isdigit() or s[1:].isdigit():#does not work for "-5"
                stack.append(int(s))
            else:
                num1 = stack.pop()
                num2 = stack.pop()
                res = ops[s] (num2, num1)
                stack.append(res)

        return stack[0]
s = Solution()
# s.evalRPN(["2", "1", "+", "3", "*"])
# s.evalRPN(["4", "13", "5", "/", "+"])
# s.evalRPN(["10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"])
s.evalRPN(["10","6","9","3","+","-11","*","/","*","17","+","5","+"])