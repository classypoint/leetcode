# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
# 1) level order
# 2) one pre, one inorder
class Codec:
    # like JAVA static method, should not use any object variable (self)
    def serialize1(self, root):
        """Encodes a tree to a single string.

        :type root: TreeNode
        :rtype: str
        """
        res = []
        queue = [root]
        while queue:
            node = queue.pop(0)
            if not node:
                res.append(None)
                continue
            res.append(node.val)
            queue.append(node.left)
            # else:
            #     queue.append(None)
            queue.append(node.right)

        return str(res)

    def deserialize2(self, data):
        """Decodes your encoded data to tree.

        :type data: str
        :rtype: TreeNode
        """

    def serialize(self, root):
        # inorder traversal
        def doit(node):
            if node:
                vals.append(str(node.val))
                doit(node.left)
                doit(node.right)
            else:
                vals.append('#')
        vals = []
        doit(root)
        return ' '.join(vals)

    def deserialize(self, data):
        def doit():
            val = next(vals)
            if val == '#':
                return None
            node = TreeNode(int(val))
            node.left = doit()
            node.right = doit()
            return node
        vals = iter(data.split())
        return doit()


# Your Codec object will be instantiated and called as such:
root = TreeNode(1)
root.left = TreeNode(2)
root.right = TreeNode(3)
root.right.left = TreeNode(4)
root.right.right = TreeNode(5)

codec = Codec()
# codec.deserialize(codec.serialize(root))
s = codec.serialize(root)
codec.deserialize(s)