class TreeNode:
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None
#import sys
def getMinimumDifference(root):
	def dfs(node, l = []):
		if node.left:
			dfs(node.left, l)
		l.append(node.val)
		if node.right:
			dfs(node.right, l)
		return l
	#l is sorted from smallest to biggest
	l = dfs(root)
	#ml = [abs(l[j]-l[i]) for j in range(i+1, len(l)) for i in range(len(l)-1)]
	return min([abs(a-b) for a,b in zip(l, l[1:])])
