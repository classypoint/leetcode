import matplotlib.pyplot as plt
import numpy as np
import math


def getFaultCluster(nc, nl):
    return nl // int((nc - 1)/3 + 1)

# nc = 30
# nl = list(range(4, 1000))
# y = []
# for n in nl:
#     y.append(getFaultCluster(nc, n)/n)
#
# plt.plot(nl, y)
# plt.show()

committee = 1000
total = 10000
fac = math.factorial
lg = math.log10

# def getFailProb(nc, n, f):
#     res = 0
#     minHonest = max(0, nc - f)
#     for i in range(minHonest, 2 * nc // 3):
#         denom = fac(n) // fac(nc) // fac(n - nc)
#         if i == 0:
#             tmp = fac(f) // fac(nc) // fac(f - nc)
#         else:
#             tmp = fac(n - f) // fac(i) // fac(n - f - i)
#             tmp *= fac(f) // fac(nc - i) // fac(f - nc + i)
#         res += tmp // denom
#     return res

def getFailProb2(nc, n, f):
    res = 0
    minHonest = max(0, nc - f)
    maxHonest = min(2 * nc // 3, n - f)
    for i in range(minHonest, maxHonest):
        #opt here
        denom = lg(fac(n)) - lg(fac(nc)) - lg(fac(n - nc))
        if i == 0:
            if f > nc:
                tmp = lg(fac(f)) - lg(fac(nc)) - lg(fac(f - nc))
            elif f == nc:
                tmp = lg(fac(f)) - lg(fac(nc))
        else:
            tmp = lg(fac(n - f)) - lg(fac(i)) - lg(fac(n - f - i))
            tmp += (lg(fac(f)) - lg(fac(nc - i)) - lg(fac(f - nc + i)))
        res += pow(10, (tmp - denom))
    return res
# print(getFailProb(nc, n, n // 5))

def helper(nc, n, alpha):
    y = []
    for a in alpha:
        v = getFailProb2(nc, n, int(n * a))
        print(v, a)
        y.append(v)
    return y

def plot(x, y):
    ax = plt.subplot()
    ax.plot(x, y, label="n=10k, nc=1k")
    plt.xlabel("Faulty nodes")
    plt.ylabel("Committee failure")
    ax.legend()
    plt.show()

x = np.arange(0, 0.4, 0.05)
y = helper(committee, total, x)
plot(x, y)
# print(getFailProb2(committee, total, total * 0.3))
