# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    # recursion
    def inorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        ret = []
        self.inorder(root, ret)
        return ret

    def inorder(self, node, res):
        if not node:
            return
        self.inorder(node.left, res)
        res.append(node.val)
        self.inorder(node.right, res)

    #iteration
    def inorderTraversal2(self, root):
        ans = []
        stack = []
        cur = root
        while cur or stack:
            # while node is not null, push stack
            while cur:
                stack.append(cur)
                cur = cur.left
            # node is null, pop stack
            cur = stack.pop()
            # add current val
            ans.append(cur.val)
            # right sub-tree
            cur = cur.right
        return ans

    # 记当前遍历的节点为 cur。
    # 1、cur.left 为 null，保存 cur 的值，更新 cur = cur.right
    # 2、cur.left 不为 null，找到 cur.left 这颗子树最右边的节点记做 last
    # 2.1 last.right 为 null，那么将 last.right = cur，更新 cur = cur.left
    # 2.2 last.right 不为 null，说明之前已经访问过，第二次来到这里，
    # 表明当前子树遍历完成，保存 cur 的值，更新 cur = cur.right
    def inorderTraversal3(self, root):
        ans = []
        cur = root
        while cur:
            # case 1
            if not cur.left:
                ans.append(cur.val)
                cur = cur.right
            else:
                # case 2
                pre = cur.left
                while pre.right and pre.right != cur:
                    pre = pre.right
                # 2.1
                if not pre.right:
                    pre.right = cur
                    cur = cur.left
                # 2.2
                if pre.right == cur:
                    pre.right = None#restore to None
                    ans.append(cur.val)
                    cur = cur.right

        return ans