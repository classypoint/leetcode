class Solution(object):
    # >>>TLE O(n^2)>>>
    def largestRectangleArea(self, heights):
        """
        :type heights: List[int]
        :rtype: int
        """
        maxArea = 0
        for i in range(len(heights)):
            l, r = i, i
            height = heights[i]
            # find the left and right bar for current height
            while l > 0 and heights[l - 1] >= height:
                l -= 1
            while r < len(heights) - 1 and heights[r + 1] >= height:
                r += 1
            curArea = height * (r - l + 1)
            maxArea = curArea if curArea > maxArea else maxArea

        return maxArea

    # DP, using 2 arr to record first pos less than cur height
    def largestRectangleArea2(self, heights):
        if not heights:
            return 0

        maxArea = 0
        n = len(heights)
        lessFromLeft = [0] * n #stores the pos of the first height shorter than cur to its left
        lessFromRight = [0] * n
        lessFromLeft[0] = -1
        lessFromRight[-1] = n

        for i in range(1, n):
            p = i - 1 # starting from i - 1 not i
            while p >= 0 and heights[p] >= heights[i]:
                p = lessFromLeft[p]
            lessFromLeft[i] = p

        for i in reversed(range(n - 1)):
            p = i + 1
            while p < n and heights[p] >= heights[i]:
                p = lessFromRight[p]
            lessFromRight[i] = p

        for i in range(n):
            area = heights[i] * (lessFromRight[i] - lessFromLeft[i] - 1)
            maxArea = max(area, maxArea)

        return maxArea

    # stack
    def max_area_histogram(self, histogram):
        # Create an empty stack. The stack holds indexes of histogram[] list.
        # The bars stored in the stack are always in increasing order of their heights.
        stack = list()
        max_area = 0  # Initalize max area

        # Run through all bars of given histogram
        index = 0
        while index < len(histogram):
            # If this bar is higher than the bar on top stack, push it to stack
            if (not stack) or (histogram[stack[-1]] <= histogram[index]):
                stack.append(index)
                index += 1
            # If this bar is lower than top of stack, then calculate area of rectangle with
            # stack top as the smallest (or minimum height) bar.'i' is 'right index' for
            # the top and element before top in stack is 'left index'
            else:
                # pop the top
                top_of_stack = stack.pop()
                # Calculate the area with histogram[top_of_stack] stack as smallest bar
                area = (histogram[top_of_stack] *
                        ((index - stack[-1] - 1)
                         if stack else index))
                # update max area, if needed
                max_area = max(max_area, area)
        # Now pop the remaining bars from stack and calculate area with
        # every popped bar as the smallest bar
        while stack:
            # pop the top
            top_of_stack = stack.pop()
            # Calculate the area with histogram[top_of_stack] stack as smallest bar
            area = (histogram[top_of_stack] *
                    ((index - stack[-1] - 1)
                     if stack else index))
            # update max area, if needed
            max_area = max(max_area, area)
            # Return maximum area under the given histogram
        return max_area


hts = [2,1,5,6,2,3]
print(Solution().max_area_histogram(hts))