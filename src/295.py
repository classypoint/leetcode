from heapq import *

class MedianFinder:

    def __init__(self):
        self.heaps = [], []

    def addNum(self, num):
        small, large = self.heaps
        heappush(small, -heappushpop(large, num))
        if len(large) < len(small):
            heappush(large, -heappop(small))

    def findMedian(self):
        small, large = self.heaps
        if len(large) > len(small):
            return float(large[0])
        return (large[0] - small[0]) / 2.0

# this will fail if num is decreasing....
# class MedianFinder(object):
#     def __init__(self):
#         """
#         initialize your data structure here.
#         """
#         self.heaps = [], []
#
#     def addNum(self, num):
#         """
#         :type num: int
#         :rtype: None
#         """
#         small, large = self.heaps
#         if len(small) == len(large):
#             heappush(large, num)
#         else:
#             heappush(small, -heappop(large))
#             heappush(large, num)
#
#     def findMedian(self):
#         """
#         :rtype: float
#         """
#         small, large = self.heaps
#         if len(small) == len(large):
#             return (large[0] - small[0]) / 2.0
#         else:
#             return float(large[0])

obj = MedianFinder()
# obj.addNum(1)
# obj.addNum(2)
# obj.findMedian()
# obj.addNum(3)
# obj.findMedian()
# obj.addNum(4)
# obj.addNum(5)
# obj.addNum(6)
# obj.addNum(7)
# obj.addNum(8)
obj.addNum(-1)
obj.findMedian()
obj.addNum(-2)
obj.findMedian()
obj.addNum(-3)
obj.findMedian()