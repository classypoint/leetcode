# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def levelOrder(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
#         BFS using fringe/queue to store current known
        if not root:
            return []
        nodes = [[root.val]]
        self.bfs([root], root, nodes)
        return nodes
    # def bfs(self, fringe, cur, curlevel, nodes):
    def bfs(self, fringe, cur, nodes):
        end = cur
        # while fringe or cur:
        while fringe:
            cur = fringe.pop(0)
            if cur.left:
                fringe.append(cur.left)
            if cur.right:
                fringe.append(cur.right)
            if cur == end and fringe:
                start, end = fringe[0], fringe[-1]
                curlevel = [x.val for x in fringe]
                nodes.append(curlevel)
s = Solution()
n = TreeNode(1)
n.left = TreeNode(2)
n.right = TreeNode(3)
n.left.left = TreeNode(4)
n.left.right = None
n.right.left = TreeNode(6)
n.right.right = TreeNode(7)
s.levelOrder(n)