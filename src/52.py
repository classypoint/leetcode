class Solution:
    def solveNQueens(self, n):
        self.count = 0
        self.backtrace([], n)
        return self.count

    def backtrace(self, curQueens, n):
        # note INT is deep copy, cannot do backtrace(curQueens, n, count)
        # instead: can return INT or change class variable
        if len(curQueens) == n:
            # has settled all n queens
            self.count += 1
            return

        for col in range(n):
            # col conflict
            if not col in curQueens:
                if self.isDiagAttack(curQueens, col):
                    continue
                # curQueens.append(col)
                self.backtrace(curQueens + [col], n)
                # curQueens.pop()

    def draw(self, lst):
        # draw chess board according to queen pos in each line
        board = []
        for pos in lst:
            line = ['.']*len(lst)
            line[pos] = 'Q'
            board.append(''.join(line))
        return board

    def isDiagAttack(self, curQueens, i):
        curRow = len(curQueens)
        for row in range(curRow):
            # left diag or right diag
            if abs(curRow - row) == abs(i - curQueens[row]):
                return True

        return False

#     >>>>>method 2>>>>
    def totalNQueens(self, n):
        cols = [False]*n    #col
        d1 = [False]*(2*n)  #main diag
        d2 = [False]*(2*n - 1)  #the other diag
        return self.dfs(0, cols, d1, d2, n, 0)

    def dfs(self, row, cols, d1, d2, n, count):
        if row == n: count += 1
        else:
            for col in range(n):
                id1 = row - col + n
                id2 = row + col
                if cols[col] or d1[id1] or d2[id2]:
                    continue
                cols[col] = True
                d1[id1] = True
                d2[id2] = True
                count = self.dfs(row + 1, cols, d1, d2, n, count)
                cols[col] = False
                d1[id1] = False
                d2[id2] = False

        return count

#     >>>>>method 3>>>>
# instead of using sum/sub as arr index, store sum/sub directly
    def totalNQueens2(self, n):
        return self.dfs2([], [], [], n, 0)

    def dfs2(self, cols, d1, d2, n, count):
        row = len(cols)
        if row == n:
            count += 1
            return count

        for col in range(n):
            id1, id2 = row + col, row - col
            if col in cols or id1 in d1 or id2 in d2:
                continue

            count = self.dfs2(cols + [col], d1 + [id1], d2 + [id2], n, count)

        return count


s = Solution()
print(s.totalNQueens2(4))