# >>> method 1>>>
def lengthOfLastWord(s):
    if not s:
        return 0
    s = s.strip(' ')
    last = s.split(sep=' ')[-1]
    return len(last)
    #len(s.strip().split(" ")[-1])

# >>> method 2>>>
def lengthOfLastWord2(s):
    ls = len(s)
    # slow and fast pointers
    slow = -1
    # iterate over trailing spaces
    while slow >= -ls and s[slow] == ' ':
        slow -= 1
    fast = slow
    # iterate over last word
    while fast >= -ls and s[fast] != ' ':
        fast -= 1
    return slow - fast

def lengthOfLastWord3(s):
    index = len(s) - 1
    count = 0

    # end of last word, skipping blank space
    while index > 0 and s[index] == ' ':
        index -= 1

    # to the start of last word
    while index >= 0:
        if s[index] == ' ':
            break
        count += 1
        index -= 1
    return count

lengthOfLastWord2("hi there")
