# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

def reverseBetween(head, m, n):
    dummy = ListNode(0)
    dummy.next = head
    cur1 = dummy
    pre1 = None
    for i in range(m):
        pre1 = cur1
        cur1 = cur1.next
    cur2 = cur1
    pre2 = pre1
    for i in range(m, n+1):
        q2 = cur2.next
        cur2.next = pre2
        pre2 = cur2
        cur2 = q2
    pre1.next = pre2
    cur1.next = cur2
    return dummy.next

#Sol 2
class Solution:
    def reverseBetween(self, head, m, n):
        """
        :type head: ListNode
        :type m: int
        :type n: int
        :rtype: ListNode
        """
        if m == n:
            return head

        dummy = ListNode(0)
        dummy.next = head
        pre = dummy

        for i in range(m - 1):
            pre = pre.next

        # reverse the [m, n] nodes
        reverse = None
        cur = pre.next
        for i in range(n - m + 1):
            next = cur.next
            cur.next = reverse
            reverse = cur
            cur = next

        # relink
        pre.next.next = cur
        pre.next = reverse

        return dummy.next


def createList():
    head = ListNode(1)
    l = head
    for i in range(2, 6):
        l.next = ListNode(i)
        l = l.next
    return head

def printList(lst):
    while lst:
        print(lst.val)
        lst = lst.next

s = Solution()
l = createList()
printList(s.reverseBetween(l, 2, 4))
