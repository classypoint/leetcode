#Sol 1: Time limit exceed
def trailingZeroes(n):
	f = 1
	for i in range(1, n+1):
		f = f*i
	last = 0
	count = -1
	while last == 0:
		count += 1
		last, f = f%10, f//10
	return count
trailingZeroes(3938)
#Sol 2
def trailingZeroes(n):
	return 0 if n == 0 else n / 5 + self.trailingZeroes(n / 5)