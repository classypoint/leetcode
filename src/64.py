# https://leetcode.com/problems/minimum-path-sum/
class Solution(object):
    def minPathSum(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        # similar to 62, use 2d DP. d[m][n] denotes the minimal sum reaching [m][n]
        # for first row and first col:
        #   d[0][0] = grid[0][0], d[0][j] = d[0][j-1] + grid[0][j], d[i][0] = d[i-1][0] + grid[i][0]
        # d[i][j] = min (d[i-1][j], d[i][j-1]) + grid[i][j]

        #         save space by using only two rows
        row = len(grid)
        col = len(grid[0])

        d1 = [grid[0][0] for j in range(col)]
        for j in range(1, col):
            d1[j] = d1[j - 1] + grid[0][j]

        if row < 2: return d1[col - 1]

        d2 = [0 for _ in range(col)]

        for i in range(1, row):
            d2[0] = d1[0] + grid[i][0]
            for j in range(1, col):
                d2[j] = min(d2[j - 1], d1[j]) + grid[i][j]
            d1 = d2

        return d2[col - 1]