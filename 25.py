# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    # reverse
    def reverseKGroup(self, head, k):
        """
        :type head: ListNode
        :type k: int
        :rtype: ListNode
        """
        cur = head
        count = 0
        # find the k+1 th node
        while cur and count != k:
            cur = cur.next
            count += 1

        if count == k:#if k+1 node is found
            cur = self.reverseKGroup(cur, k)#reverse list with k+1 node as head
            #head - head-pointer to direct part
            #cur - head-pointer to reversed part
            while count > 0:#reveres current k-group
                tmp = head.next
                head.next = cur
                cur = head
                head = tmp
                count -= 1
            head = cur

        return head

s = Solution()
l = ListNode(1)
l.next = ListNode(2)
l.next.next = ListNode(3)
l.next.next.next = ListNode(4)
l.next.next.next.next = ListNode(5)
r = s.reverseKGroup(l, 3)
while r:
    print(r.val)
    r = r.next